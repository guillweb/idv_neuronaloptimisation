__author__ = 'guillaume'

import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""

# ----------
# PARAMETERS
# ----------


# Filter frequency to simulate choclear filtering
f0 = 1000 * Hz
df0 = 200 * Hz

# Stimulus t
tmin = 0 * second
tmax = 0.5 * second
Nfilters = 20
N = 5000

# Period assumption
Tau = 10 * ms
TauMin = Tau/2
TauMax = Tau*2
TauPeriod = 5 * TauMax

# Optimisation Param
Niteration = 10**5
RecordEvery = Niteration/1000

# renormType
renormType = 1 # 0: weight, 1: output, -1: none


# ----------
# Solver
# ----------

t = time.clock()

# Define input
inp = il.Input(tmin,tmax,N)
inp.addWaveFromFile("data/yourmom.wav",0 * ms)
inp.butter_bandpass_filter(f0 -df0/2,f0 +df0/2)
inp.expandWithRecursiveFilterConvolution(TauMin,TauMax,Nfilters)
inp.whitenInputs()

t = ti.timeCount(t,'Input setup (s):')


# Create STDP window filter (Omega)
F = fb.Filter(inp.T,TauPeriod)
F.setFilter(F.getDerivativeSquaredFilter(Tau))

t = ti.timeCount(t,'Filter setup (s):')


# Create optimiser object and run optimisation
opt = fo.FilterOpt(inp,F)
opt.computeIterationsFISTA(Niteration,renormType,RecordEvery)
CostSequence,logCostSeq = opt.get_objective_sequence()
output = opt.getOutput().T
bestOutput = opt.getBestOutput().T

t = ti.timeCount(t,'Optim (s):')

# -------
# Plots
# ------

plt.figure(1)

# Plot input
plt.subplot(4,2,1)
plt.plot(inp.T,inp.x)

frq,spc = to.getLogLogSpectrum(inp.x,inp.FS,40,10**4)
plt.subplot(4,2,2)
plt.plot(frq,spc)

# Plot optimisation stuff
plt.subplot(4,2,3)
plt.plot(inp.T,inp.X.T)

plt.subplot(4,2,4)
plt.plot(logCostSeq)

# Plot output
plt.subplot(4,2,5)
plt.plot(inp.T,output)

frq,spc = to.getLogLogSpectrum(output,inp.FS,40,10**4)
plt.subplot(4,2,6)
plt.plot(frq,spc)

# Plot output
plt.subplot(4,2,7)
plt.plot(inp.T,bestOutput)

frq,spc = to.getLogLogSpectrum(bestOutput,inp.FS,40,10**4)
plt.subplot(4,2,8)
plt.plot(frq,spc)
plt.show()

t = ti.timeCount(t,'Plot (s):')