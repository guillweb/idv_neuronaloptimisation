import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
import random as rd


class Sto_Hilbert:
    def  __init__(self,X,FS,Kdelay,Ksfa):
        self.FS = FS
        self.T = len(X)
        self.delayInput(X,Kdelay)
        self.Ksfa = Ksfa


    def computeSFA(self,Ksfa):
        s, U = np.linalg.eig(np.dot(self.X,self.X.T))

        #Compute gradients
        self.dX = self.X[:,1:self.T-1] - self.X[:,0:self.T-2]
        dS = np.dot(U,self.dX)

        #Save sphering matrix
        self.U = U
        #Save slowness parameters
        self.theta = np.sqrt( np.abs(s) )

        order = np.argsort(self.theta)
        self.U = self.U[order[0:Ksfa-1]]
        self.theta = self.theta[order[0:Ksfa-1]]

    def computeHilbert(self):
        self.computeSFA(self.Ksfa)
        A = np.multiply(np.dot(self.U,self.dX).T,1/self.theta).T
        self.hilbert = np.dot(self.U.T,A)

        return self.delayOutput()


    def delayInput(self,input,Kdelay):
        x = input
        X = input
        for iDel in range(1, Kdelay):
            x = np.hstack((x[1:],[0]))
            X = np.vstack((X, x))

        self.X = X
        self.Kdelay = Kdelay
        return X

    def delayOutput(self):
        output =  self.hilbert[0,:]

        for iDel in range(1, self.Kdelay):
            output[0:self.T-1-iDel] = self.hilbert[iDel,0:self.T-1-iDel]

        return output
