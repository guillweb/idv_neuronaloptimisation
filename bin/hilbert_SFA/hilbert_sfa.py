import numpy as np
import scipy as sc
import scipy.io.wavfile as wave
from bin.hilbert_SFA import sto_hilbert
from bin.tools import spectralAnalysis
import numpy.linalg as la
import matplotlib.pyplot as plt

#Open sound

filename = "applause.wav"
FS,X = wave.read(filename)
X = np.array(X, dtype=float)
X = X/np.max(X)
X = X[0:FS/2]
Kdelay = 512
KSFA = 1

#Compute Hilbert.
hil = sto_hilbert.Sto_Hilbert(X,FS,Kdelay,KSFA)
hilSignal = hil.computeHilbert()

a = np.power(X,2)
b = np.power(hilSignal,2)
env = a[0:len(hilSignal)] + b
#Get envelope
plt.figure(1)
plt.subplot(2,2,1)
plt.plot(X)
plt.subplot(2,2,2)
plt.plot(env)

plt.subplot(2,2,3)
frq,spc = spectralAnalysis.getSpectrum(X,FS)
plt.plot(spc)
plt.subplot(2,2,4)
frq,spc = spectralAnalysis.getSpectrum(env,FS)
plt.plot(spc)
plt.show()

#Print


