from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt
import time

brian_prefs.codegen.target = 'weave'

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none


# Sampling parameters
tmin = 0 * second
tmax = 20 *second
FS = 5000 * Hz

repeat = 1
fWiskott = 10 * Hz


# SFA parameters
TauOmega = 10 /FS                   # STDP window time constant constant

# Output membrane potential
taum = 10. * ms                         # This low passes the output. Slow feature to capture have to vary slower
tauw = 10**3. * ms

alRandom = 0.1                    # Ratio between TauOpt and TauOmega

Noutputs = 5
V0 = 100. * mV
dtRenorm = 1/FS * 10

# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [fWiskott]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 200

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]




# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam,repeat)
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')



# Inital connections and output
A0 = np.random.randn(Noutputs,inp.n)
S0 = np.random.randn(Noutputs,1)



# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

stimulus = inp.getTimedArray(V0)

# Create STDP window filter (Omega)
al = exp(1)
Tau1 = TauOmega/(al - 1)
Tau2 = al * Tau1
RFinput = rf.recursiveFilters(Tau1,Tau2,2,Dt,Ninputs)
RFoutput = rf.recursiveFilters(Tau1,Tau2,2,Dt,Noutputs)

proj = np.array([[0.25], [-0.25]])

tCount = ti.timeCount(tCount,'Filter setup (s):')


G_pre = NeuronGroup(Ninputs, '''    #dv/dt = (-v + v_in)/taum + alRandom *V0 * xi/taum**0.5 : volt
                                    dv/dt = (-v + v_in)/taum  : volt
                                    v_in : volt

                                    dv_1/dt = - (v_1 + v) / Tau1 : volt
                                    dv_2/dt = - (v_2 + v) / Tau2 : volt

                             ''', method="euler")

G_post = NeuronGroup(Noutputs, '''  dv/dt = - (v - vhat)  / taum : volt
                                    vhat : volt

                                    dvhat_1/dt = - (vhat_1 + vhat) / Tau1 : volt
                                    dvhat_2/dt = - (vhat_2 + vhat) / Tau2 : volt

                                    ''', method="euler")

S = Synapses(G_pre, G_post, \
             model= '''
                        al1 : 1
                        al2 : 1

                        vOmega =  al1 * v_1_pre/Tau1 + al2 * v_2_pre/Tau2 : volt * Hz

                        bet1 : 1
                        bet2 : 1

                        vhatOmega = bet1 * vhat_1_post/Tau1 + bet2 * vhat_2_post/Tau2 : volt * Hz

                        STDP = (vOmega * vhat_post + v_pre * vhatOmega) * TauOmega / V0**2 : 1

                        dw/dt  = STDP/ tauw : 1


                        vhat_post = w*v_pre : volt (summed)
                         '''
             ,connect=True)


S.al1['j == 0'] = '0.25'
S.al2['j == 0'] = '-0.25'
S.bet1['j == 0'] = '0.25'
S.bet2['j == 0'] = '-0.25'

S.al1['j == 1'] = '-0.25'
S.al2['j == 1'] = '0.25'
S.bet1['j == 1'] = '-0.25'
S.bet2['j == 1'] = '0.25'

S.al1['j == 2'] = '0.5'
S.al2['j == 2'] = '0'
S.bet1['j == 2'] = '-0.5'
S.bet2['j == 2'] = '0'


S.al1['j == 3'] = '-0.5'
S.al2['j == 3'] = '0'
S.bet1['j == 3'] = '0.5'
S.bet2['j == 3'] = '0'

S.al1['j == 4'] = '0.5'
S.al2['j == 4'] = '0'
S.bet1['j == 4'] = '0.5'
S.bet2['j == 4'] = '0'








# Renorm


#a = - np.array([-0.99557225, -0.0443698 ,  0.05636756, -0.03133591, -0.0518255 ])
a = np.random.rand(Ninputs)
bt.init_weights_same_for_outputs(S,Noutputs,1,a)
bt.norm_weights(S,Noutputs)

G_post.vhat = np.random.randn(1,1) * V0
G_post.vhat_1 = G_post.vhat
G_post.vhat_2 = G_post.vhat
G_post.v = G_post.vhat

G_pre.v = inp.X[:,0] * V0
G_pre.v_1 = G_pre.v
G_pre.v_2 = G_pre.v
G_pre.v_in = G_pre.v

@network_operation(when='start')
def getStimulusAndRenorm(t):
    # Get input current at time t
    intT = int(t / (Dt * second))
    G_pre.v_in = inp.X[:,intT] * V0

@network_operation(when=Clock(dtRenorm))
def normalize_along(t):
    # Renorm weights
    bt.norm_weights(S,Noutputs)






mon_pre = StateMonitor(G_pre, 'v', record=True)
mon_post = StateMonitor(G_post, 'v', record=True)
mon_syn = StateMonitor(S, 'w', record=True)
tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

print 'Weights: '
print S.w

plt.figure(1)
for i in range(0,min(Ninputs,3)):
    plt.subplot(331 +i)
    j = min(Ninputs-1,i*(Ninputs+1)/2)
    plt.plot(mon_pre[j].v.transpose())
    plt.title('v_pre ' + `j`)




plt.subplot(334)
for Ni in range(0,Noutputs):
    plt.plot(inp.T,mon_post[Ni].v.transpose(), label= "%d" % Ni)
legend()
plt.title('v_post ')


# PRINT LOW PASSED SPECTRUMS
plt.subplot(336)
for Ni in range(0,Noutputs):
    (frq,sp) = to.getSpectrum(mon_post[Ni].v,1/defaultclock.dt)
    plt.plot(frq,20*np.log10(sp+ 10**-10),label="%d" % Ni)
xscale('log')
legend()
plt.title('Spectrums in dB ')



for i in range(0,3):
    plt.subplot(337 +i)

    for No in range(0,Noutputs):
        idx_post = find(S._postsynaptic_idx == No)
        idx_pre = find(S._presynaptic_idx == i)

        ind = np.intersect1d(idx_post,idx_pre)
        plt.plot(mon_syn[ind].w.transpose(),label="%d" % No)
    legend()

    plt.title('w ' + `i`)



plt.show()

pass
