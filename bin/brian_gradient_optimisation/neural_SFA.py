from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *


# ------------- Stimulus definition
from bin.gradient_optimisation import Optimiser_filter

Ndelays = 3
Noutputs = 1

# Sampling and time
Npoints = 1000
Texp = float(1.)
FS = Npoints/Texp
Dt = 1/FS
t = 1/FS * np.matrix(range(1, Npoints+1))

# Cosines coeff
f0 = 31.
f1 = 77.
q1 = 2
qrand = 0.05

# Construction
T = t
for iDel in range(1, Ndelays):
    T = np.vstack((T, t - iDel * Dt))

X = np.cos(2* np.pi * f0 *T ) \
    + q1 * np.sin(2* np.pi * f1 *T ) \
    + qrand * np.random.randn(Ndelays,Npoints)

# Non-linear expansion
X = Optimiser_filter.NonLinearExpanded(X).addQuadratic()
Ninputs = np.shape(X)[0]

# Inital connections and output
A0 = np.matrix(np.random.randn(Noutputs,np.shape(X)[0]))
S0 = np.matrix(np.random.randn(Noutputs,Npoints))


# parameters
muReg = 0
muSFA = 0.5


# ------------- Brian
taum = 10 * ms
tauw = 10**4 * ms
V0 = 100 * mV

tauMemory = 10000 * ms
alMem = exp(- Dt *second / tauMemory)



# Moving average definition
corr = np.zeros( Noutputs*Ninputs ) * mV**2

# Expansion matrix
a = np.ones( (Ninputs,1) )
b = np.zeros( (Ninputs,1) )
b[0] = 1
ExpMat = a
RedMat = b
for i in range(1,Noutputs):
    ExpMat = block_diag(ExpMat,a)
for i in range(1,Noutputs):
    RedMat = block_diag(RedMat,b)
RedMat = RedMat.transpose()

#taum = np.dot(ExpMat,taum)
#tauw = np.dot(ExpMat,tauw)

#inputs = TimedArray(np.array( (A0 * X).transpose()) * mV,dt= Dt * 1000 * ms)

G = NeuronGroup(Noutputs*Ninputs, \
                                '''du/dt = (u + v - vhat)  / taum : volt
                                dv/dt = u /(muSFA * taum) : volt
                                dw/dt = - ( corrhat - corr ) / (tauw * V0**2) : 1
                                corrhat  : volt**2
                                corr  : volt**2
                                vhat   : volt ''')

# State initialisation
x = np.array(X[:, 0]) * V0
vHat0 = np.dot(A0,x)
G.vhat =  np.dot(ExpMat,vHat0) # estimator of v
G.v = np.dot(ExpMat, np.random.randn(Noutputs)) * V0
G.u = np.dot(ExpMat, np.random.randn(Noutputs)) * V0

G.corrhat = (vHat0 * x.transpose()).reshape(Ninputs*Noutputs)
G.corr = corr

G.w = np.random.randn(Ninputs*Noutputs)


@network_operation
def update_grads(t):

    # Get input current at time t
    intT = int(t / (Dt * second))
    x = np.array(X[:, intT]) * V0

    # Get weights in a matrix form
    A = np.array(G.w).reshape((Noutputs,Ninputs))
    vHat = np.dot(A,x)
    G.vhat = np.dot(ExpMat,vHat) # estimator of v

    # Compute correlations
    G.corrhat *= alMem**2
    G.corrhat += (vHat * x.transpose()).reshape(Ninputs*Noutputs) # estimator of corr = < s, x >

    # Estimator
    G.corr *= alMem**2
    G.corr += (x * np.dot(RedMat,G.v)).transpose().reshape(Noutputs*Ninputs)

    pass



mon = StateMonitor(G, 'v', record=True)
mon2 = StateMonitor(G, 'w', record=True)
net = Network(update_grads,G, mon, mon2)
#net.add(update_inputs)
net.run(Texp * second)



plt.figure(1)
for i in range(0,min(Ninputs,3)):
    plt.subplot(331 +i)
    plt.plot(X[i,:].transpose())
    plt.title('X ' + `i`)
for i in range(0,min(Noutputs,3)):
    plt.subplot(334 +i)
    plt.plot(mon[i*Ninputs].v.transpose())
    plt.title('v ' + `i*Ninputs`)
for i in range(0,min(Ninputs,3)):
    plt.subplot(337 +i)
    plt.plot(mon2[i].w.transpose())
    plt.title('w ' + `i`)



plt.show()