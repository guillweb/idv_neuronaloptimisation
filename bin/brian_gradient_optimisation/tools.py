__author__ = 'guillaume'

from brian2 import *
import numpy as np


import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti

# Renorm

def set_voltage(xt,codeType,codeParam):
    V0,nuMin,nuMax,Vthr,dt = codeParam

    if codeType == 1: #Voltage code
        return xt * V0
    elif codeType == 0:
        v = np.zeros(shape(xt)) * V0
        rd = np.random.random(shape(xt))
        val = (xt+1)/2
        v[(nuMin + val * nuMax) * dt < rd] = 1 * Vthr
        print "set_voltage for rate type is deprecated."

        return v

def create_connect_matrix(S,Ninputs,Noutputs):
    C = np.zeros((Noutputs,Ninputs))
    for No in range(0,Noutputs):
        idx = find(S._postsynaptic_idx == No)
        C[No,:] = idx
    return np.int_(C)

#--------------------
# Define neuron classes
#----------------------

def  neuron_class(ensemble,neuron_number):
    return neuron_number/ensemble

def  string_neuron_class(ensemble,neuron_number):
    return 'j / {0} == {1}'.format(ensemble,neuron_number)

def get_neuron_class(i,Ninputs,Minputs):
    a = np.array(range(0,Ninputs*Minputs))
    return find(a/Minputs == i)

# --------------
# Set weight sum
# --------------

def set_weight_sum(S,G_post,Noutputs):


    for No in range(0,Noutputs):
        G_post.ws[No] = np.sum(S.w[:,No])


# --------------
# Norm weights
# --------------

def norm_weights(S,Noutputs):
    pass
    for No in range(0,Noutputs):
        sd = np.maximum(norm(S.w[:,No],axis=0),10 ** -10)
        S.w[:,No] /= sd
    pass

def norm_targeted_weights(S,Noutputs):

    for No in range(0,Noutputs):
        sdeq = np.maximum(norm(S.weq[:,No],axis=0),10 ** -10)
        S.weq[:,No] /= sdeq




# --------------
# Init Weights
# --------------

def init_weights_all_random(S):
    a = np.random.randn(np.shape(S.w))
    S.w = a

def init_weights_same_for_outputs(S,Ninputs,Minputs,a):
    for Ni in range(0,Ninputs*Minputs):
        S.w[Ni,:] = a[neuron_class(Minputs,Ni)]

def init_targeted_weights_same_for_outputs(S,Noutputs,a):
    for No in range(0,Noutputs):
        S.weq[No,No] = a


#------------------
# Period summation
#------------------

def period_sum(x,t0,tmax,f0,FS):

    n_0 = t0 * FS
    duration = tmax - t0
    nb_freq = int(duration*f0)
    N_out = int(FS/f0)

    X = np.hsplit(x[:,n_0:N_out*nb_freq+n_0],nb_freq)
    x_out = np.mean(X,axis=0)

    return np.arange(N_out) / FS,x_out

# --------------
# Rate filtering
# --------------

def compute_rates(tauRate,inp,T,dt,spike_mon,Noutputs,MOutput,filter):

    builder = fb.FilterBuilder(T,tauRate * 2)

    if filter == 'gauss':
        F = builder.getGaussFilter(tauRate)
    elif filter == 'alpha':
        F = builder.getAlphaFilter(tauRate)
    elif filter == 'exp':
        F = builder.getExpFilter(tauRate)
    elif filter == 'rough':
        F = builder.getRoughFilter(tauRate)


    i,t = spike_mon



    sig = np.zeros((len(T),Noutputs))
    out = il.Input(inp.tmin,inp.tmax,len(T))
    for No in range(0,Noutputs*MOutput):
        indNeuron = find(i == No)
        t0 = t[indNeuron]
        indSpikes = np.int_(t0/dt)
        sig[indSpikes,neuron_class(MOutput,No)] += 1. / MOutput



    for No in range(0,Noutputs):
        sig[:,No] = F.convolveSignal(sig[:,No].ravel())

    return sig


def smooth_rate_vector_gauss(tauRate,inp,T,dt,s,Noutputs):

    builder = fb.FilterBuilder(T,tauRate * 2)
    F = builder.getGaussFilter(tauRate)

    sig = np.zeros((len(T),Noutputs))

    for No in range(0,Noutputs):
        sig[:,No] = F.convolveSignal(s[No,:].ravel())

    return sig



def compute_rates_butter(G_post,fmin,fmax,inp,spike_mon):
    T = inp.T
    dt = inp.dt
    Noutputs = len(G_post.v)

    i,t = spike_mon



    sig = np.zeros((len(T),Noutputs))
    for Ni in range(0,Noutputs):
        out = il.Input(inp.tmin,inp.tmax,len(T))
        indNeuron = find(i == Ni)
        t0 = t[indNeuron]
        indSpikes = np.int_(t0/dt)
        sig[indSpikes,Ni] = 1

        out.addData(sig[indSpikes,Ni],0 * ms)
        out.butter_bandpass_filter(fmin,fmax)

        sig[:,Ni] = out.x

    return sig


def compute_rates_butter(G_post,fmin,fmax,inp,spike_mon):
    T = inp.T
    dt = inp.dt
    Noutputs = len(G_post.v)

    i,t = spike_mon



    sig = np.zeros((len(T),Noutputs))
    for Ni in range(0,Noutputs):
        out = il.Input(inp.tmin,inp.tmax,inp.N)
        indNeuron = find(i == Ni)
        t0 = t[indNeuron]
        indSpikes = np.int_(t0/dt)
        sig[indSpikes,Ni] = 1

        out.addData(sig[indSpikes,Ni],0 * ms)
        out.butter_bandpass_filter(fmin,fmax)

        sig[:,Ni] = out.x

    return sig