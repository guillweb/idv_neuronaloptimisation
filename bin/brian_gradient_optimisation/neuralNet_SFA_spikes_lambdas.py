from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt

import time

brian_prefs.codegen.target = 'weave'

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin


fWiskott = 1 * Hz                      # low frequency modulation in toy example
nuMin = fWiskott * 10000                  # Minimum rate
nuMax = nuMin * 2                       # Max rate

# Sampling parameters
tmin = 0 * second
tmax = 20 * second
FS = 50000 * Hz
N = int((tmax - tmin) * FS)

#repeat = 1
#k_summed = 100

# SFA parameters
TauOmega = 1. * ms                      # STDP window time constant constant
tauw = 10**1 * ms                       # Weights evolution time constant
tauAverage = 10**3. * ms
tauw_max = 0.1 * second

tauDerivation = 0.01 / fWiskott

# Output membrane potential
taum = 10 * ms                          # Membrane time constant

Vthr = 10. * mV                         # Threshold voltage
Vres = 0 * mV                           # Reset voltage
V0 = 1 * mV                             # Voltage transmitted to post-syn neuron at each spike


Noutputs = 4                            # Output neurons (useful to compare STDP windows)
MInputEnsemble = 1                      # Duplicate inputs in each output neurons
MOutputEnsemble = 1                     # Duplicate outputs

#Rigourous constant definitions
nu_av = (nuMin + nuMax)/2
nu0 = (nuMax - nuMin)/2
dV = Vthr - Vres

c = TauOmega * nu_av
c0 = c**2 * (exp(1/c) - 1)**2 * exp(-1/c)  # Rate constant to transform spike train into rates through an alpha spike-counting filter
tauRateMes = TauOmega/c0                   # Empirical rate. Time constant of window
tauRateSmooth = TauOmega/c0                # Smoothing for presentation of rates.


dtRecord = 1/fWiskott /22

print 'tauw: {0}, Duration: {1}, taum: {2}, nu:{3}, TauOmega:{4}, tauAverage:{5}, fWiskott:{6}'.format(tauw,tmax- tmin,taum,[nuMin,nuMax],TauOmega,tauAverage,fWiskott)
# -------
# Build INPUT
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,fWiskott=fWiskott)

Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')

# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# --------------------
# Code PARAMETERS
# --------------------


tCount = ti.timeCount(tCount,'Filter setup (s):')

G_pre = NeuronGroup(Ninputs * MInputEnsemble,
                    '''
                    x_train : 1

                    d x_av / dt = (x_train - x_av) / tauAverage : 1

                    x = (x_train - x_av) : 1
                    x_th : 1

                    d x_alpha /dt = (x_exp - x_alpha) / TauOmega : 1
                    d x_exp /dt = (x - x_exp) / TauOmega : 1

                    d D1x_al /dt = x_alpha / tauDerivation : 1
                    d D2x_al /dt = D1x_al / tauDerivation : 1

                    d D1x_th /dt = x_th / tauDerivation : 1
                    d D2x_th /dt = D1x_th / tauDerivation : 1

                    d x_al_var/ dt = (x_alpha**2 - x_al_var) / tauAverage : 1
                    d x_exp_var/ dt = (x_exp**2 - x_exp_var) / tauAverage : 1
                    d x_th_var/ dt = (x_th**2 - x_th_var) / tauAverage : 1


                    c1 = sqrt(x_th_var / clip(x_al_var,10**-2,10**2)) : 1
                    c2 = sqrt(x_th_var / clip(x_exp_var,10**-2,10**2)) : 1



                    dv /dt = 0 * Vthr/TauOmega : volt ''',

                    threshold='v>Vthr',
                    reset='v=Vres',
                    method="euler")


G_post = NeuronGroup(Noutputs * MOutputEnsemble,
                    '''
                    s : 1
                    d s_av /dt = (s - s_av) / tauAverage : 1
                    d s_var /dt = (s**2 - s_var) / tauAverage : 1

                    d s_alpha /dt = (s_exp  - s_alpha) / TauOmega : 1
                    d s_exp /dt = (s - s_exp) / TauOmega : 1
                    ''',
                    method="euler")

Cmax = 1
S = Synapses(G_pre, G_post,
             model= '''
                        xhat = a1 * c1_pre * x_alpha_pre + a2 * c2_pre * x_exp_pre : 1
                        xhat_lda = a1 * c1_pre * D2x_al_pre + a2 * c2_pre * x_exp_pre : 1
                        x_th_lda = a1 * D2x_th_pre + a2 * x_th_pre : 1

                        dx = xhat - x_th_pre : 1
                        dx_lda = xhat_lda - x_th_lda : 1

                        dw/dt = clip( (xhat_lda * s - gam * w * C_av) / tauw,- 1/tauw_max,1/tauw_max)  : 1
                        C_av = dx_lda*dx : 1

                        a1 : 1
                        a2 : 1

                        gam : 1
                        ''',

             pre='''
                        x_train_pre += 1''',
             method="euler",
             connect=True)

#------------------------------
# Define different STDP windows
#------------------------------

def  Nclass(i):
    return bt.string_neuron_class(MOutputEnsemble,i)
    #return S.indices[:,i]
# W SFA
S.a1[Nclass(0)] = '1'
S.a2[Nclass(0)] = '0'
S.gam[Nclass(0)] = '-1'

# W STDP asymmetric
if Noutputs > 1:
    S.a1[Nclass(1)] = '0'
    S.a2[Nclass(1)] = '1'
    S.gam[Nclass(1)] = '-1'

# W STDP symmetric
if Noutputs > 2:
    S.a1[Nclass(2)] = '1'
    S.a2[Nclass(2)] = '0'
    S.gam[Nclass(2)] = '0'

# W SFA splitted
if Noutputs > 3:
    S.a1[Nclass(3)] = '0'
    S.a2[Nclass(3)] = '1'
    S.gam[Nclass(3)] = '0'


#------------------------------
# Initialise variables
#------------------------------

a = np.random.randn(Ninputs * MInputEnsemble)
bt.init_weights_same_for_outputs(S,Ninputs,MInputEnsemble,a)
bt.norm_weights(S,Noutputs*MOutputEnsemble)



M = np.reshape(S.w,(Ninputs,Noutputs))

G_pre.x_av = nu_av*Dt
G_post.s_av = np.dot(G_pre.x_av,M)

G_pre.x_al_var = 1
G_pre.x_exp_var = 1
G_pre.s_var = 1

#------------------------------
# Define normalisation operation
#------------------------------

@network_operation(when='network_ope')
def getStimulus(t):

    # Get input current at time t and set rate
    intT = int(t / (Dt * second))
    x = inp.X[:,intT]
    G_pre.x_th = x
    rt = x *nu0 + nu_av
    G_pre.v = Vthr * Dt * rt / random.rand(1,Ninputs)

    # Normalize weights
    M = np.reshape(S.w,(Ninputs,Noutputs))
    sd = np.sqrt(np.sum(M**2,axis=0))
    S.w = (M / sd).ravel()

    # assign sum of weights
    #G_post.ws =  np.mean(G_pre.x_av) * np.sum(M,axis=0)
    G_post.s = np.dot(G_pre.x_train,M) - np.mean(G_pre.x_av) * np.sum(M,axis=0)

    # Add negative offset to set trains to zero mean
    #G_post.s_train = 0
    G_pre.x_train = 0


#------------------------------
# Define variable monitors
#------------------------------

mon_pre_x = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av','x_al_var'], record=True,when=Clock(dtRecord))
mon_post_s = StateMonitor(G_post, ['s','s_exp','s_alpha','s_av','s_var'], record=True,when=Clock(dtRecord))
mon_syn = StateMonitor(S,['w','C_av'], record=True,when=Clock(dtRecord))

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

tCount = ti.timeCount(tCount,'Brian setup (s):')
MagicNetwork.schedule = ['start','groups','network_ope','thresholds', 'synapses', 'resets', 'end']
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

#------------------------------
# Plots
#------------------------------


color = ["green","red","purple","blue","orange","cyan"]
win = ["d2 c","sig c","sig","d2","d2 c","sig c"]

print 'vars'
print [G_pre.x_al_var,G_pre.x_exp_var,G_pre.x_th_var]


Trec = range(0,len(mon_post_s.s_alpha[0,:])) * dtRecord


plt.figure()

# PRINT LOW PASSED RATES
plt.subplot(221)
for Ni in range(0,4):
    plt.plot(Trec,mon_pre_x[Ni].x_exp, label="%d" % Ni, color=color[Ni])
    plt.plot(Trec,mon_pre_x[Ni].x_av,'r--', label="%d" % Ni,linewidth=2, color=color[Ni])
    plt.plot(Trec,np.sqrt(mon_pre_x[Ni].x_al_var), label="%d" % Ni, linestyle='dotted', color=color[Ni])
legend()
plt.title('x')

# PRINT LOW PASSED RATES
plt.subplot(222)
plt.plot(inp.X.T)
plt.title('x')


# PRINT LOW PASSED RATES
plt.subplot(223)
for No in range(0,Noutputs):
    plt.plot(Trec,mon_post_s[No].s_alpha, label=S.a1[0,No], color=color[No])
    plt.plot(Trec,mon_post_s[No].s_av,'r--', label=S.a1[0,No], color=color[No], linewidth=2)
    plt.plot(Trec,np.sqrt(mon_post_s[No].s_var), label=S.a1[0,No], linestyle='dotted', color=color[No], linewidth=2)
legend()
plt.title('s')


#N_summed = int(1/(fWiskott*dtRecord))+1
#Trec_summed = range(0,N_summed)*dtRecord

plt.subplot(224)
#for No in range(0,Noutputs):
#    s_summed = np.zeros((N_summed,))
#    ind_end = len(Trec)
#    for k in range(1,k_summed+1):
#        t_start = tmax-k/fWiskott
#        ind = np.where(Trec >= t_start)
#        ind = ind[0][0]
#        s_summed[0:ind_end-ind] = mon_post_s[No].s_alpha[ind:ind_end]
#        ind_end = ind

#    plt.plot(Trec_summed.ravel(),s_summed.ravel()/k_summed,label=win[No],color=color[No])


for No in range(0,Noutputs):
    w = S.w[:,No]
    Nend = int(N*(1/fWiskott *5)/Texp)
    T_small = inp.T[0:Nend]
    plt.plot(T_small,np.dot(w,inp.X).ravel()[0:Nend],label=win[No],color=color[No])

legend()
plt.title('s (summed over periods)')

plt.show()


plt.figure()
for Ni in range(0,5):
    plt.subplot(231 + Ni)
    for No in range(0,Noutputs):
        ind = S.indices[Ni,No]
        plt.plot(mon_syn[ind].C_av.transpose(),label=win[No], color=color[No])
    legend()

    plt.title('C_av ' + `Ni`)

plt.show()

plt.figure()
for Ni in range(0,5):
    plt.subplot(231 + Ni)
    for No in range(0,Noutputs):
        ind = S.indices[Ni,No]
        plt.plot(mon_syn[ind].w.transpose(),label=win[No], color=color[No])
    legend()

    plt.title('w ' + `Ni`)

plt.show()


plt.figure()
for No in range(0,Noutputs):
    plt.subplot(100 + 10 * Noutputs +1 + No)
    plt.pcolor(S.w[:,No].reshape(Ninputs,1),cmap='BrBG',vmin=-1, vmax=1)
    plt.xlabel(win[No])
tight_layout()
plt.show()