from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt

import time

brian_prefs.codegen.target = 'weave'

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none


# Sampling parameters
tmin = 0 * second
tmax = 1 * second
FS = 44100 * Hz
N = int((tmax - tmin) * FS)

repeat = 10

# SFA parameters
TauOmega = 10 * ms                     # STDP window time constant constant

tauwstart = 10 * ms                    # Weights evolution time constant
tauwend = 1000 * ms                    # Weights evolution time constant

tauweq = 100 * ms

fWiskott = 10 * Hz                      # low frequency modulation in toy example

# Output membrane potential
taum = 1 * ms                           # Membrane time constant
tauRateMes = TauOmega                   # Empirical rate. Time constant of window
tauRateSmooth = TauOmega                # Smoothing for presentation of rates.

nuMin = fWiskott * 500                  # Minimum rate
nuMax = nuMin * 2                       # Max rate

Noutputs = 1                            # Output neurons (useful to compare STDP windows)
MInputEnsemble = 1                     # Duplicate inputs in each output neurons
MOutputEnsemble = 1                     # Duplicate outputs

Vthr = 10. * mV                         # Threshold voltage
Vres = 0 * mV                           # Reset voltage
V0 = 0.05 * mV                          # Voltage transmitted to post-syn neuron at each spike

#Rigourous constant definitions
nu_av = (nuMin + nuMax)/2
nu0 = (nuMax - nuMin)/2
r0 = nu_av
dV = Vthr - Vres

dtRecord = 1/(fWiskott * 11 *4)
dtRenorm = dtRecord
dtInputDefine = 1/FS


print 'tauw: {0}, M Inputs: {1}, Duration: {2}, nu_av:{3}, TauOmega: {4}, taum: {5}, tauweq: {6}'.format([tauwstart,tauwend],MInputEnsemble,tmax- tmin,nu_av,TauOmega,taum,tauweq)

# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType)
inp.sphereInputs()

Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')



# Inital connections and output
A0 = np.random.randn(Noutputs,inp.n)
S0 = np.random.randn(Noutputs,1)


# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# --------------------
# Code PARAMETERS
# --------------------

# Create STDP window filter (Omega)
al = exp(1)
Tau1 = TauOmega/(al - 1)
Tau2 = al * Tau1


tCount = ti.timeCount(tCount,'Filter setup (s):')

G_pre = NeuronGroup(Ninputs * MInputEnsemble,
                    '''
                    dx_1/dt = -(dt*nu_av + x_1) / Tau1 : 1
                    dx_2/dt = -(dt*nu_av + x_2) / Tau2 : 1

                    x : 1

                    dr_1/dt = - r_1 /tauRateMes : Hz
                    dr_2/dt = - r_2 /tauRateMes : Hz

                    rates : Hz
                    v = Vthr * rates * dt/rand() : volt''',
                    method='euler')

G_post = NeuronGroup(Noutputs * MOutputEnsemble,
                    '''
                    ds_1/dt = -(ws * dt*nu_av + s_1) / Tau1: 1
                    ds_2/dt = -(ws * dt*nu_av + s_2) / Tau2: 1

                    dv/dt =  - v/ taum : volt

                    dr_1/dt = - r_1 /tauRateMes : Hz
                    dr_2/dt = - r_2 /tauRateMes : Hz

                    s : 1
                    ws : 1''',
                    method='euler',
                    threshold='v>=Vthr',
                    reset='v=Vres')


S = Synapses(G_pre, G_post,
             model= '''
                        dw/dt = -(w - weq)/ tauw  : 1
                        dweq/dt = (x_pre_f * s_post + x_pre * s_post_f)/ tauweq  : 1

                        dtauw/dt = (tauwend - tauw) / (tmax - tmin) : second


                        al1 : 1
                        al2 : 1

                        bet1 : 1
                        bet2 : 1

                        x_pre_f = al1 * x_1_pre + al2 * x_2_pre : 1
                        s_post_f = bet1 * s_1_post + bet2 * s_2_post : 1
                        ''',

             pre='''
                        x_1_pre += dt*(1 - x_1_pre)/Tau1
                        x_2_pre += dt*(1 - x_2_pre)/Tau2

                        s_1_post += dt*(w - s_1_post)/Tau1
                        s_2_post += dt*(w - s_2_post)/Tau2

                        x_pre += 1
                        s_post += w

                        v_post += w * V0
                        r_pre += dt*(1/dt - r_pre) / tauRateMes  ''',

             post='''   r_post += dt*(1/dt - r_post) /tauRateMes  ''',
             method='euler',
             connect=True)

def  Nclass(i):
    return bt.string_neuron_class(MOutputEnsemble,i)


S.al1[Nclass(0)] = '-0.25'
S.al2[Nclass(0)] = '0.25'
S.bet1[Nclass(0)] = '-0.25'
S.bet2[Nclass(0)] = '0.25'


S.al1[Nclass(1)] = '0.5'
S.al2[Nclass(1)] = '0'
S.bet1[Nclass(1)] = '0.5'
S.bet2[Nclass(1)] = '0'




S.al1[Nclass(2)] = '0.25'
S.al2[Nclass(2)] = '-0.25'
S.bet1[Nclass(2)] = '0.25'
S.bet2[Nclass(2)] = '-0.25'


S.al1[Nclass(3)] = '0.5'
S.al2[Nclass(3)] = '0'
S.bet1[Nclass(3)] = '-0.5'
S.bet2[Nclass(3)] = '0'

S.al1[Nclass(4)] = '-0.5'
S.al2[Nclass(4)] = '0'
S.bet1[Nclass(4)] = '0.5'
S.bet2[Nclass(4)] = '0'



a = np.array([  5.25733831e-01, -8.50649128e-01, 1.29555750e-05, -1.44517907e-06, -4.34435917e-06])
#a = np.random.rand(Ninputs * MInputEnsemble)
bt.init_weights_same_for_outputs(S,Ninputs,MInputEnsemble,a)
bt.norm_weights(S,Noutputs*MOutputEnsemble)
bt.set_weight_sum(S,G_post,Noutputs*MOutputEnsemble)

G_pre.x_1 = - nu_av * Dt
G_pre.x_2 = - nu_av * Dt

G_post.s_1 = - G_post.ws * nu_av * Dt
G_post.s_2 = - G_post.ws * nu_av * Dt

G_pre.r = nu_av
G_post.r = nu_av
S.tauw = tauwstart
S.weq = S.w

@network_operation(when=Clock(dtInputDefine))
def getStimulus(t):

    # Get input current at time t
    intT = int(t / (Dt * second))
    rt = (inp.X[:,intT])*nu0 + nu_av
    for ir in range(0,Ninputs*MInputEnsemble):
        G_pre.rates[ir] = rt[bt.neuron_class(MInputEnsemble,ir)]


@network_operation(when=Clock(dtRenorm))
def normalize_along(t):
    # Renorm weights
    G_pre.x = - nu_av * Dt
    G_post.s = - G_post.ws * nu_av * Dt

    bt.norm_targeted_weights(S,Noutputs*MOutputEnsemble)
    bt.set_weight_sum(S,G_post,Noutputs*MOutputEnsemble)

mon_pre = StateMonitor(G_pre, 'r', record=True,when=Clock(dtRecord))

mon_post = StateMonitor(G_post, 'v', record=True,when=Clock(dtRecord))
mon_post_s = StateMonitor(G_post, 's', record=True,when=Clock(dtRecord))
mon_post_rates = StateMonitor(G_post, 'r', record=True,when=Clock(dtRecord))


syn_rec_ind = []
for i in range(0,3):
    Nilist = bt.get_neuron_class(i,Ninputs,MInputEnsemble)

    for No in range(0,Noutputs):
        idx_post = find(S._postsynaptic_idx == No)
        idx_pre = find(S._presynaptic_idx == Nilist[0])

        ind = np.intersect1d(idx_post,idx_pre)
        syn_rec_ind = hstack((syn_rec_ind,ind))

mon_syn = StateMonitor(S, 'w', record=syn_rec_ind,when=Clock(dtRecord))

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

Trec = range(0,len(mon_post.v[0,:])) * dtRecord
plt.figure(1)


# Compute rates from Spikes
sig_pre = bt.compute_rates(tauRateSmooth,inp,Trec,dtRecord,spikes_pre.it,Ninputs,MInputEnsemble,'alpha')



plt.subplot(331)
#plt.plot(Trec,(inp.X[0,:])*nu0 + nu_av, label="rates")
plt.plot(Trec,mon_pre[0].r, label="rates_mes")
plt.plot(Trec,sig_pre[:,0]*nu0 + nu_av, label="sig_pre")
legend()

# PRINT LOW PASSED RATES
plt.subplot(332)
for Ni in range(0,2):
    plt.plot(Trec,sig_pre[:,Ni]*nu0 + nu_av, label="%d" % Ni)
legend()
plt.title('Rates ')


plt.subplot(333)
i, t = spikes_post.it
plt.plot(t / ms, i, '.')


plt.subplot(334)
for Ni in range(0,Noutputs):
    plt.plot(Trec,mon_post[Ni].v.transpose(), label= "%d" % Ni)
legend()
plt.title('v_post ')
del sig_pre, spikes_pre


# Compute rates from Spikes
sig = bt.compute_rates(tauRateSmooth,inp,Trec,dtRecord,spikes_post.it,Noutputs,MOutputEnsemble,'alpha')


# PRINT LOW PASSED RATES
plt.subplot(335)
for Ni in range(0,Noutputs):
    plt.plot(Trec,sig[:,Ni]*nu0 + nu_av, label="%d" % Ni)
legend()
plt.title('Rates ')

sig = bt.smooth_rate_vector_gauss(tauRateSmooth,inp,Trec,dtRecord,mon_post_s.s,Noutputs)

# PRINT LOW PASSED SPECTRUMS
plt.subplot(336)
for Ni in range(0,Noutputs):
    plt.plot(Trec,sig[:,Ni], label= "%d" % Ni)
legend()
plt.title(' s smoothed ')



for i in range(0,3):
    plt.subplot(337 +i)

    Nilist = bt.get_neuron_class(i,Ninputs,MInputEnsemble)

    for No in range(0,Noutputs):
        idx_post = find(S._postsynaptic_idx == No)
        idx_pre = find(S._presynaptic_idx == Nilist[0])

        ind = np.intersect1d(idx_post,idx_pre)
        plt.plot(mon_syn[ind].w.transpose(),label="%d" % No)
    legend()

    plt.title('w ' + `i`)



plt.show()

