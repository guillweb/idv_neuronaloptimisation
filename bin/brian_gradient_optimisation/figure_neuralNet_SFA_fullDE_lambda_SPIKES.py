from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt
import time

brian_prefs.codegen.target = 'weave'


#-------------
# Parameters
#-------------

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none


# Sampling parameters
tmin = 0 * second
tmax = 10 * second
FS = 10000 * Hz
N = int((tmax - tmin) * FS)
inverse_period = 512/FS

repeat = 1
fWiskott = 10 * Hz


# SFA parameters
TauDerivation = 1 / FS         # Constant such that dx/dt = x/TauDerivation. Think sinus.

# Convergence time constants
tau_av = 10**2 * ms

tauw = 10**-4 * ms
tauw_max = 10**1 * ms

Noutputs = 4
V0 = 1. #100. * mV

# spiking params
TauOmega = 10 * ms
Vthr = 10 * mV

dtRecord = 1/(fWiskott * 11 * 8)

print 'tauw: {0}, Duration: {1}, TauDerivation: {2}'.format(tauw,tmax- tmin,TauDerivation)


# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType)
inp.sphereInputs()
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')


# -----------------------
builder = fb.Filter(inp.T,inverse_period)
inv_al = builder.getAlphaFilter(TauOmega).fourier_inverse()
X_in = inv_al.convolveSignal(inp.X)
tCount = ti.timeCount(tCount,'High pass input setup (s):')



# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# Create STDP window filter (Omega)
tCount = ti.timeCount(tCount,'Filter setup (s):')


G_pre = NeuronGroup(Ninputs, '''
                                    x_train : 1
                                    d x_av /dt = (x_train - x_av) / tau_average : 1

                                    d x_exp /dt = ( (x_train - x_av) - x_exp ) / tau_omega : 1
                                    d x_exp /dt = ( (x_train - x_av) - x_exp ) / tau_omega : 1

                                    Dx : 1
                                    D2x  : 1

                                    dv /dt = 0 * Vthr/TauOmega : volt ''',
                    threshold='v>Vthr',
                    reset='v=0 * volt')

G_post = NeuronGroup(Noutputs, '''  s : 1
                                    ''')

S = Synapses(G_pre, G_post, \
             model= ''' al : 1
                        bet : 1
                        gam : 1

                        STDP =  al * s_post * D2x_pre + bet * Dx_pre * s_post + gam * x_pre * s_post : 1
                        dC /dt = (STDP - C) /tau_av : 1

                        dw/dt  = C/  tauw : 1
                        s_post = w*x_pre : 1 (summed)
                         '''
             ,pre='x_train_pre += 1'
             ,connect=True)



S.al['j == 0'] = '1'
S.bet['j == 0'] = '0'
S.gam['j == 0'] = '0'

S.al['j == 1'] = '0'
S.bet['j == 1'] = '1'
S.gam['j == 1'] = '0'

S.al['j == 2'] = '0'
S.bet['j == 2'] = '0'
S.gam['j == 2'] = '1'


S.al['j == 3'] = '0'
S.bet['j == 3'] = '0'
S.gam['j == 3'] = '-.1'








# Renorm
a = np.random.rand(Ninputs)
bt.init_weights_same_for_outputs(S,Noutputs,1,a)
bt.norm_weights(S,Noutputs)

G_post.s = np.random.randn(1,1) * V0
G_pre.x = inp.X[:,0] * V0

@network_operation(when='start')
def getStimulusAndRenorm(t):
    # Get input current at time t
    intT = int(t / (Dt * second))
    x = inp.X[:,intT] * V0
    dx = (x - G_pre.x) * TauDerivation/Dt

    G_pre.D2x = (dx - G_pre.Dx) * TauDerivation/Dt
    G_pre.Dx = dx
    G_pre.x = x


    # Normalize weights
    M = np.reshape(S.w,(Ninputs,Noutputs))
    sd = np.sqrt(np.sum(M**2,axis=0))
    S.w = (M / sd).ravel()


#MagicNetwork.schedule = ['start','groups','network_ope','thresholds', 'synapses', 'resets', 'end']



mon_post = StateMonitor(G_post, 's', record=True,when=Clock(dtRecord))
mon_syn = StateMonitor(S, 'w', record=True,when=Clock(dtRecord))




tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

Trec = range(0,len(mon_post.s[0,:])) * dtRecord

print 'Weights: '
print S.w

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

color = ["green","red","purple","brown"]
win = [r'$\lambda_{SFA} = d_{(2)}$',r'$\lambda_{Classic} = d_{(1)} $',r'$\lambda_{Control+} = \delta_0$',r'$\lambda_{Control-} = - \delta_0$']

tstart = 0
tend = int(0.25 * second/ (tmax - tmin) * len(Trec))
for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(Trec[tstart:tend],mon_post.s[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 2)
    xlim([0,0.25])
    ylim([-0.01,0.01])
    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])

    xlabel(r't (s)')
    ylabel(r'$s$')

    tight_layout()
    savefig('../../figures/v_online_start_{0}.pdf'.format(i))
show()

plt.figure()
tstart = len(Trec) - int(0.25 * second/ (tmax - tmin) * len(Trec))
tend = len(Trec)
for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(Trec[tstart:tend],mon_post.s[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 2)
    plt.locator_params(axis = 'y', nbins = 2)
    xlim([tmax - 0.25*second,tmax])
    ylim([-0.01,0.01])
    xlabel(r't (s)')
    ylabel(r'$s$')

    tight_layout()
    savefig('../../figures/v_online_end_{0}.pdf'.format(i))
show()






for No in range(Noutputs-1,-1,-1):
    plt.figure()
    idx_post = find(S._postsynaptic_idx == No)

    plt.plot(Trec,mon_syn[idx_post].w.T,color=color[No],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'weights')
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)
    tight_layout()
    savefig('../../figures/w_online_{0}.pdf'.format(No))


    plt.figure()
    plt.pcolor(S.w[:,No].reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
    tight_layout()
    savefig('../../figures/w_flag_online_{0}.pdf'.format(No))


    show()


pass
