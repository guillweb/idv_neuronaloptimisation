from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt

import time

#brian_prefs.codegen.target = 'weave'


#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

# Sampling parameters
tmin = 0 * second
tmax = 20 * second
FS = 5000 * Hz
N = int((tmax - tmin) * FS)

repeat = 1
k_summed = 50

# SFA parameters
TauOmega = 1. * ms                      # STDP window time constant constant
tauw =  100 * ms                          # Weights evolution time constant

fWiskott = 10 * Hz                      # low frequency modulation in toy example


# Output membrane potential
taum = 10 * ms                          # Membrane time constant
tauRateMes = TauOmega                   # Empirical rate. Time constant of window
tauRateSmooth = TauOmega                # Smoothing for presentation of rates.

Vthr = 10. * mV                         # Threshold voltage
Vres = 0 * mV                           # Reset voltage
V0 = 1 * mV                             # Voltage transmitted to post-syn neuron at each spike

nuMin = fWiskott * 200                 # Minimum rate
nuMax = nuMin * 2                       # Max rate


Noutputs = 1                            # Output neurons (usefull to compare STDP windows)
MInputEnsemble = 1                      # Duplicate inputs in each ouput neurons
MOutputEnsemble = 1                     # Duplicate outputs



#Rigourous constant definitions
nu_av = (nuMin + nuMax)/2
nu0 = (nuMax - nuMin)/2
dV = Vthr - Vres

c = tauRateMes * nu_av
r0 = FS * c**2 * (exp(1/c) - 1)**2 * exp(-1/c)        # Rate constant to transform spike train into rates through an alpha spike-counting filter

dtRecord = 1/fWiskott / 40

print 'tauw: {0}, Duration: {1}, taum: {2}, nu:{3}, TauOmega:{4},'.format([tauw],tmax- tmin,taum,[nuMin,nuMax],TauOmega)
# -------
# Build INPUT
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType)
inp = il.Input.buildInput(tmin,tmax,N,inputType)

Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')

# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# --------------------
# Code PARAMETERS
# --------------------


tCount = ti.timeCount(tCount,'Filter setup (s):')

G_pre = NeuronGroup(Ninputs * MInputEnsemble,
                    '''
                    x : 1
                    d x_alpha /dt = (x_exp - x_alpha) / tauRateMes : 1
                    d x_exp /dt = (x - x_exp) / tauRateMes : 1

                    rates : Hz
                    v = Vthr * rates * dt/rand() : volt''',
                    threshold='v>Vthr',
                    reset='v=Vres')

G_post = NeuronGroup(Noutputs * MOutputEnsemble,
                    '''
                    s : 1

                    d s_alpha /dt = (s_exp - s_alpha) / tauRateMes : 1
                    d s_exp /dt = (s - s_exp) / tauRateMes : 1

                    ws : 1''')



S = Synapses(G_pre, G_post,
             model= "dw/dt = (x_pre*s_alpha_post + x_alpha_pre*s_post - 2*x_exp_pre*s_exp_post ) / tauw  : 1",
             pre='''
                        x_pre += r0*dt
                        s_post += w * r0*dt
                        ''',
             connect=True)

#------------------------------
# Initialise variables
#------------------------------

a = np.random.rand(Ninputs * MInputEnsemble)
#a = - np.array([-0.99557225, -0.0443698 ,  0.05636756, -0.03133591, -0.0518255 ])
bt.init_weights_same_for_outputs(S,Ninputs,MInputEnsemble,a)
bt.norm_weights(S,Noutputs*MOutputEnsemble)
bt.set_weight_sum(S,G_post,Noutputs*MOutputEnsemble)


#------------------------------
# Define normalisation operation
#------------------------------

@network_operation(when='network_ope')
def getStimulus(t):

    # Get input current at time t
    intT = int(t / (Dt * second))
    rt = (inp.X[:,intT])*nu0 + nu_av
    for ir in range(0,Ninputs*MInputEnsemble):
        G_pre.rates[ir] = rt[bt.neuron_class(MInputEnsemble,ir)]


    # Renorm weights
    bt.norm_weights(S,Noutputs*MOutputEnsemble)
    bt.set_weight_sum(S,G_post,Noutputs*MOutputEnsemble)

    G_post.s = - Dt * G_post.ws * nu_av
    G_pre.x = - Dt * nu_av


#------------------------------
# Define variable monitors
#------------------------------

mon_pre_x = StateMonitor(G_pre, ['x','x_exp','x_alpha'], record=True,when=Clock(dtRecord))
mon_post_s = StateMonitor(G_post, ['s','s_exp','s_alpha'], record=True,when=Clock(dtRecord))
mon_syn = StateMonitor(S, ['w'], record=True,when=Clock(dtRecord))

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

tCount = ti.timeCount(tCount,'Brian setup (s):')
MagicNetwork.schedule = ['start','groups','network_ope','thresholds', 'synapses', 'resets', 'end']
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')


#------------------------------
# Plots
#------------------------------


Trec = range(0,len(mon_post_s.s_alpha[0,:])) * dtRecord


plt.figure()

# PRINT LOW PASSED RATES
plt.subplot(221)
for Ni in range(0,4):
    plt.plot(Trec,mon_pre_x[Ni].x_alpha, label="%d" % Ni)
legend()
plt.title('x')

# PRINT LOW PASSED RATES
plt.subplot(223)
plt.plot(Trec,mon_post_s[0].s_alpha, label="%d" % 0)
legend()
plt.title('s')

# summed repreated pattern

N_summed = int(1/(fWiskott*dtRecord))+1
Trec_summed = range(0,N_summed)*dtRecord
s_summed = np.zeros((N_summed,))
for k in range(1,k_summed+1):
    t_start = tmax-k/fWiskott-dtRecord
    ind = np.where(Trec >= t_start)
    Npat = min(ind[0][0]+N_summed,np.shape(mon_post_s[0].s_alpha)[0])
    s_summed[0:Npat] = mon_post_s[0].s_alpha[ind[0][0]:ind[0][0]+N_summed]

plt.subplot(224)
plt.plot(Trec_summed.ravel(),s_summed.ravel()/k_summed, label="%d" % 0)
legend()
plt.title('s (summed over periods)')
plt.show()

plt.figure()
for i in range(0,5):
    plt.subplot(151 +i)
    for No in range(0,Noutputs):
        ind = S.indices[i,No]
        plt.plot(mon_syn[ind].w.transpose(),label="%d" % No)
    legend()

    plt.title('w ' + `i`)



plt.show()

for i in range(0,Noutputs):
    plt.figure()
    plt.pcolor(S.w[:,No].reshape(Ninputs,1),cmap='BrBG',vmin=-1, vmax=1)
    tight_layout()

plt.show()