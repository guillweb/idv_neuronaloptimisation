from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt
import time

brian_prefs.codegen.target = 'weave'



#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none


# Sampling parameters
tmin = 0 * second
tmax = 5 * second
FS = 22050 * Hz

repeat = 1
fWiskott = 10 * Hz


# SFA parameters
TauOmega = 10 * ms                   # STDP window time constant constant

# Output membrane potential
taum = 1. * ms                      # This low passes the inputs. Slow feature to capture have to vary slower

tauweq = 10 * ms

tauwstart = 10**2 * ms
tauwend = 10**2 * ms

#tauweqstart = 10**1 * ms
#tauweqend = 10**1 * ms

alRandom = 0.1                    # Ratio between TauOpt and TauOmega

Noutputs = 5
V0 = 100. * mV

dtRecord = 1/(fWiskott * 11 * 8)
dtRenorm = 1/FS
dtInputDefine = 1/FS

print 'tauw: {0}, Duration: {1}, TauOmega: {2}, taum: {3}, tauweq: {4}'.format([tauwstart,tauwend],tmax- tmin,TauOmega,taum,tauweq)
# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [fWiskott]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 200

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]




# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam,repeat)
inp.sphereInputs()
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')



# Inital connections and output
A0 = np.random.randn(Noutputs,inp.n)
S0 = np.random.randn(Noutputs,1)



# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

stimulus = inp.getTimedArray(V0)

# Create STDP window filter (Omega)
al = exp(1)
Tau1 = TauOmega/(al - 1)
Tau2 = al * Tau1
RFinput = rf.recursiveFilters(Tau1,Tau2,2,Dt,Ninputs)
RFoutput = rf.recursiveFilters(Tau1,Tau2,2,Dt,Noutputs)


tCount = ti.timeCount(tCount,'Filter setup (s):')


G_pre = NeuronGroup(Ninputs, '''    #dv/dt = (-v + v_in)/taum + alRandom *V0 * xi/taum**0.5 : volt
                                    dv/dt = (-v + v_in)/taum  : volt
                                    v_in : volt

                                    dv_1/dt = - (v_1 + v_in) / Tau1 : volt
                                    dv_2/dt = - (v_2 + v_in) / Tau2 : volt

                             ''', method="euler")

G_post = NeuronGroup(Noutputs, '''  dv/dt = - (v - vhat)  / taum : volt
                                    vhat : volt

                                    dvhat_1/dt = - (vhat_1 + vhat) / Tau1 : volt
                                    dvhat_2/dt = - (vhat_2 + vhat) / Tau2 : volt

                                    ''', method="euler")

S = Synapses(G_pre, G_post, \
             model= ''' al1 : 1
                        al2 : 1

                        vOmega =  al1 * v_1_pre/Tau1 + al2 * v_2_pre/Tau2 : volt * Hz

                        bet1 : 1
                        bet2 : 1

                        vhatOmega = bet1 * vhat_1_post/Tau1 + bet2 * vhat_2_post/Tau2 : volt * Hz

                        STDP = (vOmega * vhat_post + v_pre * vhatOmega) * TauOmega / V0**2 : 1


                        dw/dt = (weq - w)/tauw : 1
                        dweq/dt  = STDP/ tauweq : 1
                        dtauw/dt = (tauwend - tauw)/(tmax - tmin) : second


                        vhat_post = w*v_in_pre : volt (summed)
                         '''
             ,method=euler
             ,connect=True)


S.al1['j == 0'] = '-0.25'
S.al2['j == 0'] = '0.25'
S.bet1['j == 0'] = '-0.25'
S.bet2['j == 0'] = '0.25'

S.al1['j == 1'] = '0.5'
S.al2['j == 1'] = '0'
S.bet1['j == 1'] = '-0.5'
S.bet2['j == 1'] = '0'

S.al1['j == 2'] = '0.5'
S.al2['j == 2'] = '0'
S.bet1['j == 2'] = '0.5'
S.bet2['j == 2'] = '0'








# Renorm


#a = np.array([[-5.25733831e-01], [  8.50649128e-01], [ -1.29555750e-05], [  1.44517907e-06], [  4.34435917e-06]]).ravel()
a = np.random.rand(Ninputs)
bt.init_weights_same_for_outputs(S,Noutputs,1,a)
bt.norm_weights(S,Noutputs)

G_post.vhat = np.random.randn(1,1) * V0
G_post.vhat_1 = G_post.vhat
G_post.vhat_2 = G_post.vhat
G_post.v = G_post.vhat

G_pre.v = inp.X[:,0] * V0
G_pre.v_1 = G_pre.v
G_pre.v_2 = G_pre.v
G_pre.v_in = G_pre.v

S.tauw = tauwstart
S.weq = S.w
@network_operation(dt=dtInputDefine)
def getStimulusAndRenorm(t):
    # Get input current at time t
    intT = int(t / (Dt * second))
    G_pre.v_in = inp.X[:,intT] * V0

@network_operation(dt=dtRenorm)
def normalize_along(t):
    # Renorm weights
    bt.norm_targeted_weights(S,Noutputs)






mon_pre = StateMonitor(G_pre, 'v', record=True,dt=dtRecord)
mon_post = StateMonitor(G_post, 'vhat', record=True,dt=dtRecord)
mon_syn = StateMonitor(S, 'w', record=True,dt=dtRecord)




tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

Trec = range(0,len(mon_post.vhat[0,:])) * dtRecord

print 'Weights: '
print S.w

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')


plt.figure()
tstart = 0
tend = int(0.25 * second/ (tmax - tmin) * len(Trec))
plt.plot(Trec[tstart:tend],mon_post.vhat[2,tstart:tend],color='purple',label=r'$\textcolor{purple}{W_{control}} $',linewidth=2.0)
plt.plot(Trec[tstart:tend],mon_post.vhat[1,tstart:tend],color='red',label=r'$\textcolor{red}{W_{STDP}} $',linewidth=2.0)
plt.plot(Trec[tstart:tend],mon_post.vhat[0,tstart:tend],color='green',label=r'$\textcolor{green}{W_{SFA}} $',linewidth=2.0)
plt.locator_params(axis = 'x', nbins = 4)
plt.locator_params(axis = 'y', nbins = 2)
xlim([0,0.25])
ylim([-0.2,0.2])

xlabel(r't (s)')
ylabel(r'$s$')
tight_layout()
savefig('../../figures/v_online_start.pdf')
show()

plt.figure()
tstart = len(Trec) - int(0.25 * second/ (tmax - tmin) * len(Trec))
tend = len(Trec)
plt.plot(Trec[tstart:tend],mon_post.vhat[2,tstart:tend],color='purple',label=r'$\textcolor{purple}{W_{control}} $',linewidth=2.0)
plt.plot(Trec[tstart:tend],mon_post.vhat[1,tstart:tend],color='red',label=r'$\textcolor{red}{W_{STDP}} $',linewidth=2.0)
plt.plot(Trec[tstart:tend],mon_post.vhat[0,tstart:tend],color='green',label=r'$\textcolor{green}{W_{SFA}} $',linewidth=2.0)
plt.locator_params(axis = 'x', nbins = 2)
plt.locator_params(axis = 'y', nbins = 2)
xlim([tmax - 0.25*second,tmax])
ylim([-0.2,0.2])
xlabel(r't (s)')
ylabel(r'$s$')
tight_layout()
savefig('../../figures/v_online_end.pdf')
show()




color = ["green","red","purple"]
win = ["d2","d1","sig"]



for No in range(0,3):
    plt.figure()
    idx_post = find(S._postsynaptic_idx == No)

    plt.plot(Trec,mon_syn[idx_post].w.T,color=color[No],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'weights')
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)
    tight_layout()
    savefig('../../figures/w_online_{0}.pdf'.format(win[No]))


    plt.figure()
    plt.pcolor(S.w[:,No].reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
    tight_layout()
    savefig('../../figures/w_flag_online_{0}.pdf'.format(win[No]))


    show()


pass
