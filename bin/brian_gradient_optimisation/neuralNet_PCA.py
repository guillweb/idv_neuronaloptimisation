from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter
from bin.tools.spectralAnalysis import getSpectrum


# ------------- Stimulus definition

Ndelays = 100
Noutputs = 1

# Sampling and time
Npoints = 500
Texp = float(5.)
FS = Npoints/Texp
Dt = 1/FS
t = 1/FS * np.matrix(range(1, Npoints+1))

# Cosines coeff
f0 = 31.
f1 = 77.
q1 = 2
qrand = 0.05

# Construction
T = t
for iDel in range(1, Ndelays):
    T = np.vstack((T, t - iDel * Dt))

X = np.cos(2* np.pi * f0 *T ) \
    + q1 * np.sin(2* np.pi * f1 *T ) \
    + qrand * np.random.randn(Ndelays,Npoints)

# Non-linear expansion
#X = Optimiser_filter.NonLinearExpanded(X).addQuadratic()
X = Optimiser_filter.NonLinearExpanded(X).normalize()
Ninputs = np.shape(X)[0]

# Inital connections and output
A0 = np.random.randn(Noutputs,np.shape(X)[0])
S0 = np.random.randn(Noutputs,1)


# ------------- Brian
taum = 10. * ms
tauw = 10**4. * ms
V0 = 100. * mV

tauMemory = 10**3 * ms


alMem = exp(- Dt *second / tauMemory)



# Moving average definition
corr = np.zeros( Noutputs*Ninputs ) * mV**2

#ta = TimedArray(np.array(X.transpose()) * V0,dt= Dt * second)

G_pre = NeuronGroup(Ninputs, 'v : volt')

G_post = NeuronGroup(Noutputs, '''   dv/dt = - (v - vhat)  / taum : volt
                                vhat   : volt ''')


S = Synapses(G_pre, G_post, \
             model= ''' dw/dt = - (corrhat - corr )/ (tauw * V0**2) : 1
                        corrhat  : volt**2
                        corr  : volt**2 '''
             ,connect=True)


# State initialisation
x = np.array(X[:, 0]) * V0
vHat0 = np.dot(A0,x)
G_post.vhat =  vHat0 # estimator of v
G_post.v =S0 * V0

S.corrhat = (vHat0 * x.transpose()).reshape(Ninputs*Noutputs)
S.corr = corr

S.w = np.random.randn(Ninputs*Noutputs)
G_post.v = np.random.randn(Noutputs) * V0


@network_operation(when='start')
def update_grads(t):


    # Get input current at time t
    intT = int(t / (Dt * second))
    G_pre.v = np.array(X[:, intT]) * V0


    # Get weights in a matrix form
    A = np.array(S.w).reshape((Noutputs,Ninputs))
    vHat = np.dot(A,G_pre.v)

    # Compute correlations
    S.corrhat *= alMem**2
    S.corrhat += (vHat * x.transpose()).reshape(Ninputs*Noutputs) # estimator of corr = < s, x >

    # Estimator
    S.corr *= alMem**2
    S.corr += (x * G_post.v).transpose().reshape(Noutputs*Ninputs)


    G_post.vhat = vHat # estimator of v


mon_pre = StateMonitor(G_pre, 'v', record=True)
mon_post = StateMonitor(G_post, 'v', record=True)
mon_syn = StateMonitor(S, 'w', record=True)
run(Texp * second)


plt.figure(1)
for i in range(0,min(Ninputs,3)):
    plt.subplot(331 +i)
    plt.plot(mon_pre[i].v.transpose())
    plt.title('v_pre ' + `i`)
for i in range(0,min(Noutputs,2)):
    plt.subplot(334 +i)
    plt.plot(mon_post[i].v.transpose())
    plt.title('v_post ' + `i*Ninputs`)

    fs = np.shape(mon_post[0].v)[0] / Texp
    (frq,spec) = getSpectrum(mon_post[0].v.transpose(),fs)
    plt.subplot(336)
    plt.xscale('log')
    plt.plot(frq,spec,'r')



for i in range(0,min(Noutputs*Ninputs,3)):
    plt.subplot(337 +i)
    plt.plot(mon_syn[i].w.transpose())
    plt.title('w ' + `i`)



plt.show()