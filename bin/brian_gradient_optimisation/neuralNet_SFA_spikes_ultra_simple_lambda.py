from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import tools as bt

import time

brian_prefs.codegen.target = 'weave'

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin


fWiskott = 1 * Hz                      # low frequency modulation in toy example
nuMin = 10**3 * Hz                      # Minimum rate
nuMax = nuMin * 2                       # Max rate

# Sampling parameters
tmin = 0 * second
tmax = 10 * second
FS =  10**4 * Hz
N = int((tmax - tmin) * FS)
inverse_period = 512/FS


# SFA parameters
TauOmega = 10. * ms                      # STDP window time constant constant
tauw = 10**1 * ms                       # Weights evolution time constant
tauAverage = 3. * second



Noutputs = 4                            # Output neurons (useful to compare STDP windows)

#Rigourous constant definitions
nu_av = (nuMin + nuMax)/2
nu0 = (nuMax - nuMin)/2

Vthr = 10. * mV                         # Threshold voltage
Vres = 0 * mV                           # Reset voltage


dtRecord = 1/fWiskott /100

print 'tauw: {0}, Duration: {1}, nu:{2}, TauOmega:{3}, tauAverage:{4}, fWiskott:{5}'.format([tauw],tmax- tmin,[nuMin,nuMax],TauOmega,tauAverage,fWiskott)
# -------
# Build INPUT
# -------

tCount = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,fWiskott=fWiskott)


# -----------------------
builder = fb.Filter(inp.T,inverse_period)
inv_al = builder.getAlphaFilter(TauOmega).fourier_inverse()
X_in = inv_al.convolveSignal(inp.X)
tCount = ti.timeCount(tCount,'High pass input setup (s):')



Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')

# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# --------------------
# Code PARAMETERS
# --------------------


tCount = ti.timeCount(tCount,'Filter setup (s):')

G_pre = NeuronGroup(Ninputs,
                    '''
                    x_train : 1
                    d x_av / dt = (x_train - x_av) / tauAverage : 1
                    x = (x_train - x_av) : 1

                    d x_alpha /dt = (x_exp - x_alpha) / TauOmega : 1
                    d x_exp /dt = (x - x_exp) / TauOmega : 1

                    x_alpha_old : 1
                    Dx : 1
                    D2x : 1


                    dv /dt = 0 * Vthr/TauOmega : volt ''',

                    threshold='v>Vthr',
                    reset='v=Vres',
                    method="euler")


G_post = NeuronGroup(Noutputs,
                    '''
                    s_train : 1
                    s_av : 1
                    s = (s_train - s_av) : 1

                    Ds : 1

                    d s_alpha /dt = (s_exp  - s_alpha) / TauOmega : 1
                    d s_exp /dt = (s - s_exp) / TauOmega : 1
                    ''',
                    method="euler")


S = Synapses(G_pre, G_post,
             model= '''
                        s_train_post = w * x_train_pre: 1 (summed)
                        s_av_post = w * x_av_pre: 1 (summed)

                        dw/dt = STDP / tauw  : 1

                        STDP =  al * s_alpha_post * D2x_pre + bet * Dx_pre * s_alpha_post + gam * x_alpha_pre * s_alpha_post : 1

                        al : 1
                        bet : 1
                        gam : 1

                        ''',

             pre='''
                        x_train_pre += 1''',
             method="euler",
             connect=True)

S.al['j == 0'] = '1'
S.bet['j == 0'] = '0'
S.gam['j == 0'] = '0'

S.al['j == 1'] = '0'
S.bet['j == 1'] = '1'
S.gam['j == 1'] = '0'

S.al['j == 2'] = '0'
S.bet['j == 2'] = '0'
S.gam['j == 2'] = '1'


S.al['j == 3'] = '0'
S.bet['j == 3'] = '0'
S.gam['j == 3'] = '-1'



#------------------------------
# Initialise variables
#------------------------------

a = np.random.randn(Ninputs)
bt.init_weights_same_for_outputs(S,Ninputs,1,a)
bt.norm_weights(S,Noutputs)



M = np.reshape(S.w,(Ninputs,Noutputs))

G_pre.x_av = nu_av*Dt
G_post.s_av = np.dot(G_pre.x_av,M)


#------------------------------
# Define normalisation operation
#------------------------------

@network_operation(when='network_ope')
def getStimulus(t):

    # Get input current at time t and set rate
    intT = int(t / (Dt * second))
    intT = min(intT,N-1)

    dx = G_pre.x_alpha - G_pre.x_alpha_old
    G_pre.D2x = dx - G_pre.Dx
    G_pre.Dx = dx
    G_pre.x_alpha_old = G_pre.x_alpha

    x = X_in[:,intT]
    G_pre.x_th = x
    rt = x *nu0 + nu_av
    G_pre.v = Vthr * Dt * rt / random.rand(1,Ninputs)

    # Normalize weights
    M = np.reshape(S.w,(Ninputs,Noutputs))
    sd = np.sqrt(np.sum(M**2,axis=0))
    S.w = (M / sd).ravel()

    # assign sum of weights
    #G_post.ws =  np.mean(G_pre.x_av) * np.sum(M,axis=0)
    #G_post.s = np.dot(G_pre.x_train,M) - np.mean(G_pre.x_av) * np.sum(M,axis=0)

    # Add negative offset to set trains to zero mean
    #G_post.s_train = 0
    G_pre.x_train = 0


#------------------------------
# Define variable monitors
#------------------------------

mon_pre_x = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av'], record=True,when=Clock(dtRecord))
mon_post_s = StateMonitor(G_post, ['s','s_exp','s_alpha','s_av'], record=True,when=Clock(dtRecord))
mon_syn = StateMonitor(S,['w'], record=True,when=Clock(dtRecord))

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

tCount = ti.timeCount(tCount,'Brian setup (s):')
MagicNetwork.schedule = ['start','groups','network_ope','thresholds', 'synapses', 'resets', 'end']
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

#------------------------------
# Plots
#------------------------------


tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp)
tCount = ti.timeCount(tCount,'Brian run (s):')

Trec = range(0,len(mon_post_s.s[0,:])) * dtRecord

print 'Weights: '
print S.w

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

color = ["green","red","purple","brown"]
win = [r'$\lambda_{SFA} = d_{(2)}$',r'$\lambda_{Classic} = d_{(1)} $',r'$\lambda_{Control+} = \delta_0$',r'$\lambda_{Control-} = - \delta_0$']

tstart = 0
tend = int(0.25 * second/ (tmax - tmin) * len(Trec))
for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(Trec[tstart:tend],mon_post_s.s[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 2)

    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])

    xlabel(r't (s)')
    ylabel(r'$s$')

    tight_layout()
    savefig('../../figures/v_spiking_start_{0}.pdf'.format(i))
show()

plt.figure()
tstart = len(Trec) - int(0.25 * second/ (tmax - tmin) * len(Trec))
tend = len(Trec)
for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(Trec[tstart:tend],mon_post_s.s[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 2)
    plt.locator_params(axis = 'y', nbins = 2)

    xlabel(r't (s)')
    ylabel(r'$s$')

    tight_layout()
    savefig('../../figures/v_spiking_end_{0}.pdf'.format(i))
show()




for No in range(Noutputs-1,-1,-1):
    plt.figure()
    idx_post = find(S._postsynaptic_idx == No)

    plt.plot(Trec,mon_syn[idx_post].w.T,color=color[No],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'weights')
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)
    tight_layout()
    savefig('../../figures/w_spiking_{0}.pdf'.format(No))


    #plt.figure()
    #plt.pcolor(S.w[:,No].reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
    #tight_layout()
    #savefig('../../figures/w_flag_spiking_{0}.pdf'.format(No))


    show()

plt.figure()

# PRINT LOW PASSED RATES
plt.subplot(221)
for Ni in range(0,Noutputs):
    plt.plot(Trec,mon_pre_x[Ni].x_exp, label="%d" % Ni, color=color[Ni])
    plt.plot(Trec,mon_pre_x[Ni].x_av,'r--', label="%d" % Ni,linewidth=2, color=color[Ni])
legend()
plt.title('x')

# PRINT LOW PASSED RATES
plt.subplot(222)
plt.plot(inp.X.T)
plt.title('x')


# PRINT LOW PASSED RATES
plt.subplot(223)
for No in range(0,Noutputs):
    plt.plot(Trec,mon_post_s[No].s_alpha, label=No, color=color[No])
    plt.plot(Trec,mon_post_s[No].s_av,'r--', label=No, color=color[No], linewidth=2)
legend()
plt.title('s')


#N_summed = int(1/(fWiskott*dtRecord))+1
#Trec_summed = range(0,N_summed)*dtRecord

plt.subplot(224)
#for No in range(0,Noutputs):
#    s_summed = np.zeros((N_summed,))
#    ind_end = len(Trec)
#    for k in range(1,k_summed+1):
#        t_start = tmax-k/fWiskott
#        ind = np.where(Trec >= t_start)
#        ind = ind[0][0]
#        s_summed[0:ind_end-ind] = mon_post_s[No].s_alpha[ind:ind_end]
#        ind_end = ind

#    plt.plot(Trec_summed.ravel(),s_summed.ravel()/k_summed,label=win[No],color=color[No])


for No in range(0,Noutputs):
    w = S.w[:,No]
    Nend = int(N*(1/fWiskott *5)/Texp)
    T_small = inp.T[0:Nend]
    plt.plot(T_small,np.dot(w,inp.X).ravel()[0:Nend],label=win[No],color=color[No])

legend()
plt.title('s (summed over periods)')

plt.show()



plt.figure()
for Ni in range(0,5):
    plt.subplot(231 + Ni)
    for No in range(0,Noutputs):
        ind = S.indices[Ni,No]
        plt.plot(mon_syn[ind].w.transpose(),label=win[No], color=color[No])
    legend()

    plt.title('w ' + `Ni`)

plt.show()


plt.figure()
for No in range(0,Noutputs):
    plt.subplot(100 + 10 * Noutputs +1 + No)
    plt.pcolor(S.w[:,No].reshape(Ninputs,1),cmap='BrBG',vmin=-1, vmax=1)
    plt.xlabel(win[No])
tight_layout()
plt.show()