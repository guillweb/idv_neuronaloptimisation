__author__ = 'guillaume'


from brian2 import *

class brianOpt(object):
    def __init__(self,inp,filter):
        self.input = inp
        self.filter = filter

        # Unity of the input
        self.V0 = 1/ float(self.input.N) * 100. * mV

    def initNeuralNetwork(self,Ninputs,Noutputs):

        self.Ninputs = Ninputs
        self.Noutputs = Noutputs

        # Moving average definition
        self.corr = np.zeros( Noutputs*Ninputs ) * mV**2

        #ta = TimedArray(np.array(X.transpose()) * V0,dt= Dt * second)

        self.G_pre = NeuronGroup(Ninputs, 'v : volt')
        self.G_post = NeuronGroup(Noutputs, '''   dv/dt = - (v - vhat)  / taum : volt
                                        vhat   : volt ''')

        self.S = Synapses(self.G_pre, self.G_post, \
                     model= ''' dw/dt = - (corrhat - corr )/ (tauw * V0**2) : 1
                                corrhat  : volt**2
                                corr  : volt**2 '''
                     ,connect=True)

    def setInitialState(self):


        # Inital connections and output
        A0 = np.random.randn(self.Noutputs,self.input.n)
        S0 = np.random.randn(self.Noutputs,1)



        # State initialisation
        x = np.array(self.input.X[:, 0]) * self.V0
        vHat0 = np.dot(A0,x)
        self.G_post.vhat =  vHat0 # estimator of v
        self.G_post.v = S0 * self.V0

        self.S.corrhat = (vHat0 * x.transpose()).reshape(self.Ninputs*self.Noutputs)
        self.S.corr = self.corr

        self.S.w = np.random.randn(self.Ninputs*self.Noutputs)
        self.G_post.v = np.random.randn(self.Noutputs) * self.V0

    def setNetworkOp(self):

        @network_operation(when='start')
        def update_grads(t):


            # Get input current at time t
            intT = int(t / (Dt * second))
            G_pre.v = np.array(X[:, intT]) * V0


            # Get weights in a matrix form
            A = np.array(S.w).reshape((Noutputs,Ninputs))
            vHat = np.dot(A,G_pre.v)

            # Compute correlations
            S.corrhat *= alMem**2
            S.corrhat += (vHat * x.transpose()).reshape(Ninputs*Noutputs) # estimator of corr = < s, x >

            # Estimator
            S.corr *= alMem**2
            self.S.corr += (x * G_post.v).transpose().reshape(Noutputs*Ninputs)


            self.G_post.vhat = vHat # estimator of v

        return update_grads


    def run(self):


        mon_pre = StateMonitor(self.G_pre, 'v', record=True)
        mon_post = StateMonitor(self.G_post, 'v', record=True)
        mon_syn = StateMonitor(self.S, 'w', record=True)

        Texp = self.input.tmax- self.input.tmin
        run(Texp * second)