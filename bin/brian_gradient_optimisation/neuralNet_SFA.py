from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

brian_prefs.codegen.target = 'weave'

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none


# Sampling parameters
tmin = 0 * second
tmax = 3 *second
FS = 5000 * Hz

repeat = 1

# SFA parameters
TauOmega = 0.5 * ms                      # STDP window time constant constant
TauPeriod = TauOmega * 50                # Filer length (Limited for memory error issue)

# Output membrane potential
taum = 10. * ms
invAlOpt = 1

Noutputs = 1
V0 = 100. * mV
tauMemory = 100 * ms

# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 100

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]




# -------
# Solver
# -------

t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam,repeat)
Texp = inp.tmax - inp.tmin
t = ti.timeCount(t,'Input setup (s):')

# Create STDP window filter (Omega)
intTmem = int(tauMemory / inp.dt)
Tmemory = range(0,intTmem) * inp.dt
Tmemory -= Tmemory[intTmem/2]
builder = fb.FilterBuilder(Tmemory,TauPeriod)
F = builder.getDerivativeSquaredFilter(TauOmega)
t = ti.timeCount(t,'Filter setup (s):')


# Inital connections and output
A0 = np.random.randn(Noutputs,inp.n)
S0 = np.random.randn(Noutputs,1)


# ------------- Brian
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n
tauw = invAlOpt * Dt



G_pre = NeuronGroup(Ninputs, 'v : volt')
G_post = NeuronGroup(Noutputs, '''   dv/dt = - (v - vhat)  / taum : volt
                                vhat   : volt ''')

S = Synapses(G_pre, G_post, \
             model= ''' dw/dt = - grad / tauw : 1
                        grad  : 1 '''
             ,connect=True)


# State initialisation
xt = np.array(inp.X[:, 0]) * V0
S.grad = np.zeros((Ninputs*Noutputs,))

Swinit = np.random.randn(Ninputs*Noutputs)
W = np.array(Swinit).reshape((Noutputs,Ninputs))
# Renorm
W = (W.T - mean(W,axis=1)).T
sd = np.maximum(std(W,axis=1),10 ** -15)
W = (W.T / sd).T
S.w = W.reshape((Noutputs*Ninputs,))
G_post.vhat = np.dot(W,G_pre.v)

# Get Kernel matrix
K = F.matrix.todense()


Nmem = builder.N

@network_operation(when=Clock(dt=Dt))
def update_grads(t):


    # Get input current at time t
    intT = int(t / (Dt * second))
    Nstart = max(0,intT-Nmem)
    Nstop = min(inp.N,intT)
    x = inp.X[:,Nstart:Nstop]



    # Compute A = X K^T X^T, whatchout if the input is shorter
    if Nstop - Nstart < Nmem:
        Ktrunc = K[0:Nstop-Nstart,0:Nstop-Nstart]
        A = Ktrunc.dot(x.T)
    else:
        A = K.dot(x.T)
    A = A.T.dot(x.T)
    A /= float(Nmem)**2


    xt = inp.X[:, intT]
    G_pre.v = np.array(xt) * V0


    # Get weights in a matrix 
    W = np.array(S.w).reshape((Noutputs,Ninputs))
    
    # Renorm
    W = (W.T - mean(W,axis=1)).T
    sd = np.maximum(std(W,axis=1),10 ** -15)
    W = (W.T / sd).T
    S.w = W.reshape((Noutputs*Ninputs,))
    G_post.vhat = np.dot(W,G_pre.v)

    # Grad of the cost function: X K X^T W
    S.grad = W.dot(A)




mon_pre = StateMonitor(G_pre, 'v', record=True)
mon_post = StateMonitor(G_post, 'v', record=True)
mon_syn = StateMonitor(S, 'w', record=True)
t = ti.timeCount(t,'Brian setup (s):')
run(Texp)
t = ti.timeCount(t,'Brian run (s):')


plt.figure(1)
for i in range(0,min(Ninputs,3)):
    plt.subplot(331 +i)
    j = min(Ninputs-1,i*(Ninputs+1)/2)
    plt.plot(mon_pre[j].v.transpose())
    plt.title('v_pre ' + `j`)
for i in range(0,min(Noutputs,2)):
    plt.subplot(334 +i)
    plt.plot(mon_post[i].v.transpose())
    plt.title('v_post ' + `i*Ninputs`)

    fs = np.shape(mon_post[0].v)[0] / Texp
    (frq,spec) = to.getSpectrum(mon_post[0].v.transpose(),fs)
    plt.subplot(336)
    plt.xscale('log')
    plt.plot(frq,spec,'r')



for i in range(0,3):
    plt.subplot(337 +i)
    j = min(Noutputs-1,i*(Noutputs+1)/2)
    plt.plot(mon_syn[j].w.transpose())
    plt.title('w ' + `j`)



plt.show()
