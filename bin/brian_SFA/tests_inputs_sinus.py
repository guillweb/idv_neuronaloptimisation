from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time

#brian_prefs.codegen.target = 'weave'
set_device('cpp_standalone')
#-------------
# Parameters
#-------------

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

pre_filter = False
fWiskott = 1. * Hz

# Sampling parameters
tmin = 0 * second
tmax = 100 * second
FS = 10000 * Hz
N = int((tmax - tmin) * FS)


nu0 = 200 * Hz
nu_av = 300 * Hz

tau_omega = 10 * ms
inverse_period = 511/FS
tau_average = 5 * second

dtRecord = 1/FS


Vthr = 10 * mV

print 'fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)


# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input(tmin,tmax,N)
inp.set_wiskott_classic(fWiskott)
inp.sphereInputs()

inp.X[0,:] = np.sin(2 * np.pi * fWiskott * inp.T)
inp.X[1,:] = np.sin(2 * np.pi * 11* fWiskott * inp.T) **4

inp.whitenInputs()
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')



# ------------- Brian
Texp = inp.tmax - inp.tmin
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# -----------------------
if pre_filter:
    builder = fb.Filter(inp.T,inverse_period)
    # Get the inverse filter of an alpha_filter
    al = (builder.T >= 0) * builder.T/tau_omega**2 * exp(-builder.T/tau_omega) * Dt
    #al = (builder.T >= 0) * 1/tau_omega * exp(-builder.T/tau_omega) * Dt
    inv_al = builder.compute_fourier_inverse(al,10**-5)
    shp = shape(inp.X)
    X_in = np.zeros(shp)
    for Ni in range(0,shp[0]):
        X_in[Ni,:] = scipy.signal.fftconvolve(inp.X[Ni,:],inv_al,'same')
else:
    X_in = inp.X

# -----------------------
tCount = ti.timeCount(tCount,'High pass input setup (s):')


ta0 = TimedArray(X_in[0,:],Dt)
ta1 = TimedArray(X_in[1,:],Dt)
ta2 = TimedArray(X_in[2,:],Dt)
ta3 = TimedArray(X_in[3,:],Dt)
ta4 = TimedArray(X_in[4,:],Dt)





G_pre = NeuronGroup(Ninputs, '''
                                    x_train : 1
                                    d x_av /dt = (x_train - x_av) / tau_average : 1
                                    x = x_train - x_av : 1

                                    d x_exp /dt = (x - x_exp ) / tau_omega : 1
                                    d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1

                                    x_th = (i == 0) * ta0(t) + (i == 1) * ta1(t) + (i == 2) * ta2(t) + (i == 3) * ta3(t) + (i == 4) *ta4(t) : 1

                                    rt = nu0 * x_th + nu_av : Hz
                                    v  : volt ''',
                    threshold='v>Vthr*rand()',
                    reset='v=0 * volt')
cs = G_pre.custom_operation('''
    x_train = 0
    v = rt * dt * Vthr
    ''',when='train_reset')

G_pre.x_av = nu_av * Dt

G_post = NeuronGroup(1,"s : 1")
S = Synapses(G_pre,G_post,
             model="s_post = x_pre : 1 (summed)",
             pre="x_train_pre += 1",
             connect=True)


mon_pre_x = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av','x_th'], record=True,dt=dtRecord)
MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets', 'end']
run(Texp)
device.build(project_dir='output', compile_project=True, run_project=True, debug=False)

spikes_pre = SpikeMonitor(G_pre)


T_summed,x_summed = bt.period_sum(inp.T,mon_pre_x.x_alpha,2*tau_average,fWiskott,FS)
T_summed,x_summed_exp = bt.period_sum(inp.T,mon_pre_x.x_exp,2*tau_average,fWiskott,FS)
T_summed,x_summed_real = bt.period_sum(inp.T,inp.X,2*tau_average,fWiskott,FS)
T_summed,x_summed_filtered = bt.period_sum(inp.T,X_in,2*tau_average,fWiskott,FS)

plt.figure()
title('X spike filtered summed')
for i in range(0,4):
    plt.subplot(221+i)
    plt.plot(x_summed[i,:].T)


plt.figure()
title('X spike EXP summed')
for i in range(0,4):
    plt.subplot(221+i)
    plt.plot(x_summed_exp[i,:].T)


plt.figure()
title('X initial summed')
for i in range(0,4):
    plt.subplot(221+i)
    plt.plot(x_summed_real[i,:].T)

plt.figure()
title('X filtered summed')
for i in range(0,4):
    plt.subplot(221+i)
    plt.plot(x_summed_filtered[i,:].T)

plt.show()

plt.figure()
plt.plot(mon_pre_x.x_alpha.T)
title('X spike filtered')

plt.figure()
plt.plot(X_in.T)
title('X filtered')

plt.figure()
plt.plot(inp.X.T)
title('X initial')
plt.show()