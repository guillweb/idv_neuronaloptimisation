__author__ = 'guillaume'

from brian2 import *
from runner_brian import run_SFA_continuous_lambda
from runner_brian import plot_fancy

from runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time


tmax = 100 * second
tau_omega = 10 * ms
tau_w = 2 * second
tau_derivation = tau_omega


T,x_th,x_alpha,s,s_al,w,w_al,post_idx = run_SFA_continuous_lambda.run_SFA_continuous_lambda(tau_omega=tau_omega,tmax=tmax,tau_w=tau_w,tau_derivation=tau_derivation)

plt.figure()
#gs = gridspec.GridSpec(1,1)

plot_fancy.plot_online(T,w,s,post_idx)
plt.show()