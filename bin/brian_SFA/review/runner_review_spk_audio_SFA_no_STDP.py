__author__ = 'guillaume'


from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time

def run_SFA_spiking_review(X_in=[],
                           tau_average=5 * second,
                           nu0=80*Hz,
                           nu_av=100*Hz,
                           nu_offset=20*Hz,
                           kappa0 = 1.,
                           V0 = 1. *mV,
                           tau_PSP= 3 * ms,
                           tau_ref= 3 * ms,

                           T_audio=20 * second,
                           input_file_name="../../data/countryguit_11025.wav",
                           N_delays=128,
                           tau_delay = 9  / (11025 * Hz),
                           input_type=0,

                           fWiskott=1*Hz,
                           tmin=0*second,
                           tmax=200 * second,
                           FS=10000 * Hz,
                           tau_omega=10 * ms,
                           tau_w= 5. * ms,
                           Noutputs=1,
                           frequency_factor=11,
                           alpha_wiskott=1,w_0=[],
                           dtRecord=-1 * second,
                           do_record=True):
    set_device('cpp_standalone')
    device.reinit()
    #-------------
    # Parameters
    #-------------

    print '---SPIKING review----- fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

    # -------
    # Solver
    # -------

    if do_record and dtRecord < 0:
        dtRecord = (tmax-tmin)/1000

    tCount = time.clock()

    # Define input

    ## WISKOTT INPUT
    if input_type == 0:
        N = int((tmax - tmin) * FS)

        # Define input
        inp = il.Input(tmin,tmax,N)
        inp.set_wiskott_classic(fWiskott,frequency_factor,alpha_wiskott)
        inp.sphereInputs()
        gain = 1

        eq = '''                        x_th = int(i==0) * ta0(t) \
                                        + int(i==1) * ta1(t) \
                                        + int(i==2) * ta2(t) \
                                        + int(i==3) * ta3(t) \
                                        + int(i==4) * ta4(t) : 1
                                        '''
        Ninputs = inp.n
        tCount = ti.timeCount(tCount,'Input setup (s):')
    ## AUDIO INPUT
    else:
        N = int(T_audio * FS)
        #Define input
        inp = il.Input(tmin,T_audio+tmin,N)
        inp.addWaveFromFile(input_file_name,0*second)
        inp.n= 1
        inp.X = np.zeros((1,N))
        inp.X[0,:] = inp.x
        inp.whitenInputs()

        print "Input std: {0}".format(np.std(inp.x))

        Ninputs = N_delays

        eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''

        tCount = ti.timeCount(tCount,'Input setup (s):')

    # ------------- Brian
    Texp = tmax - tmin
    Dt = inp.dt
    defaultclock.dt = Dt

    # -----------------------
    X_in = inp.X
    tCount = ti.timeCount(tCount,'High pass input setup (s):')
    ta0 = TimedArray(X_in[0,:],Dt)

    ## WISKOTT INPUT END
    if input_type==0:
        ta1 = TimedArray(X_in[1,:],Dt)
        ta2 = TimedArray(X_in[2,:],Dt)
        ta3 = TimedArray(X_in[3,:],Dt)
        ta4 = TimedArray(X_in[4,:],Dt)

    G_pre = NeuronGroup(Ninputs, eq + '''
                    d r_exp /dt = - r_exp / tau_omega : Hz
                    d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz

                    nu_in = nu0 * x_th + nu_av : Hz''',
                        threshold='nu_in * dt>rand()',
                        method="euler",
                        refractory=tau_ref,
                        reset='v=0 * volt')

    kappa = kappa0 / V0 / Dt

    G_post = NeuronGroup(Noutputs,'''
                    d r_out_exp /dt = - r_out_exp / tau_omega : Hz
                    d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz

                    d v /dt = - v / tau_PSP : volt
                    nu_out  = nu_offset  + kappa * v : Hz

                    norm_w : 1''',
                threshold='nu_out * dt>rand()',
                reset='v=0 * volt',
                method="euler",
                refractory=tau_ref
                )
    G_post.norm_w = '1'

    S = Synapses(G_pre,G_post,
                 model='''
                 STDP = (r_out_exp_post - r_out_alpha_post) * (r_exp_pre - r_alpha_pre) / nu_av**2 :  1
                 dw/dt = - STDP / tau_w : 1

                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre='''
                    v_post += w * V0 * dt / tau_PSP
                    r_exp_pre += 1 / tau_omega
                    ''',

                 post='''
                    r_out_exp_post += 1 / tau_omega / Ninputs''',
                 connect=True)

    re_norm = S.custom_operation('''w /= sqrt(norm_w_post)''',when='end')


    if len(w_0) == 0:
        w_0 = np.random.randn(Ninputs)
        w_0 /= norm(w_0)

    for Ni in range(0,Ninputs):
        S.w['i == %d' % Ni] = w_0[Ni]

    if do_record:
        mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(0,Ninputs),dt=dtRecord)
        mon_post = StateMonitor(G_post, ['nu_out','r_out_alpha','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
        mon_syn = StateMonitor(S, ['w'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

        spikes_pre = SpikeMonitor(G_pre)
        spikes_post = SpikeMonitor(G_post)

    tCount = ti.timeCount(tCount,'Brian setup (s):')
    run(Texp, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)
    tCount = ti.timeCount(tCount,'Brian run (s):')


    if do_record:
        Trec = np.array(range(0,len(mon_post.r_out_alpha[0,:])) * dtRecord)
        return Trec,\
               np.array(spikes_pre.t),\
               np.array(spikes_pre.i),\
               mon_pre.nu_in,\
               mon_pre.r_alpha,\
               np.array(spikes_post.t),\
               np.array(spikes_post.i),\
               mon_post.nu_out,\
               mon_post.r_out_alpha,\
               mon_syn.w,\
               np.array(S.w),\
               np.array(S._postsynaptic_idx)
    else:
        return [],[],[],[],[],[],[],[],[],[],np.array(S.w),np.array(S._postsynaptic_idx)

