__author__ = 'guillaume'

import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
from brian2 import *
import matplotlib.pyplot as plt
import scipy
import numpy.random as rd
from numpy.linalg import norm

f_input = 1 * Hz
T_audio = 4 * second
tmax = 1000 * second
tau_av = .5 * second

tau_w = 1. * second

tau_PSP = 10 * ms
tau_omega = 10 * ms
FS = 10000 * Hz

kappa = .5
nu_av = 100. * Hz
nu_0 = 4. / 5. * nu_av
nu_offset = nu_av

sub_mean = False
simulate_bias = True

inp = il.Input(0.,T_audio,int(T_audio * FS))
inp.set_wiskott_classic(f_input)
X = inp.X
X *= nu_0
X += nu_av

def convolve(X,f):
    X = X / Hz
    n,N = shape(X)
    X_out = zeros(X.shape)

    for Ni in arange(n):
        X_out[Ni,:] = scipy.signal.fftconvolve(X[Ni,:],f,'same')
    return X_out * Hz
# Define filters

FB = fb.FilterBuilder(inp.T,2*tau_av)
f_al = (FB.T >= 0) * FB.T/ tau_omega **2 * exp(- FB.T/tau_omega) * FB.dt
f_exp = FB.dt / tau_omega * (FB.T >= 0) * exp(- FB.T/tau_omega)
f_av = FB.dt / tau_av * (FB.T >= 0) * exp(- FB.T / tau_av)
f_psp = FB.dt / tau_PSP * (FB.T >= 0) * exp(- FB.T/tau_PSP)

f_d = f_exp - f_al
f_bias = f_d * scipy.signal.fftconvolve(f_d,f_psp,'same')


X_al = convolve(X,f_al)
X_exp = convolve(X,f_exp)
X_d = X_exp - X_al
X_d_xi = convolve(X_d,f_psp)
X_bias = convolve(X,f_bias)

w = ones(shape(X)[0])
w /= norm(w)
def get_rate_out(X,w):
    return nu_offset + kappa * dot(w,convolve(X,f_psp))

R_out = get_rate_out(X_al,w)

if sub_mean:
    X_d -= convolve(X_d,f_av)
    X_d_xi -= convolve(X_d_xi,f_av)
K = kappa * dot(X_d,X_d_xi.T)

plt.plot(FB.Tdata,X_d.T)

N_iter = int(tmax/T_audio)
cost = zeros(N_iter)
print 'N iterations: ',N_iter

plt.figure()
plt.subplot(2,1,1)
plt.plot(FB.Tdata,X_al.T)
plt.subplot(2,1,2)
plt.plot(FB.Tdata,R_out)
plt.show()

for k in arange(N_iter):
    dw = dot(K,w.T) / nu_av**2
    w -= dw * FB.dt / tau_w
    if simulate_bias:
        #b = -w * 10**3
        b = kappa * w * sum(X_bias,axis=1)
        w += b * FB.dt / tau_w
    w /= norm(w)
    cost[k] = dot(w,dw)
    print norm(b),norm(dw)


out = get_rate_out(X,w)

plt.figure()
plt.subplot(3,1,1)
plt.plot(cost)
plt.subplot(3,1,2)
plt.plot(w)
plt.subplot(3,1,3)
plt.plot(out)

plt.show()