__author__ = 'guillaume'


from brian2 import *
import numpy as np
import scipy.io.wavfile as wave
import matplotlib.pyplot as plt
import resource
import matplotlib.gridspec as gridspec
import bin.input_loader.input_loader as il

megs = 2048
resource.setrlimit(resource.RLIMIT_AS, (megs * 1048576L, -1L))

set_device('cpp_standalone')
device.reinit()

# Audio file parameter
filename='../../../data/countryguit.wav'
FS = 10000 * Hz         # Sampling frequency matches the dt of the simulation
tmax = 1000 * second     # Duration of the simulation
T_audio = 10 * second   # Looping over the first T_audio of the audio file
dt_record = 300 / FS     # Record values every dt_record

# Ndelays
Ndelays = 64            # Number of delays in the delay line
tau_delay = 20 * ms      # Delay between each inputs of the delay line

# Neuron parameters
tau_omega = 20 * ms     # SDTP window width parameter, it also defines the spike rate
tau_w = 2.5 * second    # STDP convergence time (inverse of the learning rate)
tau_ref = 0 * ms        # Refractory period

nu_av = 500 * Hz        # Mean of the input rate
nu0 = 4./5. * nu_av     # Standard deviation of the input rate

V0 = 1 * mV             # Each spike contribute to V0 into the post synaptic membrane potential
tau_PSP = 1 * ms        # Membrane potential time constant
nu_offset = nu_av       # Offset dynamic of the output rate
kappa0= .15              # Multiplicative factor of the dynamic

epsi = 1e-3
sphering = True

# Load wave file
fswav,wav = wave.read(filename)
fswav = fswav * Hz

if len(shape(wav)) == 2: wav = wav[:,0]
N_audio = int(T_audio * fswav)
wav = array(wav[0:N_audio],dtype=float)
inp = il.Input(0,T_audio,N_audio)
inp.x = wav
inp.whitenInputs()
inp.expandWithDelay(Ndelays,tau_delay)
if sphering:
    X,R_proj = inp.sphereInputs(degenerate=False,epsi=epsi)
    inp.whitenInputs()
else:
    X = inp.X
    R_proj = eye(Ndelays)

Ninputs = inp.n
ta = TimedArray(X.T,1./fswav)
print 'N inputs: {0}'.format(Ninputs)

# Init weights
w_0 = np.random.randn(Ninputs)
w_0 /= norm(w_0)
assert dot(w_0,R_proj).size == Ndelays, 'Size should be the number of delays '

G_pre = NeuronGroup(Ninputs,'''
                    t_loop = t - floor(t/T_audio)*T_audio : second
                    x_th = ta(t_loop,i) : 1

                    d r_exp /dt = - r_exp / tau_omega : Hz
                    d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz
                    nu_in = nu0 * x_th + nu_av : Hz''',
                        threshold='nu_in * dt>rand()',
                        method="euler",
                        refractory=tau_ref,
                        reset='v=0 * volt')

kappa = kappa0 / V0 * FS
G_post = NeuronGroup(1,'''
                    d r_out_exp /dt = - r_out_exp / tau_omega : Hz
                    d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz
                    d v /dt = - v / tau_PSP : volt
                    nu_out  = nu_offset  + kappa * v : Hz
                    sum_w : 1
                    norm_w : 1''',
                threshold='nu_out * dt>rand()',
                reset='v=0 * volt',
                method="euler",
                refractory=tau_ref)
G_post.norm_w = '1'
S = Synapses(G_pre,G_post,
                 model='''
                 w : 1
                 sum_w_post = w / Ninputs : 1 (summed)
                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre='''
                    w += -(r_out_exp_post - r_out_alpha_post) / nu_av**2 / tau_w
                    v_post += w * V0 * dt / tau_PSP
                    r_exp_pre += 1 / tau_omega
                    ''',
                 post='''
                    w += -(r_exp_pre - r_alpha_pre) / nu_av**2 / tau_w
                    r_out_exp_post += 1 / tau_omega / Ninputs''',
                 connect=True)

zero_mean = S.run_regularly('w -= sum_w_post',when='end')
re_norm = S.run_regularly('w /= sqrt(norm_w_post)',when='end')
for Ni in range(Ninputs):S.w['i == %d' % Ni] = w_0[Ni]

mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(Ninputs), dt=dt_record)
mon_post = StateMonitor(G_post, ['nu_out','r_out_alpha','norm_w'], record=array([0]), dt=dt_record)
mon_syn = StateMonitor(S, ['w'], record=np.arange(Ninputs), dt=dt_record)

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

run(tmax, report="text")
device.build(directory='output', compile=True, run=True, debug=False)


# Printing results

av_rate_alpha = mean(array(mon_pre.r_alpha))
av_rate = array(spikes_pre.t).size / Ndelays / tmax
av_rate_out = array(spikes_post.t).size / tmax
av_rate_alpha_out = mean(array(mon_post.r_out_alpha))
print 'Input average rate \t  {0} \t with alpha \t {1}'.format(av_rate,av_rate_alpha)
print 'Output average rate \t {0} \t with alpha \t {1}'.format(av_rate_out,av_rate_alpha_out)
print 'Output max rate \t {0}'.format(max(array(mon_post.r_out_alpha[0,:])))
print 'Nu out min \t {0}, \t mean {1} \t max {2}'.format(min(array(mon_post.nu_out[0,:])),mean(array(mon_post.nu_out[0,:])),max(array(mon_post.nu_out[0,:])))


N = mon_post.r_out_alpha.size
T = arange(N) * FS
w = mon_syn.w
w_end = w[:,-1]
w_end = dot(w_end,R_proj).ravel()

## Printing
plt.figure()
gs = gridspec.GridSpec(3,2)
gs.update(wspace=0.3,hspace=0.5)

raster = plt.subplot(gs[0,0])
weights = plt.subplot(gs[1,:])
filter = plt.subplot(gs[2,:])

delays = range(Ndelays) * tau_delay / ms
inds = range(0,Ndelays,3)
for k in inds:
    weights.plot(T,w[k,:].T,color='gray',linewidth=2.0)
weights.plot(T,w[0,:],color='red',linewidth=2.0)
weights.plot(T,w[-1,:],color='blue',linewidth=2.0)

# Titles
ind_t = spikes_pre.t < 1. * second
ind_i = spikes_pre.i[ind_t] < 20
raster.plot(spikes_pre.t[ind_t][ind_i], spikes_pre.i[ind_t][ind_i], '.')
raster.set_yticks([])
raster.set_xticks([0,1])
raster.set_xlim([0,1])
raster.set_ylim([0,20])
raster.yaxis.tick_left()

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')

filter.plot(delays,w_end,color='black',linewidth=2.0)
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (ms)')

#filter.set_xlim([0,64])
#filter.set_ylim([-0.4,0.4])
filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig('../../../figures/fig9b_spiking_audio_review.pdf')
show()