__author__ = 'guillaume'


from brian2 import *
import numpy as np
import scipy.io.wavfile as wave
import matplotlib.pyplot as plt
import resource
import matplotlib.gridspec as gridspec
import bin.brian_SFA.runner_brian.result_saver as result_saver


megs = 2048
resource.setrlimit(resource.RLIMIT_AS, (megs * 1048576L, -1L))

set_device('cpp_standalone')
device.reinit()

save = True

# Audio file parameter
filename='../../../data/countryguit_10000.wav'
FS = 10000 * Hz         # Sampling frequency matches the dt of the simulation
T_audio = 5 * second   # Looping over the first T_audio of the audio file
tmax = 40 * T_audio     # Duration of the simulation
dt_record = tmax /1000    # Record values every dt_record

# Ndelays
Ninputs = 64            # Number of delays in the delay line
tau_delay = 60 * ms     # Delay between each inputs of the delay line

# Neuron parameters
tau_omega = 2 * ms     # SDTP window width parameter, it also defines the spike rate
tau_w = 50. * second    # STDP convergence time (inverse of the learning rate)
tau_ref = 0 * ms        # Refractory period
tau_av = 1 * second

nu0 = 80 * Hz           # Standard deviation of the input rate
nu_av = 100 * Hz        # Mean of the input rate

V0 = 1 * mV             # Each spike contribute to V0 into the post synaptic membrane potential
tau_PSP = 1 * ms        # Membrane potential time constant
nu_offset = nu_av       # Offset dynamic of the output rate
kappa0= 1. / Ninputs    # Multiplive factive of the dynamic


db_path= 'results_fig9_hp/'
params = { 'seed': seed,
           'FS' : FS,
           'tau_av' : tau_av,
           'tmax': tmax,
           'T_audio': T_audio,
           'dt_record': dt_record,
           'Ninputs': Ninputs,
           'tau_delay': tau_delay,
           'tau_omega': tau_omega,
           'tau_w': tau_w,
           'tau_ref': tau_ref,
           'nu0': nu0,
           'nu_av': nu_av,
           'V0': V0,
           'tau_PSP': tau_PSP,
           'nu_offset': nu_offset,
           'kappa0': kappa0
           }

count,file_list = result_saver.find_results_in_db(db_path,params)


if count==0:

    # Init weights
    w_0 = np.random.randn(Ninputs,1)
    w_0 /= norm(w_0)

    # Load wave file
    fswav,wav = wave.read(filename)
    if len(shape(wav)) == 2: wav = wav[:,0]
    N_audio = int(T_audio * FS)
    wav = wav[0:N_audio]
    wav -= mean(wav)
    wav /= np.std(wav)
    ta0 = TimedArray(wav,1/FS)

    G_pre = NeuronGroup(Ninputs,'''
                        t_i = t - tau_delay * i : second
                        t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
                        x_th = ta0(t_loop_i) : 1

                        d r_av /dt = - r_av / tau_av : Hz
                        d r_exp /dt = (- r_av - r_exp) / tau_omega : Hz
                        d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz
                        nu_in = nu0 * x_th + nu_av : Hz''',
                            threshold='nu_in * dt>rand()',
                            method="euler",
                            refractory=tau_ref,
                            reset='v=0 * volt')

    kappa = kappa0 / V0 * FS
    G_post = NeuronGroup(1,'''
                        d r_out_av /dt = - r_out_av / tau_av : Hz
                        d r_out_exp /dt = (- r_out_av - r_out_exp) / tau_omega : Hz
                        d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz
                        d v /dt = - v / tau_PSP : volt
                        nu_out  = nu_offset  + kappa * v : Hz
                        norm_w : 1''',
                    threshold='nu_out * dt>rand()',
                    reset='v=0 * volt',
                    method="euler",
                    refractory=tau_ref)
    G_post.norm_w = '1'
    S = Synapses(G_pre,G_post,
                     model='''
                     w : 1
                     norm_w_post = w*w : 1 (summed)''',
                     method="euler",
                     pre='''
                        w += - (t_loop_i_pre > tau_av) * (r_out_exp_post - r_out_alpha_post) / nu_av**2 / tau_w
                        v_post += w * V0 * dt / tau_PSP
                        r_exp_pre += 1 / tau_omega
                        r_av_pre += 1/tau_av
                        ''',
                     post='''
                        w += -(r_exp_pre - r_alpha_pre) / nu_av**2 / tau_w
                        r_out_av_post += 1 / tau_av / Ninputs
                        r_out_exp_post += 1 / tau_omega / Ninputs''',
                     connect=True)
    re_norm = S.custom_operation('''w /= sqrt(norm_w_post)''',when='end')
    for Ni in range(Ninputs):S.w['i == %d' % Ni] = w_0[Ni]

    mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(Ninputs), dt=dt_record)
    mon_post = StateMonitor(G_post, ['nu_out','r_out_alpha','norm_w'], record=array([0]), dt=dt_record)
    mon_syn = StateMonitor(S, ['w'], record=np.arange(Ninputs), dt=dt_record)

    spikes_pre = SpikeMonitor(G_pre)
    spikes_post = SpikeMonitor(G_post)

    run(tmax, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)

    spikes_in_t = np.array(spikes_pre.t)
    spikes_in_i = np.array(spikes_pre.i)
    spikes_out_t = np.array(spikes_post.t)

    nu_out = mon_post.nu_out[0,:]
    r_out_alpha = mon_post.r_out_alpha[0,:]

    w = mon_syn.w

    if save:result_saver.save_result(db_path,params,[spikes_in_t,spikes_in_i,spikes_out_t,nu_out,r_out_alpha,w])
else:
    file_name = db_path + 'objects/' + file_list[0]
    _,OUT_list,param_check = result_saver.get_data(file_name)
    [spikes_in_t,spikes_in_i,spikes_out_t,nu_out,r_out_alpha,w] = OUT_list
    print 'RESULT LOADED'
# Printing results

N = r_out_alpha.size
T = arange(N) * dt_record
w_end = w[:,-1]

av_rate = spikes_in_t.size / Ninputs / tmax
av_rate_out = spikes_out_t.size / tmax
av_rate_alpha_out = mean(array(r_out_alpha))
print 'Input average rate \t  {0}'.format(av_rate)
print 'Output average rate \t {0} \t with alpha \t {1}'.format(av_rate_out,av_rate_alpha_out)
print 'Output max rate \t {0}'.format(max(r_out_alpha))
print 'Nu out min \t {0}, \t mean {1} \t max {2}'.format(min(nu_out),mean(nu_out),max(nu_out))



## Printing
plt.figure()
gs = gridspec.GridSpec(3,2)
gs.update(wspace=0.3,hspace=0.5)

raster = plt.subplot(gs[0,0])
weights = plt.subplot(gs[1,:])
filter = plt.subplot(gs[2,:])

delays = range(Ninputs) * tau_delay / ms
inds = range(0,Ninputs,3)
for k in inds:
    weights.plot(T,w[k,:].T,color='gray',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)
weights.plot(T,np.sum(w[inds,:],axis=0),color='red',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)

# Titles
ind_t = spikes_in_t < 1
ind_i = spikes_in_i[ind_t] < 20
raster.plot(spikes_in_t[ind_t][ind_i], spikes_in_i[ind_t][ind_i], '.')
raster.set_yticks([])
raster.set_xticks([0,1])
raster.set_xlim([0,1])
raster.set_ylim([0,20])
raster.yaxis.tick_left()

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')

filter.plot(delays,w_end,color='black',linewidth=2.0)
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (ms)')

#filter.set_xlim([0,])
filter.set_ylim([-0.4,0.4])
filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig('../../../figures/fig9b_spiking_audio_review.pdf')
show()