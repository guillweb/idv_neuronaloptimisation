__author__ = 'guillaume'


from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time
from bin.tools.general import *
import resource

megs = 2048
resource.setrlimit(resource.RLIMIT_AS, (megs * 1048576L, -1L))

set_device('cpp_standalone')
device.reinit()


fWiskott = 1 * Hz
FS = 10000 * Hz
tmax = 10 * second

# Filtering and window length parameter
tau_omega = 20 * ms

# rate paramters
V0 = 1 * mV
nu0 = 80 * Hz
nu_av = 100 * Hz
nu_offset = nu_av
tau_PSP = 3 * ms
kappa0= .2

# Time constants
tau_w = 10. * second
tau_ref = 0 * ms
dt_record = 10 / FS

w_0 = np.random.randn(5,1)
w_0 /= norm(w_0)


tCount = time.clock()

N = int(tmax * FS)
Ninputs = 5
t_vec = arange(N) / FS
ta0 = TimedArray(np.cos(t_vec * 2 * np.pi * fWiskott),1/FS)


G_pre = NeuronGroup(Ninputs,'''
                    x_th = int(i==0) * ta0(t): 1
                    d r_exp /dt = - r_exp / tau_omega : Hz
                    d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz
                    nu_in = nu0 * x_th + nu_av : Hz''',
                        threshold='nu_in * dt>rand()',
                        method="euler",
                        refractory=tau_ref,
                        reset='v=0 * volt')

kappa = kappa0 / V0 * FS
G_post = NeuronGroup(1,'''
                    d r_out_exp /dt = - r_out_exp / tau_omega : Hz
                    d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz
                    ''',
                threshold='nu_av * dt>rand()',
                reset='v=0 * volt',
                method="euler",
                refractory=tau_ref)

S = Synapses(G_pre,G_post,
                 model='''
                 w : 1''',
                 method="euler",
                 pre='''
                    r_exp_pre += 1 / tau_omega
                    ''',
                 post='''
                    r_out_exp_post += 1 / tau_omega''',
                 connect=True)


mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(Ninputs), dt=dt_record)
mon_post = StateMonitor(G_post, ['r_out_exp','r_out_alpha'], record=array([0]), dt=dt_record)

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

tCount = ti.timeCount(tCount,'Brian setup (s):')
run(tmax, report="text")
device.build(directory='output', compile=True, run=True, debug=False)
tCount = ti.timeCount(tCount,'Brian run (s):')


N_show = 2 / fWiskott / dt_record
N_rec = len(mon_post.r_out_alpha.flat)

T = arange(N_rec) * dt_record
T_start = arange(4 * N_show) * dt_record

plt.plot(mon_pre.x_th[0,0:N_show])
plt.figure()

train = spike_times_to_train(spikes_post.t,dt_record,N_rec)
rates = get_rate(train,tau_omega,dt_record)
concat = vstack((rates.flat,mon_post.r_out_alpha.flat,mon_pre.r_alpha[0,:].flat))
plt.subplot(2,1,1)
plt.plot(concat[:,0:N_show].T / np.amax(concat,axis=1))
plt.subplot(2,1,2)
plt.plot(concat[:,N-N_show:N].T / np.amax(concat,axis=1))


plt.figure()
T_summed_0,s_summed_0 = bt.period_sum(T_start,mon_post.r_out_alpha[:,0:5 * N_show],0.,2*fWiskott)

T_summed,s_summed = bt.period_sum(T,mon_post.r_out_alpha,tmax/2,2*fWiskott)

s_summed_0 -= mean(s_summed_0)
s_summed_0 /= std(s_summed_0)

s_summed -= mean(s_summed)
s_summed /= std(s_summed)

plt.plot(T_summed.flat,s_summed.flat,color='green')
plt.plot(T_summed_0.flat,s_summed_0.flat,color='red')

print tau_omega * FS
print 1/ (tau_omega * FS)

print "rate from spikes, r_out, nu_out"
print np.amax(concat,axis=1)

plt.show()