from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from numpy import *
import scipy.io.wavfile as wave

from bin.brian_SFA.runner_brian import plot_fancy
import matplotlib.gridspec as gridspec


set_device('cpp_standalone')
device.reinit()

T_audio = 5 * second
tmax = 400 * T_audio
dtRecord = tmax / 1000
tau_omega = 2 * ms
tau_w = 3. * second
N_delays = 64
tau_delay = 1 * ms
FS_sim = 10000 * Hz
tau_average=5 * second
nu_av = 100 * Hz
nu0 = 4./5. * nu_av
use_av = 0

Noutputs = 1
tau_av = 1. * second
input_file_name = '../../../../data/countryguit.wav'

w_0 = np.random.randn(N_delays)
w_0 /= norm(w_0)

fs,wav = wave.read(input_file_name)
if len(wav.shape) == 2: wav = wav[:,0] # Handle stereo
FS = fs * Hz
N = int(T_audio * FS)
wav = array(wav[:N],dtype=float)
wav -= mean(wav)
wav /= std(wav)

ta0 = TimedArray(wav,1./FS)
Dt = 1./FS_sim
std_x = sqrt(nu_av * Dt)
G_pre = NeuronGroup(N_delays, '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1

            nu_in = nu0 * x_th + nu_av : Hz

            d x_av /dt = - x_av / tau_average * use_av : 1

            d x_exp /dt = - x_exp / tau_omega : 1
            d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1''',
            threshold='nu_in * dt>rand()')

cs = G_pre.run_regularly('''x_train = 0 ''',when='train_reset')

G_post = NeuronGroup(1,'''
                d s_av / dt = - s_av/ tau_average * use_av : 1

                d s_exp / dt = - s_exp / tau_omega : 1
                d s_alpha / dt = (s_exp - s_alpha) / tau_omega : 1

                norm_w : 1''')
G_post.norm_w = '1'
G_pre.x_av = nu_av * Dt * use_av
G_post.s_av = nu_av * Dt * use_av


learning_rate = 1. / std_x**2 * Dt / tau_w
S = Synapses(G_pre,G_post,
                 model='''
                 w : 1
                 norm_w_post = w*w : 1 (summed)''',
                 pre='''
                 x_av_pre += dt / tau_average * use_av
                 s_av_post += w * dt / tau_average * use_av

                 x_exp_pre += (1 - x_av_pre) * dt / tau_omega
                 s_exp_post += (w - x_av_pre) * dt / tau_omega

                 w += (s_exp_post - s_alpha_post) * (1 - x_av_pre) * learning_rate
                 w += (x_exp_pre - x_alpha_pre) * (w - s_av_post) * learning_rate''',
                 connect=True)

re_norm = S.run_regularly('''w /= sqrt(norm_w_post)''',when='weight_renorm')
for Ni in range(0,N_delays): S.w['i == %d' % Ni] = w_0[Ni]

MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets',
                                'weight_renorm',
                                'end']


mon_pre = StateMonitor(G_pre, ['x_alpha','x_th'], record=arange(0,N_delays),dt=dtRecord)
mon_post = StateMonitor(G_post, ['s_alpha','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
mon_syn = StateMonitor(S, ['w'], record=arange(0,N_delays),dt=dtRecord)


run(tmax, report="text")
device.build(directory='output', compile=True, run=True, debug=False)

w = array(mon_syn.w)


## Printing
plt.figure()
gs = gridspec.GridSpec(2,2)
gs.update(wspace=0.3,hspace=0.5)

plot_fancy.set_font()

weights = plt.subplot(gs[0,:])
filter = plt.subplot(gs[1,:])

T = mon_pre.t / second
delays = range(N_delays) * tau_delay
inds = range(0,N_delays,3)
for k in inds:
    weights.plot(T,w[k,:],color='gray',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)
weights.plot(T,w[0,:],color='red',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')

w_end = w[:,-1]
filter.plot(delays,w_end,color='black',linewidth=2.0)
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (s)')

filter.set_xlim([0,max(delays)])
filter.set_ylim([-0.2,0.2])
filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig('spiking_audio.pdf')
show()