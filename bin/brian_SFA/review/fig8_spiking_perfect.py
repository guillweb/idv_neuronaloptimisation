__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.review.runner_review_spk_audio_SFA_no_STDP import run_SFA_spiking_review
from bin.brian_SFA.runner_brian import plot_fancy

import bin.brian_SFA.runner_brian.result_saver as result_saver
from bin.filter_optimisation.filter_builder import *
from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import bin.brian_gradient_optimisation.tools as bt
import time

from bin.tools.general import *

seed = 0
np.random.seed(seed)
save = False

## plot param
t_period = 4 * second
t_start = 0 * second
t_converged = 500 * second

# fig param
tmax = 2 * t_converged
FS = 10000 * Hz

# Filtering and window length parameter
tau_omega = 20 * ms

# rate parameters
nu_av = 100 * Hz
nu0 = 4./5 * nu_av
nu_offset = nu_av
tau_PSP = 10. * ms
kappa0 = .2

# Time constants
f_input = 1. * Hz
tau_w = 1 * second
tau_ref = 0 * ms
dt_record = 100 /FS


tCount = time.clock()
db_path= 'results_fig8/'
params = { 'tmax': tmax,
           'fWiskott': f_input,
           'tau_omega': tau_omega,
           'tau_w': tau_w,
           'nu0':nu0,
           'nu_av':nu_av,
           'kappa0':kappa0,
           'nu_offset':nu_offset,
           'tau_PSP':tau_PSP,
           'tau_ref': tau_ref,
           'dtRecord': dt_record,
           'seed': seed
           }
count,file_list = result_saver.find_results_in_db(db_path,params)


if count==0:
    T,spikes_t,spikes_i,x,x_alpha,out_spk_t,out_spk_i,s,s_alpha,w,w_end,post_idx = run_SFA_spiking_review(
                                                                fWiskott=f_input,
                                                                tau_omega=tau_omega,
                                                                tmax=tmax,
                                                                tau_w=tau_w,
                                                                Noutputs=1,
                                                                nu0=nu0,
                                                                nu_av=nu_av,
                                                                nu_offset=nu_offset,
                                                                kappa0=kappa0,
                                                                tau_PSP=tau_PSP,
                                                                tau_ref=tau_ref,
                                                                dtRecord=dt_record)

    if save:
            result_saver.save_result(db_path,params,[out_spk_t,out_spk_i,s,s_alpha,w,w_end],[T,spikes_t,spikes_i,x,x_alpha,post_idx])
else:
    file_name = db_path + 'objects/' + file_list[0]
    [T,spikes_t,spikes_i,x,x_alpha,post_idx],[out_spk_t,out_spk_i,s,s_alpha,w,w_end],param_check = result_saver.get_data(file_name)
    print 'RESULT LOADED'


N = x.shape[1]
N_show = 2. / dt_record / f_input
s = dot(w_end,x[:,N-N_show:N])
plt.plot(s.T)


av_rate_alpha = mean(array(x_alpha))
av_rate = array(spikes_t).size / 5 / tmax
av_rate_out = array(out_spk_t).size / tmax
av_rate_alpha_out = mean(array(s_alpha))
print 'Input average rate \t  {0} \t with alpha \t {1}'.format(av_rate,av_rate_alpha)
print 'Output average rate \t {0} \t with alpha \t {1}'.format(av_rate_out,av_rate_alpha_out)
print 'Output max rate \t {0}'.format(max(array(s_alpha[0,:])))
print 'Nu out min \t {0}, \t mean {1} \t max {2}'.format(min(array(s)),mean(array(s)),max(array(s)))


r_0 = mean(s_alpha) / Hz
r_std = std(s_alpha) / Hz
r_out_min = max(0,r_0 - 4. * r_std)
r_out_max = min(second / tau_ref,r_0 + 4. * r_std)
print 'x alpha std: ', std(x_alpha)
print 'R std: ', r_std

plt.figure(figsize=(10,10))
gs = gridspec.GridSpec(3,5)
gs.update(wspace=0.3,hspace=0.6)
plot_fancy.set_font()

ax0 = plt.subplot(gs[0,0:2])
ax0.set_title(r'Inputs')
ax0.set_yticks(arange(5))
ax0.set_yticklabels([r'$x_1$',r'$x_2$',r'$x_3$',r'$x_4$',r'$x_5$'])
ax0.set_ylim([-0.5,4.5])
ax0.set_xticks([0 ,1.0])
ax0.set_xlim([0*second,1*second])
ax0.set_yticks([])
ax0.set_xticklabels(['0','1 s'])
ax0.plot(spikes_t, spikes_i, '.')

ax0.set_xticks([0,1.0])

ax2 = plt.subplot(gs[0,3:5])
ax2.set_title(r' $r^{out} in Hz$ ')
ax2.set_xlim([0*second,1*second])
ax2.set_yticks([r_out_min,r_out_max])
ax2.set_ylim([r_out_min,r_out_max])
ax2.set_xticks([0,1.0])
ax2.set_xticklabels(['0','1 s'])
ax2.plot(T,s_alpha.T,linewidth=2,color='black')


ax3 = plt.subplot(gs[1,0:2])
ax3.set_xlim([t_start,t_start+t_period])
ax3.plot(T,s_alpha.T,linewidth=2,color='black')

ax3.set_yticks([r_out_min,r_out_max])
ax3.set_ylim([r_out_min,r_out_max])
ax3.set_ylabel(r'$r_{out} in Hz$')
locator_params(axis = 'x', nbins = 2)

ax5 = plt.subplot(gs[1,2:4])
ax5.plot(T,s_alpha.T,linewidth=2,color='black')
ax5.set_xlim([tmax-t_period,tmax])

ax5.set_yticks([r_out_min,r_out_max])
ax5.set_ylim([r_out_min,r_out_max])

locator_params(axis = 'x', nbins = 2)


# Breaking between before and after
ax3.spines['right'].set_visible(False)
ax5.spines['left'].set_visible(False)

ax3.yaxis.tick_left()
ax5.yaxis.tick_right()

ax5.tick_params(labelleft='off') # don't put tick labels at the top

d = .05 # how big to make the diagonal lines in axes coordinates
kwargs = dict(transform=ax3.transAxes, color='k', clip_on=False)
ax3.plot((1-d,1+d),(1-d,1+d), **kwargs)      # bottom-right diagonal
ax3.plot((1-d,1+d),(-d,+d), **kwargs)    # tpop-right diagonal

kwargs.update(transform=ax5.transAxes)  # switch to the bottom axes
ax5.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
ax5.plot((-d,+d),(-d,+d), **kwargs) # top-left diagonal

plt.setp(ax5.get_yticklabels(), visible=False)

ax3.set_title('Start')
ax5.set_title('End')

ax6 = plt.subplot(gs[2:3,:])
ax6.plot(T,w.T,linewidth=2,color='black')

ax6.set_ylim([-1,1])
locator_params(axis = 'x', nbins = 2)
locator_params(axis = 'y', nbins = 3)
ax6.set_ylabel('Weights')
ax6.set_xlabel('t (s)')

T_summed,s_summed = bt.period_sum(s_alpha,t_converged,tmax,2/t_period,1/dt_record)

ax7 = plt.subplot(gs[1,4])
ax7.plot(T_summed,s_summed.ravel(),linewidth=2,color='black')

ax7.set_yticks([])
ax7.set_ylim([r_out_min,r_out_max])

ax7.set_title('Averaged period')
ax7.set_xlim([0*second,t_period/2])
locator_params(axis = 'x', nbins = 1)

#tight_layout()

savefig('../../../figures/fig8_spiking_review.pdf')
plt.show()