__author__ = 'guillaume'

import cPickle as pickle
import os
from numpy import *
from numpy.linalg import norm
import matplotlib.pyplot as plt
import numpy.random as rd
import matplotlib.gridspec as gridspec

folder_path = 'spk_audio_no_stdp'
file_name = 'results.pickle'
K_best = 10

def get_weight(path):
    file = folder_path + '/' + path + '/t0/' + file_name
    f = open(file,'rb')
    res = pickle.load(f)
    w = res['wend']
    return w

def get_score(path):
    w = get_weight(path)
    n_w = norm(w)
    if not( n_w > .99 and n_w < 1.01): 'Norm of weight is {0} for path {1}'.format(n_w,path)
    return std(diff(w))

list = array(os.listdir(folder_path))
s = -ones(len(list))
failed_count = 0

for k_p,p in enumerate(list):
    try:
        s[k_p] = get_score(p)
    except:
        failed_count += 1

print 'Failed files: {0} / {1}'.format(failed_count,list.size)

arg_s = argsort(s)

# Re order all arrays
path_sorted = list[arg_s]
s_sorted = s[arg_s]

# Final path list and score
sel = s_sorted >= 0
path_final = path_sorted[sel][:K_best]
score_final = s_sorted[sel][:K_best]

# Plot all scores histogram
hist,bins = histogram(s_sorted[sel],bins=10)
plt.plot(bins[1:],hist)

print K_best,' best scores:'
print score_final

plt.figure()
gs = gridspec.GridSpec(10,1)
for k in arange(K_best):
    ax = plt.subplot(gs[k,0])
    w = get_weight(path_final[k])
    ax.plot(w)
    ax.set_xticks([0,32,64])
    ax.set_yticks([-.5,0,.5])
    ax.set_xlim([0,64])
    ax.set_ylim([-.5,.5])
    ax.set_xlabel(score_final[k])

plt.show()


