import numpy as np

# parse command line arguments
from optparse import OptionParser
cmdOptParser = OptionParser()


##################
## your params  ##
##################

cmdOptParser.add_option('-o',"--out",dest="outdir", action="store", type="string",help="Output results directory")
#version of execution-if version==0 no version is included!
cmdOptParser.add_option("-v","--version",dest="version", action="store", type="int",help="version of test", default = 0)
#input params
cmdOptParser.add_option("-m","--maxsteps",dest="maxsteps", action="store", type="int",help="max number of simulation steps", default = 100)
#distribution params
cmdOptParser.add_option("-p","--npools",dest="npools", action="store", type="int",help="number of neuron pools (distribution modes)", default = 2)
cmdOptParser.add_option("-n","--npp",dest="npp", action="store", type="int",help="number of neurons per pool", default = 1)
cmdOptParser.add_option("-w","--weights",dest="weights", action="store", type="float",help="absolute size of weights (positive and negative)", default = 1)
#random generator
cmdOptParser.add_option("-s","--seed",dest="seed", action="store", type="int",help="random seed", default = -1)

(options,args) = cmdOptParser.parse_args()

theseed = options.seed
maxsteps= options.maxsteps
npools= options.npools
npp= options.npp
weights= options.weights


print "set seed to", theseed
np.random.seed(theseed)



##################
## your code    ##
##################
print 'test print, \t weights {0} \t seed {1} '.format(weights,theseed)
somevariable = 'test save, \t weights {0} \t seed  {1}'.format(weights,theseed)


##################
## save results ##
##################
results = {"somevariable":somevariable}

#save results
if options.version>0:
    fajlname = options.outdir+'/results_v'+str(options.version)+'.shelf'
else:
    fajlname = options.outdir+'/results.shelf'

import operator
import shelve
shelf = shelve.open(fajlname)
shelf['results'] = results
shelf.close()
