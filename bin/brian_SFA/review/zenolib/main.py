#!/usr/bin/python

import numpy as np
import os, errno
import sys

import zenolib.zlib_thread as zth
import zenolib.zlib_commands as zcom

cc = zcom.CommandCreator("spk_audio_no_stdp_no_m", True)

#fix params
cc.AddFix('FS', 10000)
cc.AddFix('T', 4)
cc.AddFix('t',100000)
cc.AddFix('k',.5)          # kappa
cc.AddFix('d',2e-3)
cc.AddFix('n',100)
cc.AddFix('N',840)
cc.AddFix('R',120)
cc.AddFix('g',.004) # tau omega
cc.AddFix('p',.001) # tau PSP
#params
cc.AddParam('m',[1000.,10000.], range(2)) # tau momentum
cc.AddParam('w',[.01], range(1))  # tau w start
cc.AddParam('W',[1000.,100.], range(2)) # tau w end
cc.AddParam('s',range(1), range(1))         # seed

commands, dirs = cc.Create("script_audio_spiking_SFA_no_stdp_no_spk_minitor.py", each=1)
mp = zth.MultipleProcesses(maxprocesses=40, maxperhost=2)
mp.Run(commands, dirs)

exit()