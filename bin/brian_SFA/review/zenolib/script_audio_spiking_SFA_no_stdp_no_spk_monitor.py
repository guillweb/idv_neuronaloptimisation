__author__ = 'guillaume'

import matplotlib as mpl
mpl.use('Agg')
from brian2 import *
import numpy as np
import scipy.io.wavfile as wave
import matplotlib.pyplot as plt
import resource
import matplotlib.gridspec as gridspec
import bin.input_loader.input_loader as il
import numpy.random as rd

import cPickle as pickle
import os

from optparse import OptionParser
cmdOptParser = OptionParser()

print 'Setting Brian2 device'
set_device('cpp_standalone')
device.reinit()


print 'Set options.'
cmdOptParser.add_option('-o',"--out",dest="outdir", action="store", type="string",help="Output results directory")
cmdOptParser.add_option("-v","--version",dest="version", action="store", type="int",help="version of test", default = 0)

cmdOptParser.add_option("-F","--FS",dest="FS", action="store", type="int",help="sampling frequency in simulation", default = 10000)
cmdOptParser.add_option("-t","--tmax",dest="tmax", action="store", type="float",help="Simulation duration in sec", default = 1000)
cmdOptParser.add_option("-T","--Taudio",dest="T_audio", action="store", type="float",help="Simulation duration in sec", default = 5)

cmdOptParser.add_option("-N","--Nd",dest="N_delays", action="store", type="int",help="number of delays", default = 64)
cmdOptParser.add_option("-d","--taud",dest="tau_delay", action="store", type="float",help="Input delay", default = 0.001)

cmdOptParser.add_option("-g","--taug",dest="tau_omega", action="store", type="float",help="STDP time width", default = 20e-3)
cmdOptParser.add_option("-w","--tauws",dest="tau_w_start", action="store", type="float",help="Learning rate at start", default = .1)
cmdOptParser.add_option("-W","--tauwe",dest="tau_w_end", action="store", type="float",help="Learning rate at end", default = 10)
cmdOptParser.add_option("-m","--taum",dest="tau_mom", action="store", type="float",help="Momentun", default = 1000)

cmdOptParser.add_option("-n","--nuav",dest="nu_av", action="store", type="float",help="Input average frequency", default = 100)

cmdOptParser.add_option("-p","--taupsp",dest="tau_PSP", action="store", type="float",help="Membrane time constant", default = 1e-3)
cmdOptParser.add_option("-k","--kappa",dest="kappa", action="store", type="float",help="Multiplicating factor in dynamic", default = .2)

cmdOptParser.add_option("-s","--seed",dest="seed", action="store", type="int",help="random seed", default = -1)
cmdOptParser.add_option("-R","--Nrec",dest="Nrec", action="store", type="int",help="neuron to record", default = -1)

(options,args) = cmdOptParser.parse_args()
os.environ["MPLCONFIGDIR"] = options.outdir

filename='../../../../data/countryguit.wav'
seed = options.seed
if seed != -1: rd.seed(seed)

# Audio file parameter
FS = options.FS * Hz
tmax = options.tmax * second
T_audio = options.T_audio * second
dt_record = tmax / 1000

# Input parameters
Ndelays = options.N_delays       # Number of delays in the delay line
Nrec = options.Nrec      # Number of delays in the delay line
tau_delay = options.tau_delay * second    # Delay between each inputs of the delay line
epsi = 1e-6

# Neuron parameters
tau_omega = options.tau_omega * second     # SDTP window width parameter, it also defines the spike rate
tau_w_start = options.tau_w_start * second    # STDP convergence time (inverse of the learning rate)
tau_w_end = options.tau_w_start * second    # STDP convergence time (inverse of the learning rate)

tau_mom = options.tau_mom * second    # STDP convergence time (inverse of the learning rate)
tau_ref = 0 * second        # Refractory period

nu_av = options.nu_av * Hz        # Mean of the input rate
nu0 = 4. / 5. * nu_av    # Standard deviation of the input rate

V0 = 1 * mV                 # Each spike contribute to V0 into the post synaptic membrane potential
tau_PSP = options.tau_PSP * second   # Membrane potential time constant
nu_offset = nu_av           # Offset dynamic of the output rate
kappa0 = options.kappa / sqrt(float(Ndelays))     # Multiplive factive of the dynamic


if Nrec == -1:
    Nrec = Ndelays

print 'Setting learning rate.'
# Varying learning rate
N_iter = int(tmax/dt_record)
tau_w = tau_w_start * pow(tau_w_end/tau_w_start,arange(N_iter)/float(N_iter))
tau_w = TimedArray(tau_w,dt_record)


print 'Reading audio.'
# Load wave file
fswav,wav = wave.read(filename)
fswav = fswav * Hz

if len(shape(wav)) == 2: wav = wav[:,0]
N_audio = int(T_audio * fswav)
wav = array(wav[0:N_audio],dtype=float)
wav -= mean(wav)
wav /= max(abs(wav))

ta = TimedArray(wav,1./fswav)

Ninputs = Ndelays
print 'N inputs: {0}'.format(Ninputs)

# Init weights
w_0 = ones(Ninputs)
w_0 /= norm(w_0)


defaultclock.dt = 1. / FS
G_pre = NeuronGroup(Ninputs,'''
                    t_i = t - i * tau_delay : second
                    t_loop_i = t_i - floor(t/T_audio)*T_audio : second
                    x_th = ta(t_loop_i) : 1

                    d r_exp /dt = - r_exp / tau_omega : Hz
                    d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz
                    nu_in = nu0 * x_th + nu_av : Hz''',
                        threshold='nu_in * dt>rand()',
                        method="euler",
                        refractory=tau_ref,
                        reset='v=0 * volt')

kappa = kappa0 / V0 * FS
G_post = NeuronGroup(1,'''
                    d r_out_exp /dt = - r_out_exp / tau_omega : Hz
                    d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz
                    d v /dt = - v / tau_PSP : volt
                    nu_out  = nu_offset  + kappa * v : Hz
                    norm_w : 1''',
                threshold='nu_out * dt>rand()',
                reset='v=0 * volt',
                method="euler",
                refractory=tau_ref)
G_post.norm_w = '1'
S = Synapses(G_pre,G_post,
                 model='''
                 dw_STDP = - (r_out_exp_post - r_out_alpha_post) * (r_exp_pre - r_alpha_pre) / nu_av**2 : 1
                 d DW/dt = (dw_STDP - DW) / tau_mom : 1
                 dw/dt = DW / tau_w(t) : 1
                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre='''
                    v_post += w * V0 * dt / tau_PSP
                    r_exp_pre += 1 / tau_omega
                    ''',
                 post='''
                    r_out_exp_post += 1 / tau_omega / Ninputs''',
                 connect=True)

re_norm = S.run_regularly('w /= sqrt(norm_w_post)',when='end')
for Ni in range(Ninputs):S.w['i == %d' % Ni] = w_0[Ni]

mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(Nrec), dt=dt_record)
mon_post = StateMonitor(G_post, ['nu_out','r_out_alpha','norm_w'], record=array([0]), dt=dt_record)
mon_syn = StateMonitor(S, ['w'], record=np.arange(Nrec), dt=dt_record)

run(tmax, report="text")
device.build(directory= options.outdir + '/output', compile=True, run=True, debug=False)


# Printing results

av_rate_alpha = mean(array(mon_pre.r_alpha))
av_rate_alpha_out = mean(array(mon_post.r_out_alpha))
print 'Input average rate with alpha \t {0}'.format(av_rate_alpha)
print 'Output average rate with alpha \t {0}'.format(av_rate_alpha_out)
print 'Output max rate \t {0}'.format(max(array(mon_post.r_out_alpha[0,:])))
print 'Nu out min \t {0}, \t mean {1} \t max {2}'.format(min(array(mon_post.nu_out[0,:])),mean(array(mon_post.nu_out[0,:])),max(array(mon_post.nu_out[0,:])))

N = mon_post.r_out_alpha.size
T = arange(N) * 1./FS
w = mon_syn.w
w_end = w[:,-1]
w_end = w_end.ravel()

f = open(options.outdir + '/results.pickle','wb')
pickle.dump(
    {
        'rout':array(mon_post.r_out_alpha[0,:]),
        'nuout': array(mon_post.r_out_alpha[0,:]),
        'w': array(w),
        'wend':w_end,
        'T':T},f)

f = open(options.outdir +'/options.pickle','wb')
pickle.dump(options,f)


## Printing
plt.figure()
gs = gridspec.GridSpec(2,2)
gs.update(wspace=0.3,hspace=0.5)

weights = plt.subplot(gs[0,:])
filter = plt.subplot(gs[1,:])

sel = T > tmax - 1 *second
weights.locator_params(axis = 'y', nbins = 3)

delays = range(Nrec) * tau_delay / ms
inds = arange(0,Nrec,max(Nrec/15,1))
for k in inds:
    weights.plot(T,w[k,:].T,color='gray',linewidth=2.0)
weights.plot(T,w[0,:],color='red',linewidth=2.0)
weights.plot(T,w[-1,:],color='blue',linewidth=2.0)

# Titles

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')

filter.plot(delays,w_end,color='black',linewidth=2.0)
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (ms)')

filter.set_xlim([0,max(delays)])
filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig(options.outdir + '/fig9b_spiking_audio_review.pdf')
show()