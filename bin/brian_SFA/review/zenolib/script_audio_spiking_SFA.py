__author__ = 'guillaume'


import bin.input_loader.input_loader as il
from brian2 import *
import numpy.random as rd
import numpy as np
import scipy.io.wavfile as wave
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import os

from optparse import OptionParser
cmdOptParser = OptionParser()

set_device('cpp_standalone')
device.reinit()

cmdOptParser.add_option('-o',"--out",dest="outdir", action="store", type="string",help="Output results directory")
cmdOptParser.add_option("-v","--version",dest="version", action="store", type="int",help="version of test", default = 0)

cmdOptParser.add_option("-F","--FS",dest="FS", action="store", type="int",help="sampling frequency in simulation", default = 10000)
cmdOptParser.add_option("-t","--tmax",dest="tmax", action="store", type="float",help="Simulation duration in sec", default = 1000)
cmdOptParser.add_option("-T","--Taudio",dest="T_audio", action="store", type="float",help="Simulation duration in sec", default = 5)

cmdOptParser.add_option("-N","--Nd",dest="N_delays", action="store", type="int",help="number of delays", default = 64)
cmdOptParser.add_option("-d","--taud",dest="tau_delay", action="store", type="float",help="Input delay", default = 0.001)

cmdOptParser.add_option("-g","--taug",dest="tau_omega", action="store", type="float",help="STDP time width", default = 20e-3)
cmdOptParser.add_option("-w","--tauw",dest="tau_w", action="store", type="float",help="Learning rate", default = 10)

cmdOptParser.add_option("-n","--nuav",dest="nu_av", action="store", type="float",help="Input average frequency", default = 100)

cmdOptParser.add_option("-p","--taupsp",dest="tau_PSP", action="store", type="float",help="Membrane time constant", default = 1e-3)
cmdOptParser.add_option("-k","--kappa",dest="kappa", action="store", type="float",help="Multiplicating factor in dynamic", default = .2)

cmdOptParser.add_option("-S","--sphering",dest="sphering", action="store", type="int",help="Do shpering or not", default = 1)
cmdOptParser.add_option("-s","--seed",dest="seed", action="store", type="int",help="random seed", default = -1)

(options,args) = cmdOptParser.parse_args()
os.environ["MPLCONFIGDIR"] = options.outdir

filename='../../../../data/countryguit.wav'
seed = options.seed
if seed != -1: rd.seed(seed)

# Setting params


# Audio file parameter
FS = options.FS * Hz
tmax = options.tmax * second
T_audio = options.T_audio * second
dt_record = tmax / 1000

# Input parameters
Ndelays = options.N_delays       # Number of delays in the delay line
tau_delay = options.tau_delay     # Delay between each inputs of the delay line
epsi = 1e-6

# Neuron parameters
tau_omega = options.tau_omega * second     # SDTP window width parameter, it also defines the spike rate
tau_w = options.tau_w * second    # STDP convergence time (inverse of the learning rate)
tau_ref = 0 * second        # Refractory period

nu_av = options.nu_av * Hz        # Mean of the input rate
nu0 = 4. / 5. * nu_av    # Standard deviation of the input rate

V0 = 1 * mV                 # Each spike contribute to V0 into the post synaptic membrane potential
tau_PSP = options.tau_PSP * second   # Membrane potential time constant
nu_offset = nu_av           # Offset dynamic of the output rate
kappa0 = options.kappa      # Multiplive factive of the dynamic

sphering = options.sphering

# Load wave file
fswav,wav = wave.read(filename)
fswav = fswav * Hz

if len(shape(wav)) == 2: wav = wav[:,0]
N_audio = int(T_audio * fswav)
wav = array(wav[0:N_audio],dtype=float)
inp = il.Input(0,T_audio,N_audio)
inp.x = wav
inp.whitenInputs()
inp.expandWithDelay(Ndelays,tau_delay)
if sphering:
    X,R_proj = inp.sphereInputs(degenerate=False,epsi=epsi)
    inp.whitenInputs()
else:
    X = inp.X
    R_proj = eye(Ndelays)

Ninputs = inp.n
ta = TimedArray(X.T,1./fswav)
print 'N inputs: {0}'.format(Ninputs)

# Init weights
w_0 = np.random.randn(Ninputs)
w_0 /= norm(w_0)
wR_0 = dot(w_0,R_proj)
assert dot(w_0,R_proj).size == Ndelays, 'Size should be the number of delays '

if sphering:
    print 'Check correlation.'
    for ii in arange(Ninputs):
        x_i = X[ii,:]
        n_i = sqrt(sum(x_i**2))
        for jj in arange(ii):
            x_j = X[jj,:]
            n_j = sqrt(sum(x_j**2))
            assert std(x_i) > .99 and std(x_i) < 1.01, 'Std of each row should be one. It is {0} for row {1}'.format(std(x_i),ii)
            rho = dot(x_i,x_j) / len(x_i)
            assert rho < epsi, 'Correlation {0},{1} is too high {2}'.format(ii,jj,rho)

G_pre = NeuronGroup(Ninputs,'''
                    t_loop = t - floor(t/T_audio)*T_audio : second
                    x_th = ta(t_loop,i) : 1

                    d r_exp /dt = - r_exp / tau_omega : Hz
                    d r_alpha /dt = (r_exp - r_alpha) / tau_omega : Hz
                    nu_in = nu0 * x_th + nu_av : Hz''',
                        threshold='nu_in * dt>rand()',
                        method="euler",
                        refractory=tau_ref,
                        reset='v=0 * volt')

kappa = kappa0 / V0 * FS
G_post = NeuronGroup(1,'''
                    d r_out_exp /dt = - r_out_exp / tau_omega : Hz
                    d r_out_alpha /dt = (r_out_exp - r_out_alpha) / tau_omega : Hz
                    d v /dt = - v / tau_PSP : volt
                    nu_out  = nu_offset  + kappa * v : Hz
                    sum_w : 1
                    norm_w : 1''',
                threshold='nu_out * dt>rand()',
                reset='v=0 * volt',
                method="euler",
                refractory=tau_ref)
G_post.norm_w = '1'
S = Synapses(G_pre,G_post,
                 model='''
                 w : 1
                 sum_w_post = w / Ninputs : 1 (summed)
                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre='''
                    w += -(r_out_exp_post - r_out_alpha_post) / nu_av**2 / tau_w
                    v_post += w * V0 * dt / tau_PSP
                    r_exp_pre += 1 / tau_omega
                    ''',
                 post='''
                    w += -(r_exp_pre - r_alpha_pre) / nu_av**2 / tau_w
                    r_out_exp_post += 1 / tau_omega / Ninputs''',
                 connect=True)

zero_mean = S.run_regularly('w -= sum_w_post',when='end')
re_norm = S.run_regularly('w /= sqrt(norm_w_post)',when='end')
for Ni in range(Ninputs):S.w['i == %d' % Ni] = w_0[Ni]

mon_pre = StateMonitor(G_pre, ['nu_in','r_alpha','x_th'], record=arange(Ninputs), dt=dt_record)
mon_post = StateMonitor(G_post, ['nu_out','r_out_alpha','norm_w'], record=array([0]), dt=dt_record)
mon_syn = StateMonitor(S, ['w'], record=np.arange(Ninputs), dt=dt_record)

spikes_pre = SpikeMonitor(G_pre)
spikes_post = SpikeMonitor(G_post)

run(tmax, report="text")
device.build(directory=options.outdir + '/output', compile=True, run=True, debug=False)

# Printing results
av_rate_alpha = mean(array(mon_pre.r_alpha))
av_rate = array(spikes_pre.t).size / Ndelays / tmax
av_rate_out = array(spikes_post.t).size / tmax
av_rate_alpha_out = mean(array(mon_post.r_out_alpha))
print 'Input average rate \t  {0} \t with alpha \t {1}'.format(av_rate,av_rate_alpha)
print 'Output average rate \t {0} \t with alpha \t {1}'.format(av_rate_out,av_rate_alpha_out)
print 'Output max rate \t {0}'.format(max(array(mon_post.r_out_alpha[0,:])))
print 'Nu out min \t {0}, \t mean {1} \t max {2}'.format(min(array(mon_post.nu_out[0,:])),mean(array(mon_post.nu_out[0,:])),max(array(mon_post.nu_out[0,:])))


N = mon_post.r_out_alpha.size
T = arange(N) * FS
w = mon_syn.w
wR_end = w[:,-1]
wR_end = dot(wR_end,R_proj).ravel()

obj = {'T': T, 'w': np.array(w), 'w_end': np.array(wR_end), 'r_out': np.array(mon_post.r_out_alpha), 'R_proj': R_proj, 'wR_0': wR_0}

f = open(options.outdir + '/results.pickle','wb')
pickle.dump(obj,f)


## Printing
plt.figure()
gs = gridspec.GridSpec(3,2)
gs.update(wspace=0.3,hspace=0.5)

raster = plt.subplot(gs[0,0])
weights = plt.subplot(gs[1,:])
filter = plt.subplot(gs[2,:])

delays = range(Ndelays) * tau_delay / ms
inds = range(0,Ndelays,3)
for k in inds:
    weights.plot(T,w[k,:].T,color='gray',linewidth=2.0)
weights.plot(T,w[0,:],color='red',linewidth=2.0)
weights.plot(T,w[-1,:],color='blue',linewidth=2.0)

# Titles
ind_t = spikes_pre.t < 1. * second
ind_i = spikes_pre.i[ind_t] < 20
raster.plot(spikes_pre.t[ind_t][ind_i], spikes_pre.i[ind_t][ind_i], '.')
raster.set_yticks([])
raster.set_xticks([0,1])
raster.set_xlim([0,1])
raster.set_ylim([0,20])
raster.yaxis.tick_left()

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')

filter.plot(delays,wR_end,color='black',linewidth=2.0)
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (ms)')

filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig(options.outdir + '/fig9b_spiking_audio_review.pdf')

