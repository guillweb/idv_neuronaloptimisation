__author__ = 'guillaume'


import bin.input_loader.input_loader as il
from brian2 import *
import numpy.random as rd
import numpy as np
import scipy.io.wavfile as wave
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import json
import cPickle as pickle

from optparse import OptionParser
cmdOptParser = OptionParser()

set_device('cpp_standalone')
device.reinit()

cmdOptParser.add_option('-o',"--out",dest="outdir", action="store", type="string",help="Output results directory")
cmdOptParser.add_option("-v","--version",dest="version", action="store", type="int",help="version of test", default = 0)

cmdOptParser.add_option("-F","--FS",dest="FS", action="store", type="int",help="sampling frequency in simulation", default = 10000)
cmdOptParser.add_option("-t","--tmax",dest="tmax", action="store", type="float",help="Simulation duration in sec", default = 1000)
cmdOptParser.add_option("-T","--Taudio",dest="T_audio", action="store", type="float",help="Simulation duration in sec", default = 5)

cmdOptParser.add_option("-N","--Nd",dest="N_delays", action="store", type="int",help="number of delays", default = 64)
cmdOptParser.add_option("-d","--taud",dest="tau_delay", action="store", type="float",help="Input delay", default = 0.001)

cmdOptParser.add_option("-g","--taug",dest="tau_omega", action="store", type="float",help="STDP time width", default = 20e-3)
cmdOptParser.add_option("-w","--tauw",dest="tau_w", action="store", type="float",help="Learning rate", default = 10)

cmdOptParser.add_option("-n","--nuav",dest="nu_av", action="store", type="float",help="Input average frequency", default = 100)

cmdOptParser.add_option("-p","--taupsp",dest="tau_PSP", action="store", type="float",help="Membrane time constant", default = 1e-3)
cmdOptParser.add_option("-k","--kappa",dest="kappa", action="store", type="float",help="Multiplicating factor in dynamic", default = .2)

cmdOptParser.add_option("-S","--sphering",dest="sphering", action="store", type="int",help="Do shpering or not", default = 1)
cmdOptParser.add_option("-s","--seed",dest="seed", action="store", type="int",help="random seed", default = -1)

(options,args) = cmdOptParser.parse_args()

filename='../../../data/countryguit.wav'
seed = options.seed
if seed != -1: rd.seed(seed)

# Setting params


# Audio file parameter
FS = options.FS * Hz
tmax = options.tmax * second
T_audio = options.T_audio * second
dt_record = tmax / 1000

# Input parameters
Ndelays = options.N_delays       # Number of delays in the delay line
tau_delay = options.tau_delay     # Delay between each inputs of the delay line
epsi = 1e-6

# Neuron parameters
tau_omega = options.tau_omega * second     # SDTP window width parameter, it also defines the spike rate
tau_w = options.tau_w * second    # STDP convergence time (inverse of the learning rate)
tau_ref = 0 * second        # Refractory period

nu_av = options.nu_av * Hz        # Mean of the input rate
nu0 = 4. / 5. * nu_av    # Standard deviation of the input rate

V0 = 1 * mV          # Each spike contribute to V0 into the post synaptic membrane potential
tau_PSP = options.tau_PSP        # Membrane potential time constant
nu_offset = nu_av      # Offset dynamic of the output rate
kappa0 = options.kappa              # Multiplive factive of the dynamic

sphering = options.sphering

f = open(options.outdir + '/saved.json','wb')
pickle.dump({'FS': np.array(FS),'kappa': np.array(kappa0)},f)