__author__ = 'guillaume'

from guillaume_toolbox.optimization.eigs_dimension_reduction import PCA
import bin.input_loader.input_loader as il
from brian2 import *
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave
import scipy
from numpy import *
import numpy.random as rd

tmax = 100000 * second
T_audio = 4 * second
tau_PSP = 1 * ms
tau_omega = 4 * ms
FS = 10000 * Hz

tau_w_start = .001 * second
tau_w_end = 100. * second

Ndelays = 840
tau_delay = .2 * ms

epsi = 1e-6
nu = .1

tau_mom = 1000 * T_audio
mu = exp(- T_audio / tau_mom)

nu_av = 100. * Hz
nu_0 = 4. / 5. * nu_av
nu_offset = nu_av
kappa0 = .5
kappa = kappa0 / sqrt(Ndelays)


sphering = False
set_offset = True
sub_mean = False
simulate_bias = True
rotate = False

dt = 1. / FS
N_filt = int(tau_omega * 20 * FS)
T_filt = arange(start=-N_filt,stop=N_filt) * dt

filename = '../../../data/countryguit.wav'

def naive_resample(wave,FS_in,FS_out):
    FS_in = float(FS_in)
    FS_out = float(FS_out)

    N_in = len(wave)
    T = N_in / FS_in

    N_out = int(T * FS_out)
    t_out = arange(N_out) / FS_out
    idx = array(t_out * FS_in,dtype=int)

    w_out = wave[idx]
    assert len(w_out) == N_out, 'Wrong size'
    return w_out

def convolve(X,f):
    X + Hz
    X = X / Hz
    n,N = shape(X)

    X_out = zeros(X.shape)
    X_mean = mean(X,axis=1)
    X_in = (X.T - X_mean).T

    for Ni in arange(n):
        X_out[Ni,:] = scipy.signal.fftconvolve(X_in[Ni,:],f,'same')
    return (X_out.T + X_mean).T * Hz
# Define filters

def exp_convolve(X,tau,dt):
    X = X / Hz
    n,N = shape(X)
    X_exp = zeros(shape(X))
    X_exp[:,0] = X[:,0] * dt/tau

    al = dt/tau
    for k in arange(N-1):
        X_exp[:,k+1] = (1 - al) * X_exp[:,k] + X[:,k] * al

    return X_exp * Hz


f_al = (T_filt >= 0) * T_filt/ tau_omega **2 * exp(- T_filt/tau_omega) * dt
f_exp = dt / tau_omega * (T_filt >= 0) * exp(- T_filt/tau_omega)
#f_av = dt / tau_av * (T_filt >= 0) * exp(- T_filt / tau_av)
f_psp = dt / tau_PSP * (T_filt >= 0) * exp(- T_filt/tau_PSP)

f_d = f_exp - f_al
f_bias = f_d * scipy.signal.fftconvolve(f_d / dt,f_psp,'same')



# Load wave file
fswav,wav = wave.read(filename)
if len(shape(wav)) == 2: wav = wav[:,0]
wav = wav[:int(fswav * Hz * T_audio)]
wav = naive_resample(wav,fswav,FS)
if rotate: wav = roll(wav,wav.size/2)
wav = array(wav,dtype=float)

inp = il.Input(0,T_audio,len(wav))
inp.x = wav
inp.whitenInputs()

inp.expandWithDelay(Ndelays,tau_delay)
if sphering:
    inp_f = il.Input(0,T_audio,len(wav))
    inp_f.x = wav
    inp_f.whitenInputs()
    inp_f.expandWithDelay(Ndelays,tau_delay)
    X_f = convolve(inp_f.X * Hz, f_al) / Hz

    pca = PCA(X_f.T,std_min=epsi)
    print 'Dimension kept: ', pca.n_out
    X = pca.transform(inp.X.T).T
    R_proj_inv = pca.R_proj_inv.T
else:
    X = inp.X
    R_proj_inv = eye(Ndelays)
X /= amax(abs(X))

if set_offset:
    X = X*nu_0
    X = X+nu_av

print 'Input statistics :'
print amin(X,axis=1)
print mean(X,axis=1)
print amax(X,axis=1)



X_exp = exp_convolve(X,tau_omega,1./FS)
X_al = exp_convolve(X_exp,tau_omega,1./FS)
X_d = X_exp - X_al
X_d_xi = exp_convolve(X_d,tau_PSP,1./FS)

X_xi = exp_convolve(X,tau_PSP,1./FS)

print 'Filtered input statistics :'
print amin(X_xi,axis=1)
print mean(X_xi,axis=1)
print amax(X_xi,axis=1)

w = ones(shape(X)[0])
w /= norm(w)

def get_rate_out(w): return nu_offset + kappa * dot(w,X_xi)

R_out = get_rate_out(w)

print 'nu_out'
print amin(R_out)
print mean(R_out)
print amax(R_out)

assert amin(R_out) > 0, 'Rate has to be always positive'

if set_offset:
    K = dot(X_d / nu_av,X_d_xi.T / nu_av)
else:
    K = dot(X_d,X_d_xi.T)

sX_bias = sum(X /nu_av**2 ,axis=1) * dt/ tau_omega

N_iter = int(tmax/T_audio)
cost = zeros(N_iter)
print 'N iterations: ',N_iter

#plt.plot(inp.T,X_d.T)
# plt.figure()
# plt.subplot(2,1,1)
# plt.plot(inp.T,X_al.T)
# plt.subplot(2,1,2)
# plt.plot(inp.T,R_out)
# plt.show()

b=0
dW_exp = 0
dW_al = 0
tau_w = tau_w_start

N_records = 20
q_rec = N_iter / N_records
w_storage = zeros((Ndelays,N_records))

tau_w_list = tau_w_start * power(tau_w_end/tau_w_start,arange(N_iter)/float(N_iter))
for k in arange(N_iter):
    tau_w = tau_w_list[k]
    dw = - kappa * dot(K,w.T) * dt / tau_w
    dw += nu * rd.randn(Ndelays) * T_audio / tau_w / sqrt(T_audio / dt)
    cost[k] = - dot(w,dw)

    if simulate_bias:
        b = kappa * w * sX_bias
        dw += b * dt / tau_w

    dW_exp = mu * dW_exp + (1-mu) * dw
    dW_al = mu * dW_al + (1-mu) * dW_exp
    w += dW_al
    w /= norm(w)

    if k >= N_iter - N_records:
        w_storage[:,mod(k,N_records)] = w

    if mod(k,q_rec) == 0:
        R_out = get_rate_out(w)
        mm = mean(R_out)
        ss = std(R_out)
        print 'min {0} \t lower {1} \t ss {2} \t max {3}'.format(amin(R_out), mm - ss/2, ss, amax(R_out))

figure()
plt.plot(tau_w_list)
print norm(b),norm(dw)

out = get_rate_out(w)

plt.figure()
plt.subplot(3,1,1)
plt.plot(cost)
plt.subplot(3,1,2)
plt.plot(arange(Ndelays) * tau_delay,w_storage)
plt.subplot(3,1,3)
plt.plot(out)

plt.show()