from brian2 import *
from runner_brian import run_SFA_spiking_lambda
from runner_brian import plot_fancy
from runner_brian import check_validity
import matplotlib.pyplot as plt
import bin.brian_gradient_optimisation.tools as bt
import bin.input_loader.input_loader as il

tau_omega_list = [1.]*ms
nu_av_list = 4*np.power(10.,[3,3,2,1]) *Hz
nu0_list = .8 * nu_av_list
tau_w0_list = [2.]*ms #[10,2,10,10] * ms
tau_derivation_list = [1,1,10,100] * ms

tmax = 800 * second
FS = 10**4 * Hz

w_0 =  np.random.randn(5,1)
w_0 /= norm(w_0)



# Define input
#N = int(tmax*10**4*Hz)
#inp = il.Input(0*second,tmax,N)
#inp.set_wiskott_classic(1*Hz)
#inp.sphereInputs()
#Texp = inp.tmax - inp.tmin



OUT = []
WEIGHTS = []
for idx_tau in arange(len(tau_omega_list)):
    tau_omega = tau_omega_list[idx_tau]
    nu0 = nu0_list[idx_tau]
    nu_av = nu_av_list[idx_tau]
    tau_w0 = tau_w0_list[idx_tau]
    tau_derivation = tau_derivation_list[idx_tau]
    T,x,x_alpha,s,s_alpha,w,post_idx = run_SFA_spiking_lambda.run_SFA_spiking_lambda(FS=FS,tau_omega=tau_omega,tmax=tmax,tau_w0=tau_w0,tau_derivation=tau_derivation,w_0=w_0)

    OUT.append(s_alpha)
    WEIGHTS.append(w)


OUT_summed = []
for idx_tau in arange(len(tau_omega_list)):
    s = OUT[idx_tau]
    #plot_fancy.plot_s(T,s,one_fig=True,tmin=tmax- 4*second)
    #title(tau_omega_list[idx_tau])


    dtRecord = tmax /len(T)
    T_summed,s_summed = bt.period_sum(T,s,0.75*tmax,1 * Hz,1/dtRecord)
    OUT_summed.append(s_summed)


    plot_fancy.plot_s(T_summed,s_summed,one_fig=True)
    title(tau_omega_list[idx_tau])

plt.show()



for idx_tau in arange(len(tau_omega_list)):
    w = WEIGHTS[idx_tau]
    plot_fancy.plot_w(T,w,post_idx,one_fig=True)
    title(tau_omega_list[idx_tau])
plt.show()

plt.figure()
corr = check_validity.check_correlation([T_summed],OUT_summed)
print corr
pcolor(corr)
show()