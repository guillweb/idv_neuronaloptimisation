from brian2 import *
from runner_brian import run_SFA_continuous_lambda
from runner_brian import plot_fancy
from runner_brian import check_validity
import matplotlib.pyplot as plt
import cPickle as pickle
import time

save = False
print_fig = True

tau_omega_list = [0,0.1,1.,10.,100.]*ms
tau_w = 2 * second
tmax = 1000 * second
tau_derivation = 10 * ms

w_0 =  np.random.randn(5,1)
w_0 /= norm(w_0)

OUT = [[0]]
WEIGHTS = [[0]]
for idx_tau in arange(len(tau_omega_list)):
    tau_omega = tau_omega_list[idx_tau]
    if tau_omega > 0:
        T,x_th,x_alpha,s,s_al,w,w_al,post_idx = run_SFA_continuous_lambda.run_SFA_continuous_lambda(tau_omega=tau_omega,tmax=tmax,tau_w=tau_w,tau_derivation=tau_derivation,w_0=w_0)


        OUT.append(s_al)
        WEIGHTS.append(w_al)

OUT[0] = s
WEIGHTS[0] = w

if print_fig:
    for idx_tau in arange(len(tau_omega_list)):
        s = OUT[idx_tau]
        plot_fancy.plot_s(T,s,one_fig=True,tmin=tmax- 4*second)
        title(tau_omega_list[idx_tau])
    plt.show()


    for idx_tau in arange(len(tau_omega_list)):
        w = WEIGHTS[idx_tau]
        plot_fancy.plot_w(T,w,post_idx,one_fig=True)
        title(tau_omega_list[idx_tau])
    plt.show()

    plt.figure()
    corr = check_validity.check_correlation([T],OUT)
    print corr
    pcolor(corr)
    show()

if save:
    params = { 't': time.clock(),
               'tau_omega_list': tau_omega_list,
               'tmax': tmax,
               'tau_w': tau_w,
               'tau_derivation': tau_derivation,
               'w_0': w_0}

    saved_obj = {'params': params, 'OUT': OUT, 'T_list': [T]}
    file_path = 'results_tau/coutinuous.pickle'

    f = open(file_path,'a')
    pickle.dump(saved_obj,f)