from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time

set_device('cpp_standalone')

#-------------
# Parameters
#-------------

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

fWiskott = 1 * Hz

# Sampling parameters
tmin = 0 * second
tmax = 300 * second

FS = 5000 * Hz
N = int((tmax - tmin) * FS)

# Filtering and window length parameter
tau_omega = 10 * ms

# Time constants
tau_derivation = 0.01/fWiskott #0.01/fWiskott
tau_w = 30. * second


Noutputs = 4


print 'fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

# -------
# Solver
# -------

tCount = time.clock()

# Define input
inp = il.Input(tmin,tmax,N)
inp.set_wiskott_classic(fWiskott)
inp.sphereInputs()
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')

# ------------- Brian
Texp = inp.tmax - inp.tmin
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# -----------------------
X_in = inp.X
tCount = ti.timeCount(tCount,'High pass input setup (s):')

print X_in.shape
ta0 = TimedArray(X_in[0,:],Dt)
ta1 = TimedArray(X_in[1,:],Dt)
ta2 = TimedArray(X_in[2,:],Dt)
ta3 = TimedArray(X_in[3,:],Dt)
ta4 = TimedArray(X_in[4,:],Dt)


G_pre = NeuronGroup(Ninputs, '''


                                    x_th = int(i==0) * ta0(t) \
                                    + int(i==1) * ta1(t) \
                                    + int(i==2) * ta2(t) \
                                    + int(i==3) * ta3(t) \
                                    + int(i==4) *ta4(t) : 1

                                    d x_exp /dt = (x_th - x_exp ) / tau_omega : 1
                                    d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1


                                    x_old : 1
                                    Dx : 1
                                    D2x : 1

                                    dx :1
                                    dx_al : 1

                                    x_al_old : 1
                                    Dx_al : 1
                                    D2x_al : 1''')

cs = G_pre.custom_operation('''
                dx = tau_derivation / dt * (x_th - x_old)
                D2x = tau_derivation / dt * (dx - Dx)
                Dx = dx

                dx_al = tau_derivation / dt * (x_alpha - x_al_old)
                D2x_al = tau_derivation / dt * (dx_al - Dx_al)
                Dx_al = dx_al

                x_al_old = x_alpha
                x_old = x_th ''',
                when='train_reset')

G_post = NeuronGroup(Noutputs,'''
            s : 1
            s_al : 1

            d s_al_exp / dt = (s_al - s_al_exp) / tau_omega : 1
            d s_al_alpha / dt = (s_al_exp - s_al_alpha) / tau_omega : 1

            norm_w_al : 1
            norm_w : 1''')

G_post.norm_w = '1'
G_post.norm_w_al = '1'

S = Synapses(G_pre,G_post,
             model='''
             STDP = int(j==0) * D2x_pre * s_post \
             + int(j==1) * Dx_pre * s_post \
             + int(j==2) * x_th_pre * s_post \
             - int(j==3) * x_th_pre * s_post: 1

             STDP_al = int(j==0) * D2x_al_pre * s_al_alpha_post \
             + int(j==1) * Dx_al_pre * s_al_alpha_post \
             + int(j==2) * x_alpha_pre * s_al_alpha_post \
             - int(j==3) * x_alpha_pre * s_al_alpha_post: 1

             d w / dt = (t > tau_omega) * STDP / tau_w : 1
             d w_al / dt = (t > tau_omega) * STDP_al / tau_w : 1

             s_al_post = w_al * x_th_pre : 1 (summed)
             norm_w_al_post = w_al * w_al : 1 (summed)

             s_post = w * x_th_pre / Ninputs : 1 (summed)
             norm_w_post = w*w / Ninputs : 1 (summed)''',
             connect=True)

re_norm = S.custom_operation('''
            w_al /= sqrt(norm_w_al_post)
            w /= sqrt(norm_w_post)
            ''',when='weight_renorm')


a = np.random.randn(Ninputs)
a /= norm(a)
for Ni in range(0,Ninputs):
    S.w['i == %d' % Ni] = a[Ni]
    S.w_al['i == %d' % Ni] = a[Ni]


MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets','weight_renorm','end']



mon_pre = StateMonitor(G_pre, ['x_exp','x_alpha','x_th','D2x','D2x_al'], record=arange(0,Ninputs))
mon_post = StateMonitor(G_post, ['s','s_al','norm_w'], record=arange(0,Noutputs))
mon_syn = StateMonitor(S, ['w','w_al'], record=arange(0,Ninputs*Noutputs))

tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp, report="text")
device.build(project_dir='output', compile_project=True, run_project=True, debug=False)
tCount = ti.timeCount(tCount,'Brian run (s):')


print "Max of s:"
print np.max(np.abs(mon_post.s),axis=1)
print np.max(np.abs(mon_post.s_al),axis=1)
print np.max(np.abs(mon_post.norm_w),axis=1)

print "Max of x:"
print np.max(np.abs(mon_pre.x_exp),axis=1)
print np.max(np.abs(mon_pre.x_alpha),axis=1)
print np.max(np.abs(mon_pre.x_th),axis=1)
print np.max(np.abs(mon_pre.D2x),axis=1)
print np.max(np.abs(mon_pre.D2x_al),axis=1)


print "Variance of x:"
print np.dot(mon_pre.x_th,mon_pre.x_th.T)
print np.var(mon_pre.x_alpha,axis=1)

print 'Weights: '
print S.w



matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

color = ["green","red","purple","brown"]
win = [r'$\Lambda_{SFA} = d_{(2)}$',r'$\Lambda_{Classic} = d_{(1)} $',r'$\Lambda_{Control+} = \delta_0$',r'$\Lambda_{Control-} = - \delta_0$']

tstart = 0
tend = len(inp.T)

plt.figure()
plt.plot(mon_post.norm_w.T)
plt.show()

tstart = int(1 - 4/(fWiskott* (tmax - tmin)) * len(inp.T))
tend = len(inp.T)

for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(inp.T[tstart:tend],mon_post.s[i,tstart:tend],'r--',color=color[i],label=win[i],linewidth=2.0)
    plt.plot(inp.T[tstart:tend],mon_post.s_al[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 2)
    #xlim([0,0.25])
    #ylim([-0.01,0.01])
    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])

    xlabel(r't (s)')
    ylabel(r'$s$')

    tight_layout()
    savefig('../../figures/s_comp_online_spiking_{0}.pdf'.format(i))
show()



for No in range(Noutputs-1,-1,-1):
    plt.figure()
    idx_post = find(S._postsynaptic_idx == No)


    plt.plot(inp.T,mon_syn[idx_post].w.T,'r--',color=color[No],linewidth=2.0)
    plt.plot(inp.T,mon_syn[idx_post].w_al.T,color=color[No],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'weights')
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)

    tight_layout()
    savefig('../../figures/w_comp_online_spiking_{0}.pdf'.format(i))

show()


pass
