from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time

#brian_prefs.codegen.target = 'weave'
set_device('cpp_standalone')

#-------------
# Parameters
#-------------

#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

pre_filter = False
fWiskott = 1 * Hz

# Sampling parameters
tmin = 0 * second
tmax = 200 * second
FS = 10000 * Hz
N = int((tmax - tmin) * FS)

# Filtering and window length parameter
tau_omega = 10 * ms
inverse_period = 511/FS

# rate paramters
nu0 = 80 * Hz
nu_av = 100 * Hz

# Time constants
tau_average = 5 * second
tau_derivation = 0.01/fWiskott
tau_w = 5. * ms


Noutputs = 4

dtRecord = 1/(500 * Hz)


Vthr = 10 * mV

print 'fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)


# -------
# Solver
# -------

tCount = time.clock()



# Define input
inp = il.Input(tmin,tmax,N)
inp.set_wiskott_classic(fWiskott)
inp.sphereInputs()
Texp = inp.tmax - inp.tmin
tCount = ti.timeCount(tCount,'Input setup (s):')

# ------------- Brian
Texp = inp.tmax - inp.tmin
Dt = inp.dt
defaultclock.dt = Dt
Ninputs = inp.n

# -----------------------
if pre_filter:
    builder = fb.Filter(inp.T,inverse_period)
    # Get the inverse filter of an alpha_filter
    al = (builder.T >= 0) * builder.T/tau_omega**2 * exp(-builder.T/tau_omega) * Dt
    #al = (builder.T >= 0) * 1/tau_omega * exp(-builder.T/tau_omega) * Dt
    inv_al = builder.compute_fourier_inverse(al,10**-5)
    shp = shape(inp.X)
    X_in = np.zeros(shp)
    for Ni in range(0,shp[0]):
        X_in[Ni,:] = scipy.signal.fftconvolve(inp.X[Ni,:],inv_al,'same')
else:
    X_in = inp.X
tCount = ti.timeCount(tCount,'High pass input setup (s):')

print X_in.shape
ta0 = TimedArray(X_in[0,:],Dt)
ta1 = TimedArray(X_in[1,:],Dt)
ta2 = TimedArray(X_in[2,:],Dt)
ta3 = TimedArray(X_in[3,:],Dt)
ta4 = TimedArray(X_in[4,:],Dt)





G_pre = NeuronGroup(Ninputs, '''
                                    x_train : 1
                                    d x_av /dt = (x_train - x_av) / tau_average : 1
                                    x = x_train - x_av : 1

                                    d x_exp /dt = (x - x_exp ) / tau_omega : 1
                                    d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1

                                    x_th = int(i==0) * ta0(t) \
                                    + int(i==1) * ta1(t) \
                                    + int(i==2) * ta2(t) \
                                    + int(i==3) * ta3(t) \
                                    + int(i==4) *ta4(t) : 1

                                    x_al_old : 1
                                    Dx : 1
                                    D2x : 1

                                    rt = nu0 * x_th + nu_av : Hz
                                    v : volt ''',
                    threshold='v>Vthr*rand()',
                    reset='v=0 * volt')

cs = G_pre.custom_operation('''
                v = rt * dt * Vthr
                dx = tau_derivation / dt * (x_alpha - x_al_old)

                D2x = tau_derivation / dt * (dx - Dx)
                Dx = dx
                x_al_old = x_alpha

                x_train = 0 ''',when='train_reset')

G_pre.x_av = nu_av * Dt

G_post = NeuronGroup(Noutputs,'''
            r : 1
            d r_av /dt = (r - r_av) / tau_average : 1

            s = r - r_av: 1
            ind = i : 1

            d s_exp / dt = (s - s_exp) / tau_omega : 1
            d s_alpha / dt = (s_exp - s_alpha) / tau_omega : 1

            norm_w : 1''')
G_post.norm_w = '1'

S = Synapses(G_pre,G_post,
             model='''
             STDP = int(j==0) * 0.25 * ( (x_alpha_pre - x_exp_pre) * s_post + x_pre * (s_alpha_post - s_exp_post) )\
             + int(j==1) * 0.5 *( x_exp_pre * s_post - x_pre * s_exp_post ) \
             + int(j==2) * 0.5 *( x_exp_pre * s_post + x_pre * s_exp_post ) \
             - int(j==3) * 0.5 *( x_exp_pre * s_post + x_pre * s_exp_post ) : 1


             dw / dt = int(t > 2*tau_average) *  STDP / tau_w : 1


             r_post = w * x_train_pre : 1 (summed)
             norm_w_post = w*w : 1 (summed)''',

             pre="x_train_pre += 1",
             connect=True)

re_norm = S.custom_operation('''w /= sqrt(norm_w_post)''',when='weight_renorm')


a = np.random.randn(Ninputs)
a /= norm(a)
for Ni in range(0,Ninputs):
    S.w['i == %d' % Ni] = a[Ni]


MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets','weight_renorm','end']



mon_pre = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av','x_th','Dx','D2x'], record=arange(0,Ninputs),dt=dtRecord)
mon_post = StateMonitor(G_post, ['s','s_alpha','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
mon_syn = StateMonitor(S, ['w'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

tCount = ti.timeCount(tCount,'Brian setup (s):')
run(Texp, report="text")
device.build(project_dir='output', compile_project=True, run_project=True, debug=False)
tCount = ti.timeCount(tCount,'Brian run (s):')

Trec = range(0,len(mon_post.s[0,:])) * dtRecord

print 'Weights: '
print S.w

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

color = ["green","red","purple","brown"]
win = [r'$\Omega_{SFA} = d_{(2)}$',r'$\Omega_{Classic} = d_{(1)} $',r'$\Omega_{Control+} = \delta_0$',r'$\Omega_{Control-} = - \delta_0$']

tstart = 0
#tend = int(1 * second/ (tmax - tmin) * len(Trec))
tend = len(Trec)



T_summed,x_summed = bt.period_sum(Trec,mon_pre.x_alpha,2*tau_average,fWiskott,1/dtRecord)
T_summed,x_summed_exp = bt.period_sum(Trec,mon_pre.x_exp,2*tau_average,fWiskott,1/dtRecord)
T_summed,x_summed_real = bt.period_sum(inp.T,inp.X,2*tau_average,fWiskott,FS)
T_summed,x_summed_filtered = bt.period_sum(inp.T,X_in,2*tau_average,fWiskott,FS)

plt.figure()
for i in range(0,4):
    plt.subplot(221+i)
    title('X spike filtered summed')
    plt.plot(x_summed[i,:].T)

plt.figure()
for i in range(0,4):
    plt.subplot(221+i)
    title('X spike filtered summed')
    plt.plot(x_summed_filtered[i,:].T)

show()


for i in range(Noutputs-1,-1,-1):
    plt.figure()
    plt.plot(Trec[tstart:tend],mon_post.s_alpha[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 2)
    #xlim([0,0.25])
    #ylim([-0.01,0.01])
    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])

    xlabel(r't (s)')
    ylabel(r'$s$')

    #tight_layout()
    #savefig('../../figures/spiking_{0}.pdf'.format(i))
show()



T_summed,s_summed = bt.period_sum(Trec,mon_post.s_alpha,tmax/2,fWiskott,1/dtRecord)

for No in range(0,Noutputs):

    plt.figure()
    plt.plot(T_summed,s_summed[No,:],color=color[No],label=win[No],linewidth=2.0)
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 2)
    #xlim([0,0.25])
    #ylim([-0.01,0.01])
    figtext(0.55, 0.88, win[No], ha="center", va="bottom", size="medium",color=color[No])

    xlabel(r't (s)')
    ylabel(r'$s$')


show()


for No in range(Noutputs-1,-1,-1):
    plt.figure()
    idx_post = find(S._postsynaptic_idx == No)


    plt.plot(Trec,mon_syn[idx_post].w.T,color=color[No],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'weights')
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)
    #tight_layout()
    #savefig('../../figures/w_online_{0}.pdf'.format(No))

show()


pass
