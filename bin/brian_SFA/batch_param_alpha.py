from brian2 import *
from runner_brian import run_SFA_batch_lambda
from runner_brian import result_saver
from runner_brian import plot_fancy
from runner_brian import check_validity
import matplotlib.pyplot as plt
import pickle
import time
#import cPickle as pickle

file_path = 'results_alpha/batch/'
save = True
print_fig = False
loop_it = 20

noise_coeff_list = 0. * np.power(100.,range(-3,4))
cos_coeff_list = np.power(100.,range(-3,4))


tmax = 4 * second
tau_omega= 0 * ms
N_iteration = 10**5

for i in range(0,loop_it):

    w_0 =  np.random.randn(5,1)
    w_0 /= norm(w_0)

    OUT = []
    T_list = []
    for idx_al in arange(len(noise_coeff_list)):
        noise_coeff = noise_coeff_list[idx_al]
        cos_coeff = cos_coeff_list[idx_al]
        T,x,s = run_SFA_batch_lambda.run_SFA_batch_lambda(cos_coeff=cos_coeff,tau_omega=tau_omega,N_iteration=N_iteration,noise_coeff=noise_coeff,tmax=tmax,w_0=w_0)

        OUT.append(s)
        T_list.append(T)




    # PRINTING
    if print_fig:
        for idx_al in arange(len(noise_coeff_list)):
            s = OUT[idx_al]
            T = T_list[idx_al]
            plot_fancy.plot_s(T,s,one_fig=True,tmin=tmax- 4*second)
            title([noise_coeff_list[idx_al],cos_coeff_list[idx_al]])
        plt.show()


        plt.figure()
        corr = check_validity.check_correlation(T_list,OUT)
        plot_fancy.plot_correlation_matrix(corr.T,np.log10(cos_coeff_list),plot_fancy.get_win())
        show()


    # SAVING
    if save:
        params = { 't': time.clock(), 'noise_coeff_list': noise_coeff_list, 'cos_coeff_list': cos_coeff_list, 'tmax': tmax ,'tau_omega': tau_omega, 'N_iteration': N_iteration}
        result_saver.save_result(file_path,params,OUT,T_list)