from brian2 import *
from runner_brian import run_SFA_batch_lambda
from runner_brian import result_saver
from runner_brian import plot_fancy
from runner_brian import check_validity

import bin.tools.spectralAnalysis as to
import matplotlib.pyplot as plt
import pickle
import time
#import cPickle as pickle

file_path = 'results_audio/batch/'
save = True
print_fig = True
loop_it = 1

power_start = -1
number=8
tau_omega_list = np.power(10.,np.array(range(-number+1,number))/(number-1.) ) * ms
K = len(tau_omega_list)


print tau_omega_list
tmax = 5 * second
N_delays = 512
#tau_delay_list = np.ones(K) * .1 * ms
tau_delay= 9/ (11025 * Hz)

N_iteration = 10**6
al_opt = 0.9

epsi = 10**-6
X_in = []
R_proj = []

print [tmax,N_delays,tau_delay,N_iteration,al_opt,epsi]

for i in range(0,loop_it):

    w_0 = np.ones((N_delays,1))
    w_0 /= norm(w_0)

    OUT = []
    WEIGHTS = []
    T_list = []
    figure()
    for idx_al in arange(K):
        tau_omega = tau_omega_list[idx_al]
        #tau_delay = tau_delay_list[idx_al]
        T,X_in,R_proj,output,weights,cost_seq = run_SFA_batch_lambda.run_SFA_batch_audio(epsi=epsi,N_iteration=N_iteration,al_opt=al_opt,tau_delay=tau_delay,tau_omega=tau_omega,N_delays=N_delays,tmax=tmax,w_0=w_0,X_in=X_in,R_proj=R_proj)

        print np.shape(X_in)

        weights = np.dot(weights,R_proj)
        subplot(K/3+1,3,idx_al+1)
        plt.plot(cost_seq)
        title(tau_omega)

        OUT.append(output)
        WEIGHTS.append(weights)
        #T_list.append(T)


        # SAVING
        if save:
            params = { 't': time.clock(),
                       'tau_omega': tau_omega_list,
                       'N_delays': N_delays,
                       'tau_delay': tau_delay,
                       'tmax': tmax ,
                       'N_iteration': N_iteration}
            result_saver.save_result(file_path,params,[output,weights],[T])

    show()




    # PRINTING
    if print_fig:
        for idx_al in arange(K):
            tau_omega = tau_omega_list[idx_al]

            plt.figure()
            weights = WEIGHTS[idx_al]
            plt.plot(range(0,N_delays)*tau_delay,weights,color='green',linewidth=2.0)

            ylabel('Weights')
            xlabel('Delay (s)')
            title(tau_omega_list[idx_al])

        plt.show()

        padded_weights = np.hstack((weights,np.zeros(8 * N_delays)))
        frq,spc = to.getSpectrum(padded_weights,1/tau_delay)
        A = np.zeros((K,len(spc)))
        for idx_al in arange(K):
            weights = WEIGHTS[idx_al]
            #tau_delay = tau_delay_list[idx_al]

            normed_weights = weights/std(weights)
            padded_weights = np.hstack((normed_weights,np.zeros(8 * N_delays)))
            frq,spc = to.getSpectrum(padded_weights,1/tau_delay)

            A[idx_al,:] = spc

        im = plt.pcolor(A)
        plt.xticks(frq)
        plt.axvline(49, color='red')
        plt.axvline(82.4, color='red')
        plt.axvline(98, color='red')
        plt.xscale('log')
        xlim([10,1000])
        plt.show()


        plt.figure()
        frq,spc = to.getSpectrum(output,11025)


        B = np.zeros((K,len(spc)))
        for idx_al in arange(K):
            output = OUT[idx_al]
            #tau_delay = tau_delay_list[idx_al]

            _,spc = to.getSpectrum(output,11025)

            B[idx_al,:] = spc.ravel()



        im = plt.pcolor(A)
        plt.xticks(frq)
        plt.axvline(49, color='red')
        plt.axvline(82.4, color='red')
        plt.axvline(98, color='red')
        plt.xscale('log')
        plt.show()

