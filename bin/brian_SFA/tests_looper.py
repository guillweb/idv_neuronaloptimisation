__author__ = 'guillaume'

from brian2 import *
from runner_brian import run_SFA_continuous_lambda
from runner_brian import plot_fancy
import bin.tools.spectralAnalysis as to

from runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

T_audio = 1 * second
tmax = 7 * T_audio
Ndelays = 32
FS = 11025 * Hz
tau_delay = 9/FS

print 'FS: {0}'.format(FS)

T,x = run_SFA_continuous_lambda.looper(
    N_delays=Ndelays,
    tmax=tmax,
    tau_delay=tau_delay,
    T_audio=T_audio,
    FS=FS)

print np.shape(T)
print np.shape(x)

figure()
plt.plot(T,x[0,:],color="red")
show()
