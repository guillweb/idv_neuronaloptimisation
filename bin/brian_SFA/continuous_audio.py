__author__ = 'guillaume'

from brian2 import *
from runner_brian import run_SFA_continuous_lambda
from runner_brian import plot_fancy
import bin.tools.spectralAnalysis as to

from runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

T_audio = 6 * second
tmax = 10 * T_audio
tau_omega = 2 * ms
tau_w = 6. * second
Ndelays = 32
tau_delay = 1/(92.5 * Hz * 16)
FS = 11025 * Hz

print 'FS: {0}'.format(FS)

T,x_alpha,s,w,w_end,post_idx = run_SFA_continuous_lambda.run_SFA_continuous_omega(
    N_delays=Ndelays,
    tau_omega=tau_omega,
    tmax=tmax,
    tau_w=tau_w,
    input_type=1,
    Noutputs=1,
    tau_delay=tau_delay,
    T_audio=T_audio,
    FS=FS)

figure()
plt.plot(T,x_alpha[0,:],color="red")
plt.plot(T,s[0,:],color="blue")

figure()
gs = gridspec.GridSpec(2,1)

frq_wei,spc_w = to.getSpectrum(w_end,1/tau_delay,pad_to=4*Ndelays)
spc_w /= np.linalg.norm(spc_w)
spc_w_dB = 10 * np.log(spc_w)
delays = range(Ndelays) * tau_delay

ax1 = plt.subplot(gs[0,0])
ax1.plot(delays,w_end,color='black',linewidth=2.0)
ax1.set_title('Learnt filter')
ax1.set_ylabel(r'$w$')
ax1.set_xlabel('delay (s)')


frq_min = 10 * Hz
frq_max = 500 * Hz
ax2 = plt.subplot(gs[1,0])
plot_fancy.set_font()
ax2.plot(frq_wei,spc_w_dB,color='black',linewidth=2.0)
ax2.set_title('Frequency response')
ax2.set_ylabel('Gain (dB)')
ax2.set_xlabel('f (Hz)')
ax2.set_xscale('log')
ax2.set_ylim([-60,0])
ax2.set_xlim([frq_min,frq_max])

plt.figure()
plot_fancy.plot_online(T,w[0:5,:],s,post_idx[0:5])
plt.show()