from brian2 import *
from runner_brian import run_SFA_batch_lambda
from runner_brian import result_saver
from runner_brian import plot_fancy
from runner_brian import check_validity

import bin.tools.spectralAnalysis as to
import matplotlib.pyplot as plt
import pickle
import time
#import cPickle as pickle

file_path = 'results_audio/batch/'
save = True
print_fig = True
loop_it = 1

power_start = -1
number=2
tau_omega_list = [0 * ms]
K = len(tau_omega_list)


print tau_omega_list
tmax = 5 * second
N_delays = 512
#tau_delay_list = np.ones(K) * .1 * ms
tau_delay= 1/ (11025 * Hz)

N_iteration = 10**4
al_opt = 0.9

X_in = []
R_proj = []
for i in range(0,loop_it):

    w_0 = np.random.randn(N_delays,1)
    w_0 /= norm(w_0)

    OUT = []
    WEIGHTS = []
    T_list = []
    figure()
    for idx_al in arange(K):
        tau_omega = tau_omega_list[idx_al]
        #tau_delay = tau_delay_list[idx_al]
        T,X_in,R_proj,output,weights,cost_seq = run_SFA_batch_lambda.run_SFA_batch_audio(N_iteration=N_iteration,al_opt=al_opt,tau_delay=tau_delay,tau_omega=tau_omega,N_delays=N_delays,tmax=tmax,w_0=w_0,X_in=X_in,R_proj=R_proj)

        weights = np.dot(weights,R_proj)
        subplot(K/3+1,3,idx_al+1)
        plt.plot(cost_seq)
        title(tau_omega)

        #OUT.append(output)
        #WEIGHTS.append(weights)
        #T_list.append(T)


        # SAVING
        if save:
            params = { 't': time.clock(),
                       'tau_omega': tau_omega_list,
                       'N_delays': N_delays,
                       'tau_delay': tau_delay,
                       'tmax': tmax ,
                       'N_iteration': N_iteration}
            result_saver.save_result(file_path,params,[output,weights],[T])

    show()




    # PRINTING
    if print_fig:
        for idx_al in arange(K):
            tau_omega = tau_omega_list[idx_al]

            plt.figure()
            weights = WEIGHTS[idx_al]
            plt.plot(range(0,N_delays)*tau_delay,weights,color='green',linewidth=2.0)

            ylabel('Weights')
            xlabel('Delay (s)')
            title(tau_omega_list[idx_al])

        plt.show()

        padded_weights = np.hstack((weights,np.zeros(100 * N_delays)))
        frq,spc = to.getSpectrum(padded_weights,1/tau_delay)
        A = np.zeros((K,len(spc)))
        for idx_al in arange(K):
            weights = WEIGHTS[idx_al]
            #tau_delay = tau_delay_list[idx_al]

            padded_weights = np.hstack((weights,np.zeros(100 * N_delays)))
            frq,spc = to.getSpectrum(padded_weights,1/tau_delay)

            A[idx_al,:] = spc

        plt.figure()
        im = plt.pcolor(A)
        plt.xticks(frq)
        plt.axvline(49, color='red')
        plt.axvline(82.4, color='red')
        plt.axvline(98, color='red')
        plt.xscale('log')
        plt.show()

