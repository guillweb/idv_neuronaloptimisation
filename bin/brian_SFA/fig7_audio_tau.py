from brian2 import *
from runner_brian import run_SFA_batch_lambda
from runner_brian import result_saver
from runner_brian import plot_fancy
from runner_brian import check_validity
import matplotlib.gridspec as gridspec

import bin.tools.spectralAnalysis as to
import matplotlib.pyplot as plt
import time
from matplotlib.colors import LogNorm
#import cPickle as pickle

save = True
print_fig = True

BPM1 = 181/(60 * second)
BPM2 = 2* BPM1



pitchE2 = 82.4 *Hz
pitchG2 = 98 *Hz


pitchF1 = 46.75 *Hz
pitchF2 = 92.5 *Hz

pitchC2 = 138.6/2 * Hz
pitchA2 = 116.5 * Hz
pitchC3 = 138.6 * Hz
pitchF3 = 185 * Hz

dP1 = 20.6 * Hz
dP2 = (pitchF3 - pitchC3)

loop_it = 1

power_start = -1
octaves=3
number=3
tau_omega_list = np.power(10.,np.array(arange(number*octaves))/float(number) +power_start) * ms
K = len(tau_omega_list)
tau_omega_list = tau_omega_list[0:K-1]
K = len(tau_omega_list)


print tau_omega_list
tmax = 5 * second
N_delays = 512
#tau_delay_list = np.ones(K) * .1 * ms
tau_delay= 1./7* 1/pitchF2

N_iteration = 10**4
al_opt = 0.9

epsi = 10**-3
X_in = []
R_proj = []


db_path='results_fig7/'

print [tmax,N_delays,tau_delay,N_iteration,al_opt,epsi]

for i in range(0,loop_it):

    w_0 = np.ones((N_delays,1))
    w_0 /= norm(w_0)

    OUT = []
    WEIGHTS = []
    T_list = []
    for idx_al in arange(K):
        tau_omega = tau_omega_list[idx_al]
        print tau_delay


        params = { 'tmax': tmax, 'N_delays': N_delays, 'epsi': epsi, 'tau_omega':tau_omega,'tau_delay':tau_delay, 'N_iteration': N_iteration, 'al_opt': al_opt}
        count,file_list = result_saver.find_results_in_db(db_path,params)

        if count == 0:


            # if len(X_in) == 0:
            #     params_input = { 'tmax': tmax, 'N_delays': N_delays, 'epsi': epsi, 'tau_delay': tau_delay}
            #     count_input,file_list_input = result_saver.find_results_in_db(db_path,params_input)
            #     if count_input > 0:
            #         file_name_input = db_path + file_list_input[0]
            #         [T,X_in,R_proj],[output,weights] = result_saver.get_data(file_name_input)
            #         print 'INPUT LOADED'

            T,X_in,R_proj,output,weights,cost_seq = run_SFA_batch_lambda.run_SFA_batch_audio(epsi=epsi,N_iteration=N_iteration,al_opt=al_opt,tau_delay=tau_delay,tau_omega=tau_omega,N_delays=N_delays,tmax=tmax,w_0=w_0,X_in=X_in,R_proj=R_proj)

            print np.shape(X_in)

            weights = np.dot(weights,R_proj).ravel()
            subplot(K/3+1,3,idx_al+1)
            plt.plot(cost_seq)
            title(tau_omega)

            #T_list.append(T)


        # SAVING

            if save:
                result_saver.save_result(db_path,params,[output,weights],[T,R_proj])
        else:
            file_name = db_path + 'objects/' + file_list[0]
            [T,R_proj],[output,weights],param_check = result_saver.get_data(file_name)
            print 'RESULT LOADED'

        OUT.append(output)
        WEIGHTS.append(weights)
        T_list.append(T)





    # PRINTING
    if print_fig:

        plt.figure(figsize=(20,10))
        gs = gridspec.GridSpec(3,2)
        gs.update(hspace=0.25)

        plot_fancy.set_font()
        for i in arange(3):
            idx_al = [K-2,(K-1)/2,0][i]
            tau_omega = tau_omega_list[idx_al]

            ax = subplot(gs[i,0])
            weights = WEIGHTS[idx_al]
            ax.plot(range(0,N_delays)*tau_delay,weights,color='black',linewidth=2.0)

            ax.set_ylabel('Weights')
            ax.set_xlabel('Delay (s)')
            if i == 0:
                ax.set_title(r'Filter learnt with $\tau_{STDP} = %2.0f (ms)$' % (tau_omega_list[idx_al]/ ms))
            else:
                ax.set_title(r'$\tau_{STDP} = %2.1f (ms)$' % (tau_omega_list[idx_al]/ ms))

            xlim([0,(N_delays-1)*tau_delay])
            locator_params(axis = 'x', nbins = 4)
            locator_params(axis = 'y', nbins = 3)

            if i < 2:
                ax.set_xlabel('')
                plt.setp(ax.get_xticklabels(), visible=False)



        ax = subplot(gs[:,1])
        frq,spc = to.getSpectrum(weights,1/tau_delay,pad_to=2048)
        A = np.zeros((K,len(spc)))
        for idx_al in arange(K):
            weights = WEIGHTS[idx_al]
            #tau_delay = tau_delay_list[idx_al]

            normed_weights = weights #/std(weights)
            frq,spc = to.getSpectrum(weights,1/tau_delay,pad_to=2048)

            A[idx_al,:] = 10*np.log10(spc/norm(spc)*norm(weights))

        im = plt.pcolormesh(frq,tau_omega_list,A,vmin=-20,vmax=np.max(A))
        im.set_rasterized(True)
        title('Frequency response')
        plt.xticks(frq/Hz)
        xscale('log')
        yscale('log')

        ax.set_xlabel('f (Hz)')
        ax.set_ylabel(r'$\tau_{STDP}$')

        plt.axvline(BPM1/Hz,linestyle='--',linewidth=4.0, color='black')
        #plt.axvline(BPM2/Hz,linestyle='--',linewidth=4.0, color='black')

        plt.axvline(pitchE2/Hz,linestyle='--',linewidth=4.0, color='green')
        plt.axvline(pitchG2/Hz,linestyle='--',linewidth=4.0, color='green')

        plt.axvline(dP1/Hz,linestyle='--',linewidth=4.0, color='blue')

        #ax.set_yticks(np.arange(A.shape[0]) + 0.5, minor=False)
        #ax.set_yticklabels(['0.1','','','','','1.','','','','','10.'], minor=False)
        #ax.set_ylabel(r'$\tau_{STDP}$ (ms)')


        cbar = colorbar(ticks=[-20,-10,np.max(A)])
        cbar.ax.set_yticklabels(['< -20 dB', '-10 dB', '%d dB' % np.max(A)])# vertically oriented colorbar


        plt.xscale('log')
        xlim([1,200])
        ylim([min(tau_omega_list),max(tau_omega_list)])

        savefig('../../figures/fig7_audio_tau.pdf')
        plt.show()


