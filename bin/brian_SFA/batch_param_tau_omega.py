from brian2 import *
from runner_brian import run_SFA_batch_lambda
from runner_brian import plot_fancy
from runner_brian import check_validity
from runner_brian import result_saver

import matplotlib.pyplot as plt
import pickle
import time
#import cPickle as pickle

file_path = 'results_tau/batch/'
save = True
print_fig = False
loop_it = 20

tau_omega_list = [0,0.1,1.,10.,100.]*ms
tmax = 4 * second
N_iteration = 10**5

for i in range(0,loop_it):


    w_0 =  np.random.randn(5,1)
    w_0 /= norm(w_0)


    OUT = []
    T_list = []
    for idx_tau in arange(len(tau_omega_list)):
        tau_omega = tau_omega_list[idx_tau]
        T,x,s = run_SFA_batch_lambda.run_SFA_batch_lambda(N_iteration=N_iteration,tau_omega=tau_omega,tmax=tmax,w_0=w_0)

        OUT.append(s)
        T_list.append(T)

    if print_fig:
        for idx_tau in arange(len(tau_omega_list)):
            s = OUT[idx_tau]
            T = T_list[idx_tau]
            plot_fancy.plot_s(T,s,one_fig=True,tmin=tmax- 4*second)
            title(tau_omega_list[idx_tau])
        plt.show()



        plt.figure()
        corr = check_validity.check_correlation(T_list,OUT)
        plot_fancy.plot_correlation_matrix(corr.T,tau_omega_list,plot_fancy.get_win())
        show()

    if save:
        params = { 't': time.clock(), 'tau_omega_list': tau_omega_list, 'tmax': tmax ,'N_iteration': N_iteration }
        result_saver.save_result(file_path,params,OUT,T_list)
