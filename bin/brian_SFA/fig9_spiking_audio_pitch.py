__author__ = 'guillaume'

from brian2 import *
from runner_brian import run_SFA_spiking_lambda
from runner_brian import plot_fancy
import bin.tools.spectralAnalysis as to

from runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import bin.tools.timeCheck as ti
import cPickle as pickle
import time
from bin.brian_SFA.runner_brian import result_saver


T_audio = 5 * second
tmax = 400 * T_audio
tau_omega = 2 * ms
tau_w = 6. * second
Ndelays = 64
tau_delay = 1/(92.5 * Hz * 16)
FS = 11025 * Hz

nu_av = 100 * Hz
nu0 = 4./5. * nu_av

Noutputs = 1
tau_av = 1. * second

do_record=True
w_0 = np.random.randn(Ndelays)

save = True
db_path='results_fig9/'
params = {'T_audio': T_audio, 'tmax': tmax, 'FS': FS,'tau_w': tau_w, 'tau_omega': tau_omega, 'N_delays': Ndelays, 'tau_delay':tau_delay, 'nu_av': nu_av, 'nu0': nu0, 'tau_av': tau_av}
count,file_list = result_saver.find_results_in_db(db_path,params)


if count == 0:
    T_start,spikes_t,spikes_i,_,_,_,s_alpha_start,_,_,_ = run_SFA_spiking_lambda.run_SFA_spiking_omega(
        nu_av=nu_av,
        nu0=nu0,
        do_record=do_record,
        tau_omega=tau_omega,
        tmax=4 * second,
        tau_w0= 100*tau_w,
        input_type=1,
        tau_average= tau_av,
        Noutputs=Noutputs,
        tau_delay=tau_delay,
        N_delays=Ndelays,
        T_audio=T_audio,
        FS=FS,
        w_0 = w_0)

    T,_,_,_,_,_,_,w,w_end,_ = run_SFA_spiking_lambda.run_SFA_spiking_omega(
        nu_av=nu_av,
        nu0=nu0,
        do_record=do_record,
        tau_omega=tau_omega,
        tmax=tmax,
        tau_w0=tau_w,
        input_type=1,
        tau_average= tau_av,
        Noutputs=Noutputs,
        tau_delay=tau_delay,
        N_delays=Ndelays,
        T_audio=T_audio,
        FS=FS,
        w_0 = w_0)


    T_end,_,_,_,_,_,s_alpha_end,_,_,_ = run_SFA_spiking_lambda.run_SFA_spiking_omega(
        nu_av=nu_av,
        nu0=nu0,
        do_record=do_record,
        tau_omega=tau_omega,
        tmax=4 * second,
        tau_w0= 100*tau_w,
        input_type=1,
        tau_average= tau_av,
        Noutputs=Noutputs,
        tau_delay=tau_delay,
        N_delays=Ndelays,
        T_audio=T_audio,
        FS=FS,
        w_0 = w_end)

    if save:
        result_saver.save_result(db_path,params,[w,w_end,s_alpha_start,s_alpha_end,spikes_i,spikes_t],[T_start,T,T_end])

else:

    file_name = db_path + 'objects/' + file_list[0]
    [T_start,T,T_end],[w,w_end,s_alpha_start,s_alpha_end,spikes_i,spikes_t],params_loaded = result_saver.get_data(file_name)


## Printing
plt.figure()
gs = gridspec.GridSpec(3,2)
gs.update(wspace=0.3,hspace=0.5)

plot_fancy.set_font()

raster = plt.subplot(gs[0,0])
weights = plt.subplot(gs[1,:])
filter = plt.subplot(gs[2,:])


delays = range(Ndelays) * tau_delay
#inds = np.random.random_integers(0,Ndelays-1,10)
inds = range(0,Ndelays,3)
for k in inds:
    weights.plot(T,w[k,:].T,color='gray',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)
weights.plot(T,np.sum(w[inds,:],axis=0),color='red',label=r'delay: {0:2.2} ms'.format(delays[k]),linewidth=2.0)
#weights.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
#           ncol=5, mode="expand", borderaxespad=0.)
#weights.pcolor(T,delays,w,cmap='RdBu')


# Titles
ind_t = spikes_t < 1
ind_i = spikes_i[ind_t] < 20
raster.plot(spikes_t[ind_t][ind_i], spikes_i[ind_t][ind_i], '.', mew=0)
raster.set_yticks([])
raster.set_xticks([0,1])
raster.set_xlim([0,1])
raster.set_ylim([0,20])
raster.yaxis.tick_left()

weights.locator_params(axis = 'x', nbins = 4)
weights.locator_params(axis = 'y', nbins = 3)
weights.set_ylim([-.5,.5])
weights.set_xlabel('t (s)')
weights.set_ylabel(r'$w_{i}$')
#weights.set_title('Weights convergence')




filter.plot(delays,w_end,color='black',linewidth=2.0)
#filter.set_title('Learnt filter')
filter.set_ylabel(r'$w(t_{end})$')
filter.set_xlabel('delay (s)')

filter.set_xlim([0,0.04])
filter.set_ylim([-0.2,0.2])
filter.locator_params(axis = 'x', nbins = 4)
filter.locator_params(axis = 'y', nbins = 3)

savefig('../../figures/fig9b_spiking_audio.pdf')
show()