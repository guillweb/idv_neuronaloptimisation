__author__ = 'guillaume'

from brian2 import *
from runner_brian import run_SFA_spiking_lambda
from runner_brian import plot_fancy
import bin.tools.spectralAnalysis as to

from runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time
tempo = 1 * Hz
pitchG2 = 98 *Hz
pitchF2 = 92.5 *Hz

T_audio = 12 * second
tmax = 1 * T_audio
tau_omega = 2 * ms
tau_w = .5 * second
Ndelays = 32
tau_delay = 9/(11025 * Hz)
FS = 11025

tau_av = 1 * second
nu_av = 100 * Hz
nu0 = 80 * Hz

FS = 11025 * Hz
tau_delay = 97/FS
dtRecord = tmax/1000.
do_record=True
w_0 = np.random.randn(Ndelays)

print 'FS: {0}'.format(FS)

T,spikes_t,spikes_i,x,x_alpha,s,s_alpha,w,w_end,post_idx = run_SFA_spiking_lambda.run_SFA_spiking_omega(
    nu_av=nu_av,
    nu0=nu0,
    do_record=do_record,
    tau_omega=tau_omega,
    tmax=tmax,
    tau_w0=tau_w,
    input_type=1,
    tau_average= tau_av,
    Noutputs=1,
    tau_delay=tau_delay,
    N_delays=Ndelays,
    T_audio=T_audio,
    FS=FS,
    dtRecord=dtRecord,
    w_0 = w_0)

t_start = 2 * tau_av
ind_valid = T > t_start
print "Mean x alpha: {0},{1}".format(np.var(x_alpha[:,ind_valid],axis=1),np.var(x_alpha,axis=1))
print "Variance x alpha: {0},{1}".format(np.std(x_alpha[:,ind_valid],axis=1),np.std(x_alpha,axis=1))


if do_record:
    figure()
    plot(spikes_t, spikes_i, '.', mew=0)
    title(r'Inputs')
    yticks(arange(5))
    #yticklabels([r'$x_1$',r'$x_2$',r'$x_3$',r'$x_4$',r'$x_5$'])
    ylim([-0.5,20.5])
    xticks([0 ,1.0])
    xlim([0*second,1*second])
    yticks([])
    #xticklabels(['0','1 s'])

figure()
gs = gridspec.GridSpec(2,1)

frq_wei,spc_w = to.getSpectrum(w_end,1/tau_delay,pad_to=4*Ndelays)
spc_w /= np.linalg.norm(spc_w)
spc_w_dB = 10 * np.log(spc_w)
delays = range(Ndelays) * tau_delay

ax1 = plt.subplot(gs[0,0])
ax1.plot(delays,w_end,color='black',linewidth=2.0)
ax1.set_title('Learnt filter')
ax1.set_ylabel(r'$w$')
ax1.set_xlabel('delay (s)')


frq_min = 10 * Hz
frq_max = 500 * Hz
ax2 = plt.subplot(gs[1,0])
plot_fancy.set_font()
ax2.plot(frq_wei,spc_w_dB,color='black',linewidth=2.0)
ax2.set_title('Frequency response')
ax2.set_ylabel('Gain (dB)')
ax2.set_xlabel('f (Hz)')
ax2.set_xscale('log')
#ax2.set_ylim([-60,0])
#ax2.set_xlim([frq_min,frq_max])


if do_record:
    plt.figure()
    plot_fancy.plot_online(T,w[0:5,:],s_alpha,post_idx[0:5])

plt.show()