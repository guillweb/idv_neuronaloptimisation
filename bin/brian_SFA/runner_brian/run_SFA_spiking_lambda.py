from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time


def run_SFA_spiking_lambda(X_in=[],
                           Vthr = 10 * mV,
                           tau_average=5 * second,
                           nu0=80*Hz,
                           nu_av=100*Hz,

                           T_audio=20 * second,
                           input_file_name="../../data/countryguit_11025.wav",
                           N_delays=128,
                           tau_delay = 9  / (11025 * Hz),
                           input_type=0,

                           fWiskott=1*Hz,
                           tmin=0*second,
                           tmax=200 * second,
                           FS=10000 * Hz,
                           tau_omega=10 * ms,
                           tau_derivation=0.01 * second,
                           tau_w0= 5. * ms,
                           Noutputs=4,
                           frequency_factor=11,
                           alpha_wiskott=1,w_0=[],
                           dtRecord=-1 * second):
    set_device('cpp_standalone')
    device.reinit()

    tau_w = tau_w0 / (nu_av * tau_omega)**2

    #-------------
    # Parameters
    #-------------


    N = int((tmax - tmin) * FS)
    print '---SPIKING----- fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

    # -------
    # Solver
    # -------

    if dtRecord < 0:
        dtRecord = (tmax-tmin)/1000

    tCount = time.clock()

    # Define input

    ## WISKOTT INPUT
    if input_type == 0:

        # Define input
        inp = il.Input(tmin,tmax,N)
        inp.set_wiskott_classic(fWiskott,frequency_factor,alpha_wiskott)
        inp.sphereInputs()
        gain = 1

        eq = '''                        x_th = int(i==0) * ta0(t) \
                                        + int(i==1) * ta1(t) \
                                        + int(i==2) * ta2(t) \
                                        + int(i==3) * ta3(t) \
                                        + int(i==4) * ta4(t) : 1
                                        '''


        Ninputs = inp.n
        tCount = ti.timeCount(tCount,'Input setup (s):')
    ## AUDIO INPUT
    else:
        #Define input
        inp = il.Input(tmin,T_audio+tmin,N)
        inp.addWaveFromFile(input_file_name,0*second)
        inp.n= 1
        inp.X = np.zeros((1,N))
        inp.X[0,:] = inp.x
        inp.whitenInputs()

        Ninputs = N_delays

        eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''

        tCount = ti.timeCount(tCount,'Input setup (s):')

    # ------------- Brian
    Texp = tmax - tmin
    Dt = inp.dt
    defaultclock.dt = Dt

    # -----------------------
    X_in = inp.X
    tCount = ti.timeCount(tCount,'High pass input setup (s):')
    ta0 = TimedArray(X_in[0,:],Dt)

    ## WISKOTT INPUT END
    if input_type==0:
        ta1 = TimedArray(X_in[1,:],Dt)
        ta2 = TimedArray(X_in[2,:],Dt)
        ta3 = TimedArray(X_in[3,:],Dt)
        ta4 = TimedArray(X_in[4,:],Dt)



    G_pre = NeuronGroup(Ninputs, eq + '''
                                        x_train : 1
                                        d x_av /dt = (x_train - x_av) / tau_average : 1
                                        x = (x_train - x_av : 1

                                        d x_exp /dt = (x - x_exp ) / tau_omega : 1
                                        d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1


                                        x_al_old : 1
                                        Dx : 1
                                        D2x : 1

                                        rt = nu0 * x_th + nu_av : Hz
                                        v : volt ''',
                        threshold='v>Vthr*rand()',
                        method="euler",
                        reset='v=0 * volt')




    cs = G_pre.custom_operation('''
                    v = rt * dt * Vthr
                    dx = tau_derivation / dt * (x_alpha - x_al_old)

                    D2x = tau_derivation / dt * (dx - Dx)
                    Dx = dx
                    x_al_old = x_alpha

                    x_train = 0 ''',when='train_reset')

    G_pre.x_av = nu_av * Dt

    G_post = NeuronGroup(Noutputs,'''
                s : 1
                ind = i : 1

                d s_exp / dt = (s - s_exp) / tau_omega : 1
                d s_alpha / dt = (s_exp - s_alpha) / tau_omega : 1

                norm_w : 1''',
                method="euler")


    G_post.norm_w = '1'

    S = Synapses(G_pre,G_post,
                 model='''
                 STDP = int(j==0) * D2x_pre * s_alpha_post \
                 + int(j==1) * Dx_pre * s_alpha_post \
                 + int(j==2) * x_alpha_pre*s_alpha_post \
                 - int(j==3) * x_alpha_pre*s_alpha_post: 1


                 dw / dt = int(t > 2*tau_average) *  STDP / tau_w : 1


                 s_post = w * x_pre : 1 (summed)
                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre="x_train_pre += 1",
                 connect=True)



    re_norm = S.custom_operation('''w /= sqrt(norm_w_post)''',when='weight_renorm')


    if len(w_0) == 0:
        w_0 = np.random.randn(Ninputs)
        w_0 /= norm(w_0)

    for Ni in range(0,Ninputs):
        S.w['i == %d' % Ni] = w_0[Ni]


    MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets','weight_renorm','end']



    mon_pre = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av','x_th','Dx','D2x'], record=arange(0,Ninputs),dt=dtRecord)
    mon_post = StateMonitor(G_post, ['s','s_alpha','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
    mon_syn = StateMonitor(S, ['w'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

    spikes_pre = SpikeMonitor(G_pre)

    tCount = ti.timeCount(tCount,'Brian setup (s):')
    run(Texp, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)
    tCount = ti.timeCount(tCount,'Brian run (s):')

    Trec = range(0,len(mon_post.s[0,:])) * dtRecord

    return Trec,np.array(spikes_pre.t),np.array(spikes_pre.i),mon_pre.x,mon_pre.x_alpha,mon_post.s,mon_post.s_alpha,mon_syn.w,np.array(S.w),S._postsynaptic_idx



def run_SFA_spiking_omega(X_in=[],
                           Vthr = 10 * mV,
                           tau_average=5 * second,
                           nu0=80*Hz,
                           nu_av=100*Hz,

                           T_audio=20 * second,
                           input_file_name="../../data/countryguit_11025.wav",
                           N_delays=128,
                           tau_delay = 9  / (11025 * Hz),
                           input_type=0,

                           fWiskott=1*Hz,
                           tmin=0*second,
                           tmax=200 * second,
                           FS=10000 * Hz,
                           tau_omega=10 * ms,
                           tau_w0= 5. * ms,
                           Noutputs=4,
                           frequency_factor=11,
                           alpha_wiskott=1,w_0=[],
                           dtRecord=-1 * second,
                           do_record=True):
    set_device('cpp_standalone')
    device.reinit()

    tau_w = tau_w0 #* (nu_av * tau_omega)**2

    #-------------
    # Parameters
    #-------------


    print '---SPIKING Omega----- fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

    # -------
    # Solver
    # -------

    if do_record and dtRecord < 0:
        dtRecord = (tmax-tmin)/1000

    tCount = time.clock()

    # Define input

    ## WISKOTT INPUT
    if input_type == 0:
        N = int((tmax - tmin) * FS)

        # Define input
        inp = il.Input(tmin,tmax,N)
        inp.set_wiskott_classic(fWiskott,frequency_factor,alpha_wiskott)
        inp.sphereInputs()
        gain = 1

        eq = '''                        x_th = int(i==0) * ta0(t) \
                                        + int(i==1) * ta1(t) \
                                        + int(i==2) * ta2(t) \
                                        + int(i==3) * ta3(t) \
                                        + int(i==4) * ta4(t) : 1
                                        '''


        Ninputs = inp.n
        tCount = ti.timeCount(tCount,'Input setup (s):')
    ## AUDIO INPUT
    else:
        N = int(T_audio * FS)
        #Define input
        inp = il.Input(tmin,T_audio+tmin,N)
        inp.addWaveFromFile(input_file_name,0*second)
        inp.n= 1
        inp.X = np.zeros((1,N))
        inp.X[0,:] = inp.x
        inp.whitenInputs()

        print "Input std: {0}".format(np.std(inp.x))

        Ninputs = N_delays

        eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''

        tCount = ti.timeCount(tCount,'Input setup (s):')

    # ------------- Brian
    Texp = tmax - tmin
    Dt = inp.dt
    defaultclock.dt = Dt

    # -----------------------
    X_in = inp.X
    tCount = ti.timeCount(tCount,'High pass input setup (s):')
    ta0 = TimedArray(X_in[0,:],Dt)

    ## WISKOTT INPUT END
    if input_type==0:
        ta1 = TimedArray(X_in[1,:],Dt)
        ta2 = TimedArray(X_in[2,:],Dt)
        ta3 = TimedArray(X_in[3,:],Dt)
        ta4 = TimedArray(X_in[4,:],Dt)

    std_x = sqrt(nu_av * Dt)
    G_pre = NeuronGroup(Ninputs, eq + '''
                                        x_train : 1
                                        d x_av /dt = (x_train - x_av) / tau_average : 1
                                        norm : 1

                                        x = (x_train - x_av) / norm  : 1

                                        d x_exp /dt = (x - x_exp ) / tau_omega : 1
                                        d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1

                                        rt = nu0 * x_th + nu_av : Hz''',
                        threshold='rt * dt>rand()',
                        method="euler",
                        reset='v=0 * volt')
    cs = G_pre.run_regularly('''x_train = 0 ''',when='train_reset')
    G_pre.x_av = nu_av * Dt
    G_pre.norm = std_x

    G_post = NeuronGroup(Noutputs,'''
                s : 1

                var_x : 1
                d norm_av /dt = (sqrt(var_x/Ninputs) - norm_av) / tau_average : 1

                d s_exp / dt = (s - s_exp) / tau_omega : 1
                d s_alpha / dt = (s_exp - s_alpha) / tau_omega : 1

                norm_w : 1'''
                ,method="euler"
                )
    G_post.norm_w = '1'
    G_post.norm_av = std_x
    G_post.var_x = std_x**2


    S = Synapses(G_pre,G_post,
                 model='''
                 STDP = (x_exp_pre - x_alpha_pre) * s_post + (s_exp_post - s_alpha_post) * x_pre: 1
                 dw / dt = STDP / tau_w : 1

                 norm_pre = norm_av_post : 1

                 var_x_post = x_pre : 1 (summed)
                 s_post = w * x_pre : 1 (summed)
                 norm_w_post = w*w : 1 (summed)''',
                 method="euler",
                 pre="x_train_pre += 1",
                 connect=True)

    re_norm = S.run_regularly('''w /= sqrt(norm_w_post)''',when='weight_renorm')


    if len(w_0) == 0:
        w_0 = np.random.randn(Ninputs)
        w_0 /= norm(w_0)

    for Ni in range(0,Ninputs):
        S.w['i == %d' % Ni] = w_0[Ni]

    MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets',
                                'weight_renorm',
                                'end']

    if do_record:
        mon_pre = StateMonitor(G_pre, ['x','x_exp','x_alpha','x_av','x_th'], record=arange(0,Ninputs),dt=dtRecord)
        mon_post = StateMonitor(G_post, ['s','s_alpha','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
        mon_syn = StateMonitor(S, ['w'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

        spikes_pre = SpikeMonitor(G_pre)

    tCount = ti.timeCount(tCount,'Brian setup (s):')
    run(Texp, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)
    tCount = ti.timeCount(tCount,'Brian run (s):')


    if do_record:
        Trec = range(0,len(mon_post.s[0,:])) * dtRecord
        return Trec,np.array(spikes_pre.t),np.array(spikes_pre.i),mon_pre.x_th,mon_pre.x_alpha,mon_post.s,mon_post.s_alpha,mon_syn.w,np.array(S.w),S._postsynaptic_idx
    else:
        return [],[],[],[],[],[],[],[],np.array(S.w),S._postsynaptic_idx

