from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time


def run_SFA_continuous_lambda(
        T_audio=20 * second,
        input_file_name="../../data/countryguit_10000.wav",
        N_delays=128,
        tau_delay = 9  / (11025 * Hz),
        input_type=0,
        fWiskott=1*Hz,
        tmin=0*second,
        tmax=30*second,
        FS=10**4 * Hz,
        tau_omega=10 * ms,
        tau_derivation=0.01 * second,
        tau_w= 3. * second,
        Noutputs=4,
        frequency_factor=11,
        alpha_wiskott=1,
        w_0=[],
        dtRecord=-1*second):
    set_device('cpp_standalone')
    device.reinit()

    #-------------
    # Parameters
    #-------------

    N = int((tmax - tmin) * FS)
    print '----CONTINUOUS---- fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

    # -------
    # Solver
    # -------

    if dtRecord < 0:
        dtRecord = (tmax-tmin)/1000

    tCount = time.clock()


    ## WISKOTT INPUT
    if input_type == 0:

        # Define input
        inp = il.Input(tmin,tmax,N)
        inp.set_wiskott_classic(fWiskott,frequency_factor,alpha_wiskott)
        inp.sphereInputs()

        eq = '''                        x_th = int(i==0) * ta0(t) \
                                        + int(i==1) * ta1(t) \
                                        + int(i==2) * ta2(t) \
                                        + int(i==3) * ta3(t) \
                                        + int(i==4) *ta4(t) : 1
                                        '''


        Ninputs = inp.n
        tCount = ti.timeCount(tCount,'Input setup (s):')
    ## AUDIO INPUT
    else:
        #Define input
        inp = il.Input(tmin,T_audio+tmin,N)
        inp.addWaveFromFile(input_file_name,0*second)
        inp.n= 1
        inp.X = np.zeros((1,N))
        inp.X[0,:] = inp.x
        inp.whitenInputs()

        Ninputs = N_delays

        eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''

        tCount = ti.timeCount(tCount,'Input setup (s):')

    # ------------- Brian
    Texp = tmax - tmin
    Dt = inp.dt
    defaultclock.dt = Dt

    # -----------------------
    X_in = inp.X
    tCount = ti.timeCount(tCount,'High pass input setup (s):')
    ta0 = TimedArray(X_in[0,:],Dt)

    ## WISKOTT INPUT END
    if input_type==0:
        ta1 = TimedArray(X_in[1,:],Dt)
        ta2 = TimedArray(X_in[2,:],Dt)
        ta3 = TimedArray(X_in[3,:],Dt)
        ta4 = TimedArray(X_in[4,:],Dt)

    G_pre = NeuronGroup(Ninputs, eq + '''

                                        d x_exp /dt = (x_th - x_exp ) / tau_omega : 1
                                        d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1


                                        x_old : 1
                                        Dx : 1
                                        D2x : 1

                                        dx :1
                                        dx_al : 1

                                        x_al_old : 1
                                        Dx_al : 1
                                        D2x_al : 1''')

    cs = G_pre.custom_operation('''
                    dx = tau_derivation / dt * (x_th - x_old)
                    D2x = tau_derivation / dt * (dx - Dx)
                    Dx = dx

                    dx_al = tau_derivation / dt * (x_alpha - x_al_old)
                    D2x_al = tau_derivation / dt * (dx_al - Dx_al)
                    Dx_al = dx_al

                    x_al_old = x_alpha
                    x_old = x_th ''',
                    when='train_reset')

    G_post = NeuronGroup(Noutputs,'''
                s : 1
                s_al : 1

                d s_al_exp / dt = (s_al - s_al_exp) / tau_omega : 1
                d s_al_alpha / dt = (s_al_exp - s_al_alpha) / tau_omega : 1

                norm_w_al : 1
                norm_w : 1''')

    G_post.norm_w = '1'
    G_post.norm_w_al = '1'

    S = Synapses(G_pre,G_post,
                 model='''
                 STDP = int(j==0) * D2x_pre * s_post \
                 + int(j==1) * Dx_pre * s_post \
                 + int(j==2) * x_th_pre * s_post \
                 - int(j==3) * x_th_pre * s_post: 1

                 STDP_al = int(j==0) * D2x_al_pre * s_al_alpha_post \
                 + int(j==1) * Dx_al_pre * s_al_alpha_post \
                 + int(j==2) * x_alpha_pre * s_al_alpha_post \
                 - int(j==3) * x_alpha_pre * s_al_alpha_post: 1

                 d w / dt = (t > tau_omega) * STDP / tau_w : 1
                 d w_al / dt = (t > tau_omega) * STDP_al / tau_w : 1

                 s_al_post = w_al * x_th_pre : 1 (summed)
                 norm_w_al_post = w_al * w_al : 1 (summed)

                 s_post = w * x_th_pre  : 1 (summed)
                 norm_w_post = w*w  : 1 (summed)''',
                 connect=True)

    re_norm = S.custom_operation('''
                w_al /= sqrt(norm_w_al_post)
                w /= sqrt(norm_w_post)
                ''',when='weight_renorm')


    if len(w_0) == 0:
        w_0 = np.random.randn(Ninputs)
        w_0 /= norm(w_0)

    for Ni in range(0,Ninputs):
        S.w['i == %d' % Ni] = w_0[Ni]
        S.w_al['i == %d' % Ni] = w_0[Ni]


    MagicNetwork.schedule = ['start','groups','train_reset','thresholds','synapses','resets','weight_renorm','end']



    mon_pre = StateMonitor(G_pre, ['x_exp','x_alpha','x_th','D2x','D2x_al'], record=arange(0,Ninputs),dt=dtRecord)
    mon_post = StateMonitor(G_post, ['s','s_al','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
    mon_syn = StateMonitor(S, ['w','w_al'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

    tCount = ti.timeCount(tCount,'Brian setup (s):')
    run(Texp, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)
    tCount = ti.timeCount(tCount,'Brian run (s):')

    Trec = range(0,len(mon_post.s[0,:])) * dtRecord

    return Trec,mon_pre.x_th,mon_pre.x_alpha,mon_post.s,mon_post.s_al,mon_syn.w,mon_syn.w_al,S._postsynaptic_idx


def run_SFA_continuous_omega(
        T_audio=20 * second,
        input_file_name="../../data/countryguit_short_11025.wav",
        N_delays=128,
        tau_delay = 9  / (11025 * Hz),
        input_type=0,
        fWiskott=1*Hz,
        tmin=0*second,
        tmax=30*second,
        FS=11025 *Hz,
        tau_omega=10 * ms,
        tau_w= 3. * second,
        Noutputs=4,
        frequency_factor=11,
        alpha_wiskott=1,
        w_0=[],
        dtRecord=-1*second):
    set_device('cpp_standalone')
    device.reinit()

    #-------------
    # Parameters
    #-------------

    print '----CONTINUOUS---- fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega)

    # -------
    # Solver
    # -------

    if dtRecord < 0:
        dtRecord = (tmax-tmin)/10**4

    tCount = time.clock()


    ## WISKOTT INPUT
    if input_type == 0:
        N = int( (tmax- tmin) * FS)

        # Define input
        inp = il.Input(tmin,tmax,N)
        inp.set_wiskott_classic(fWiskott,frequency_factor,alpha_wiskott)
        inp.sphereInputs()

        eq = '''                        x_th = int(i==0) * ta0(t) \
                                        + int(i==1) * ta1(t) \
                                        + int(i==2) * ta2(t) \
                                        + int(i==3) * ta3(t) \
                                        + int(i==4) *ta4(t) : 1
                                        '''


        Ninputs = inp.n
        tCount = ti.timeCount(tCount,'Input setup (s):')
    ## AUDIO INPUT
    else:
        N = int(T_audio * FS)
        #Define input
        inp = il.Input(tmin,T_audio+tmin,N)
        inp.addWaveFromFile(input_file_name,0*second)
        inp.n= 1
        inp.X = np.zeros((1,N))
        inp.X[0,:] = inp.x
        inp.whitenInputs()

        Ninputs = N_delays

        eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''


        tCount = ti.timeCount(tCount,'Input setup (s):')

    # ------------- Brian
    Texp = tmax - tmin
    defaultclock.dt = inp.dt
    Dt = defaultclock.dt

    # -----------------------
    X_in = inp.X
    tCount = ti.timeCount(tCount,'High pass input setup (s):')
    ta0 = TimedArray(X_in[0,:],defaultclock.dt)

    ## WISKOTT INPUT END
    if input_type==0:
        ta1 = TimedArray(X_in[1,:],Dt)
        ta2 = TimedArray(X_in[2,:],Dt)
        ta3 = TimedArray(X_in[3,:],Dt)
        ta4 = TimedArray(X_in[4,:],Dt)

    G_pre = NeuronGroup(Ninputs, eq + '''

                                        d x_exp /dt = (x_th - x_exp ) / tau_omega : 1
                                        d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1''')

    G_post = NeuronGroup(Noutputs,'''
                s : 1

                d s_exp / dt = (s - s_exp) / tau_omega : 1
                d s_alpha / dt = (s_exp - s_alpha) / tau_omega : 1

                norm_w : 1''')

    G_post.norm_w = '1'

    S = Synapses(G_pre,G_post,
                 model='''
                 STDP =  (x_exp_pre - x_alpha_pre) * s_post + (s_exp_post - s_alpha_post) * x_th_pre: 1

                 d w / dt = STDP / tau_w : 1

                 s_post = w * x_th_pre  : 1 (summed)
                 norm_w_post = w*w  : 1 (summed)''',
                 connect=True)

    re_norm = S.custom_operation('''
                w /= sqrt(norm_w_post)
                ''',when='weight_renorm')


    if len(w_0) == 0:
        w_0 = np.random.randn(Ninputs)
        w_0 /= norm(w_0)

    for Ni in range(0,Ninputs):
        S.w['i == %d' % Ni] = w_0[Ni]


    MagicNetwork.schedule = ['start','groups','thresholds','synapses','resets','weight_renorm','end']


    mon_pre = StateMonitor(G_pre, ['x_exp','x_alpha'], record=arange(0,Ninputs),dt=dtRecord)
    mon_post = StateMonitor(G_post, ['s','norm_w'], record=arange(0,Noutputs),dt=dtRecord)
    mon_syn = StateMonitor(S, ['w'], record=arange(0,Ninputs*Noutputs),dt=dtRecord)

    tCount = ti.timeCount(tCount,'Brian setup (s):')
    run(Texp, report="text")
    device.build(directory='output', compile=True, run=True, debug=False)
    tCount = ti.timeCount(tCount,'Brian run (s):')

    Trec = range(0,len(mon_post.s[0,:])) * dtRecord

    return Trec,mon_pre.x_alpha,mon_post.s,mon_syn.w,np.array(S.w),S._postsynaptic_idx


def looper(
        T_audio=20 * second,
        input_file_name="../../data/countryguit_short_11025.wav",
        N_delays=128,
        tau_delay = 9  / (11025 * Hz),
        tmax=30*second,
        tmin = 0*second,
        FS=11025 *Hz,
        dtRecord=-1*second):


    #-------------
    # Parameters
    #-------------

    N = int(T_audio * FS)
    print '----Looper---- '

    # -------
    # Solver
    # -------

    if dtRecord < 0:
        dtRecord = (tmax-tmin)/10**4

    tCount = time.clock()

    #Define input
    inp = il.Input(tmin,T_audio+tmin,N)
    inp.addWaveFromFile(input_file_name,0*second)
    inp.n= 1
    inp.X = np.zeros((1,N))
    inp.X[0,:] = inp.x
    inp.whitenInputs()

    Ninputs = N_delays


    # ------------- Brian
    Texp = tmax - tmin
    defaultclock.dt = inp.dt
    Dt = defaultclock.dt

    # -----------------------
    X_in = inp.X
    ta0 = TimedArray(X_in[0,:],defaultclock.dt)


    eq = '''
            t_i = t - tau_delay * i : second
            t_loop_i = t_i - floor(t_i/T_audio)*T_audio : second
            x_th = ta0(t_loop_i) : 1
            '''
    G_pre = NeuronGroup(Ninputs, eq)

    mon_pre = StateMonitor(G_pre, ['x_th'], record=arange(0,Ninputs),dt=dtRecord)

    run(Texp)

    Trec = range(len(mon_pre.x_th[0,:])) * dtRecord
    return Trec,np.array(mon_pre.x_th)
