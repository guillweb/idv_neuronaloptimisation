from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.eigs_optimisation.eigs_optimisers as eo
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time


def run_SFA_wiskott(noise_coeff=0.,fWiskott=1*Hz,tmin=0*second,tmax=30*second,FS=10**4 * Hz,tau_omega=10 * ms,tau_w0= 3. * second,Noutputs=4,frequency_factor=11,cos_coeff=1,w_0=[]):


    t = time.clock()

    if tau_omega >0:
        FS = max(FS,1/(tau_omega/5))
        FS = min(FS,1000/(20 * tau_omega))

    N = int((tmax - tmin) * FS)

    print '-----WISKOTT--- FS:{3}, fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax-tmin,tau_omega,FS)


    tCount = time.clock()

    # Define input
    inp = il.Input(tmin,tmax,N)
    inp.set_wiskott_classic(fWiskott,frequency_factor,cos_coeff=cos_coeff,noise_coeff=noise_coeff)
    inp.sphereInputs()


    # Create STDP window filter (Omega)
    if tau_omega >0:tau_period = 20 * tau_omega
    else:tau_period = 10/FS
    builder = fb.FilterBuilder(inp.T,tau_period)


    output = np.zeros((Noutputs,N))
    for i in range(0,Noutputs):
        F = builder.newFilter()

        opt = eo.EigOpt(inp.X,1)
        opt.computeSFA()

        t = ti.timeCount(t,'Output {0} (s):'.format(i))

        output[i,:] = opt.getOutput().ravel()



    return inp.T,inp.X,output

