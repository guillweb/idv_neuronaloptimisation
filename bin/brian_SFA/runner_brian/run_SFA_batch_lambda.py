from brian2 import *
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from scipy.linalg import block_diag
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

import bin.input_loader.recursive_filters as rf
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.brian_gradient_optimisation.tools as bt
import time


def run_SFA_batch_lambda(noise_coeff=0.,renorm_type=0,opt_type=0,N_iteration=10**4,al_opt = 0.9,record_every=10**3,windowType=[0,1,2,3],fWiskott=1*Hz,tmin=0*second,tmax=30*second,FS=10**4 * Hz,tau_omega=10 * ms,tau_w0= 3. * second,Noutputs=4,frequency_factor=11,cos_coeff=1,w_0=[]):

    t = time.clock()

    if tau_omega >0:
        FS = max(FS,1/(tau_omega/5))
        FS = min(FS,1000/(20 * tau_omega))

    N = int((tmax - tmin) * FS)

    print '-----BATCH--- FS:{3}, fWiskott: {0}, Duration: {1}, tau_omega: {2}'.format(fWiskott,tmax- tmin,tau_omega,FS)

    opt_param=[[],[]]
    opt_param[0] = [N_iteration,al_opt,record_every]

    tCount = time.clock()

    # Define input
    inp = il.Input(tmin,tmax,N)
    inp.set_wiskott_classic(fWiskott,frequency_factor,cos_coeff=cos_coeff,noise_coeff=noise_coeff)
    inp.sphereInputs()

    # Create STDP window filter (Omega)
    if tau_omega >0:tau_period = 20 * tau_omega
    else:tau_period = 10/FS
    builder = fb.FilterBuilder(inp.T,tau_period)


    output = np.zeros((Noutputs,N))
    for i in range(0,Noutputs):
        F = builder.newFilter()

        if tau_omega >0:
            if windowType[i] == 2:F = builder.getSigmaFilter(tau_omega)
            elif windowType[i] == 1:F = builder.getDerivativeFilter(tau_omega)
            elif windowType[i] == 3:F.setFilter(- builder.getSigmaFilter(tau_omega).getFilter())
            else:F = builder.getSFAFilter(tau_omega)
        else:
            if windowType[i] == 2:F = builder.getDiracFilter()
            elif windowType[i] == 1:F = builder.get_D_filter(1)
            elif windowType[i] == 3:F.setFilter(- builder.getDiracFilter().getFilter())
            else:F = builder.get_D_filter(2)


        opt = fo.FilterOpt(inp,F,opt_type,opt_param,renorm_type,w_0)
        opt.solve()
        t = ti.timeCount(t,'Output {0} (s):'.format(i))

        output[i,:] = opt.getOutput().ravel()


    return inp.T,inp.X,output




def run_SFA_batch_audio(epsi=10**-6,mu =0,X_in=[],R_proj=[],tau_omega=10*ms,tau_delay=-1*second,tmin=.0*second,tmax=5.*second,FS=11025,N_iteration=10**4,al_opt=0.9,record_every=10**2,inputFileName="../../data/countryguit_11025.wav",N_delays=100,w_0=[]):

    t = time.clock()
    # ----------------
    # INPUT PARAMETERS
    # ----------------
    N = int((tmax - tmin) * FS)

    if tau_delay < 0* second:
        tau_delay = tau_omega

    # -------
    # Solver
    # -------

    # Define input
    inp = il.Input(tmin,tmax,N)
    offset = tmin

    if len(X_in) > 0:
        inp.X= X_in
        inp.n = np.shape(X_in)[0]
    else:
        inp.addWaveFromFile(inputFileName,offset)
        inp.whitenInputs()
        inp.expandWithDelay(N_delays,tau_delay)
        _,R_proj = inp.sphereInputs(False,epsi)


    if tau_omega > 1/(FS*Hz):
        builder = fb.Filter(inp.T,tau_omega * 20)
        F = builder.getSFAFilter(tau_omega,set_matrix=False)
    else:
        builder = fb.Filter(inp.T,10/(FS*Hz))
        F = builder.get_D_filter(2)

    if mu >0:
        F1 = builder.getDiracFilter()
        F.setFilter(F.getFilter() - mu * F1.getFilter(),set_matrix=False)


    t = ti.timeCount(t,'Input setup (s):')


    if len(w_0) != N_delays:
        opt = fo.FilterOpt(inp,F,optParam=[[N_iteration,al_opt,record_every]])
    else:
        opt = fo.FilterOpt(inp,F,optParam=[[N_iteration,al_opt,record_every]],w_0=w_0)

    opt.solve()
    output = opt.getOutput().ravel()
    cost_sequence,log_cost_seq = opt.get_objective_sequence()

    t = ti.timeCount(t,'Solve setup (s):')

    return inp.T,inp.X,R_proj,output,opt.W.ravel(),log_cost_seq

