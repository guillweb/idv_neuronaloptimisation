__author__ = 'guillaume'
import check_validity

from brian2 import *
import numpy as np
import os
import glob
import cPickle as pickle



def get_file_name(file_path,id,pre_fix='',post_fix='.pickle'):
    end = pre_fix + '_%05d' % id + post_fix
    return file_path + 'params/' + end,file_path + 'objects/' + end

def get_file_id(file_path,id=0,pre_fix='',post_fix='.pickle'):

    file_name_params,file_name_obj = get_file_name(file_path,id,pre_fix,post_fix)
    while os.path.isfile(file_name_params):
        id += 1
        file_name_params,file_name_obj = get_file_name(file_path,id,pre_fix,post_fix)

    return id,file_name_params,file_name_obj

def save_result(file_path,params,OUT,T_list={},pre_fix='',post_fix='.pickle',id=0):

    param_object = {'params': params}
    object = {'params': params, 'OUT': OUT, 'T_list': T_list}

    id,file_name_param,file_name_obj = get_file_id(file_path,id,pre_fix,post_fix)

    f = open(file_name_param,'wb')
    pickle.dump(param_object,f)

    f = open(file_name_obj,'wb')
    pickle.dump(object,f)

def find_results_in_db(file_path,params,pre_fix='',post_fix='.pickle',number=10):

    file_path_params = file_path + 'params/'
    count = 0
    list= []
    for file in os.listdir(file_path_params):
        print pre_fix
        if file.endswith(post_fix) and file.startswith(pre_fix):

            param_check = True
            file_name =  file_path_params + file
            f = open(file_name,'rb')
            param_obj = pickle.load(f)

            if 'params' in param_obj:

                for key in params:
                    if not(key in param_obj['params']) or np.array(params[key] != param_obj['params'][key]).any():
                        param_check = False
                        break

                if param_check:
                    list.append(file)
                    count += 1
                    if count == number:
                        break

    return count,list

def get_data(file_name):
    f = open(file_name,'rb')
    obj = pickle.load(f)

    return obj['T_list'],obj['OUT'],obj['params']

def get_correlation_matrix_from_file(file_name):
    f = open(file_name,'rb')
    obj = pickle.load(f)

    return check_validity.check_correlation(obj['T_list'],obj['OUT'])

def build_correlation_statistic(db_path,params,number=10,geometrical_mean=True):
    count,list = find_results_in_db(db_path,params,number=number)

    CORR = []
    for file in list:
        file_name = db_path + file
        CORR.append(get_correlation_matrix_from_file(file_name))

    if geometrical_mean:
        corr = np.ones(np.shape(CORR[0]))
        for i in arange(len(CORR)):
            corr *= CORR[i]
        corr = np.power(corr,1./len(CORR))

    else:
        corr = np.zeros(np.shape(CORR[0]))
        for i in arange(len(CORR)):
            corr += CORR[i]
        corr /= len(CORR)

    return corr







