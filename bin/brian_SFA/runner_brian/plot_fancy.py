__author__ = 'guillaume'

import matplotlib
import matplotlib.pyplot as plt
from brian2 import *

from matplotlib import gridspec




def plot_s(T,s,Noutputs=4,tmin=-1*second,tmax=-1*second,s_al=[],one_fig=False,linestyles=[]):
    '''
    Plot de output
    '''
    set_font()
    color = get_color()
    win = get_win()


    tstart = 0
    tend = len(T)
    dtRecord = T[1] - T[0]

    if tmin >0:tstart = int(tmin / dtRecord)
    else:tstart = 0

    if tmax >0:tend = int(tmax / dtRecord)
    else: tend= len(T)

    if one_fig:
        plt.figure()


    for i in range(Noutputs-1,-1,-1):
        if one_fig: plt.subplot(221+i)
        else: plt.figure()


        plt.plot(T[tstart:tend],s[i,tstart:tend],color=color[i],label=win[i],linewidth=2.0)
        if s_al:
            plt.plot(T[tstart:tend],s_al[i,tstart:tend],'r--',color=color[i],label=win[i],linewidth=2.0)

            #xlim([0,0.25])
            #ylim([-0.01,0.01])
            #figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])

        xlabel(r't (s)')
        ylabel(r'$s$')

def plot_s_embed(T,s,background=[],Noutputs=4,subplot=[],titles=[],xlabels=[],ylabels=[],colors=[],linestyles=[],tmin=-1*second,tmax=-1*second,lim=True):

    set_font()

    ax_list = []

    if len(subplot) == Noutputs:

        for No in range(0,Noutputs):
            if len(np.shape(s)) > 1:
                s_raveled = s[No,:].ravel()
            else: s_raveled = s

            if tmin >0 and tmax >0:
                dt = T[1] - T[0]
                int_min = int(tmin/dt)
                int_max = int(tmax/dt)

                T= T[int_min:int_max]
                s_raveled = s_raveled[int_min:int_max]
                background= background[int_min:int_max]

            pt = plt.subplot(subplot[No])


            if len(colors) > No and colors[No]: color = colors[No]
            else: color = 'black'

            if len(linestyles) > No and linestyles[No]: linestyle = linestyles[No]
            else: linestyle = '-'


            if len(background) >0:
                pt.plot(T,background,linestyle,color='gray',linewidth=2.0)
            pt.plot(T,s_raveled,linestyle,color=color,linewidth=2.0)

            if len(xlabels) > No and xlabels[No]:
                xlabel(xlabels[No])

            if len(ylabels) > No and ylabels[No]:
                ylabel(ylabels[No])

            if len(titles) > No and titles[No]:
                pt.set_title(titles[No])



            pt.locator_params(axis = 'x', nbins = 3)
            pt.locator_params(axis = 'y', nbins = 4)

            if lim:
                pt.set_ylim([-2.5,2.5])

            set_small_tick_font(pt,2)
            ax_list.append(pt)
    else:
        print 'ERROR: Nouputs is different from the number of subplots in the grid'

    return ax_list

def plot_x_embed(T,x,Ninputs=4,subplot=[],titles=[],xlabels=[],ylabels=[],colors=[],linestyles=[]):

    set_font()

    ax_list = []
    if len(subplot) == Ninputs:

        for Ni in range(0,Ninputs):
            if len(np.shape(x)) > 1:
                x_raveled = x[Ni,:].ravel()
            else:
                x_raveled = x

            pt = plt.subplot(subplot[Ni])

            if len(colors) > Ni and colors[Ni]: color = colors[Ni]
            else: color = 'blue'


            if len(linestyles) > Ni and linestyles[Ni]: linestyle = linestyles[Ni]
            else: linestyle = '-'

            pt.plot(T,x_raveled,linestyle,linewidth=2.0,color=color)

            if len(xlabels) > Ni and xlabels[Ni]:
                xlabel(xlabels[Ni])

            if len(ylabels) > Ni and ylabels[Ni]:
                ylabel(ylabels[Ni])

            if len(titles) > Ni and titles[Ni]:
                pt.set_title(titles[Ni])



            pt.locator_params(axis = 'x', nbins = 3)
            pt.locator_params(axis = 'y', nbins = 4)
            pt.set_ylim([-2.5,2.5])

            set_small_tick_font(pt,2)
            ax_list.append(pt)
    else:
        print 'ERROR: Nouputs is different from the number of subplots in the grid'
    return ax_list

def plot_kernels_embed(T,x_list,N_kernels=4,subplot=[],titles=[],xlabels=[],ylabels=[],colors=[]):

    T_ms = T / (1 * ms)

    set_font()

    ylim_plus = 3*np.std(x_list)
    xlim_plus = max(T_ms)

    ax_list = []
    if len(subplot) == N_kernels:

        for Nk in range(0,N_kernels):
            x = x_list[Nk]
            if len(np.shape(x)) > 1:
                x_raveled = x[Nk,:].ravel()
            else:
                x_raveled = x

            pt = plt.subplot(subplot[Nk])

            if len(colors) > Nk and colors[Nk]: color = colors[Nk]
            else: color = 'black'
            pt.plot(T_ms,x_raveled,linewidth=2.0,color = color)

            if len(xlabels) > Nk and xlabels[Nk]:
                xlabel(xlabels[Nk])

            if len(ylabels) > Nk and ylabels[Nk]:
                ylabel(ylabels[Nk])

            if len(titles) > Nk and titles[Nk]:
                pt.set_title(titles[Nk])


            pt.locator_params(axis = 'x', nbins = 4)
            pt.locator_params(axis = 'y', nbins = 4)

            pt.set_ylim([-ylim_plus,ylim_plus])
            pt.set_xlim([-xlim_plus,xlim_plus])

            set_small_tick_font(pt,2)
            ax_list.append(pt)
    else:
        print 'ERROR: Nouputs is different from the number of subplots in the grid'

    return ax_list

def plot_w(T,w,post_idx,Noutputs=4,w_al=[],one_fig=False,n_recorded=-1):

    set_font()

    color = get_color()

    if one_fig:
        plt.figure()
    for No in range(Noutputs-1,-1,-1):
        if one_fig: plt.subplot(221+No)
        else: plt.figure()

        if n_recorded <0:
            idx_No = find(post_idx == No)
        else:
            idx_No = arange(n_recorded) * (No+1)


        plt.plot(T,w[idx_No,:].T,'r--',color=color[No],linewidth=2.0)
        plt.plot(T,w[idx_No,:].T,color=color[No],linewidth=2.0)
        xlabel(r't (s)')
        ylabel(r'weights')
        plt.locator_params(axis = 'x', nbins = 4)
        plt.locator_params(axis = 'y', nbins = 4)

def set_small_tick_font(ax,axis=2,size=16):
    if axis in [0,2]:
        ticks_font = matplotlib.font_manager.FontProperties(size=size, weight='normal')

        for label in ax.get_xticklabels():
            label.set_fontproperties(ticks_font)

    if axis in [1,2]:
        ticks_font = matplotlib.font_manager.FontProperties(size=size, weight='normal')

        for label in ax.get_yticklabels():
            label.set_fontproperties(ticks_font)

def set_font():

    matplotlib.rc('font', size=20)
    matplotlib.rc('xtick', labelsize='20')
    matplotlib.rc('ytick', labelsize='20')

def get_color():
    return ["black","magenta","darkcyan","brown"]

def get_win():
    win = [r'$\Lambda_{SFA} = d_{(2)}$',r'$\Lambda_{Classic} = - d_{(1)} $',r'$\Lambda_{Hebbian+} = \delta_0$',r'$\Lambda_{Hebbian-} = - \delta_0$']
    return win

def plot_online(T,w,s,post_synaptic_idx,t_period=4*second,subplot=[],output_id=0,color=''):

    set_font()

    dt = T[1] - T[0]
    t_start_before = 0
    t_end_before = int(t_period/dt)


    t_start_after = len(T) - t_end_before
    t_end_after = len(T)



    if len(subplot)  == 0:
        gs = gridspec.GridSpec(2, 2)
    else:
        gs = gridspec.GridSpecFromSubplotSpec(2,2,subplot_spec=subplot[0])


    #Create plots
    before = plt.subplot(gs[0,0])
    after = plt.subplot(gs[0,1])
    weights = plt.subplot(gs[1,:])

    if not(color): color = 'black'
    #Plot
    before.plot(T[t_start_before:t_end_before],s[output_id,t_start_before:t_end_before],color=color,linewidth=2.0)
    after.plot(T[t_start_after:t_end_after],s[output_id,t_start_after:t_end_after],color=color,linewidth=2.0)

    inds = find(post_synaptic_idx == output_id)
    weights.plot(T,w[inds,:].T,color=color,linewidth=2.0)

    # Titles
    before.set_title('Online (start)')
    after.set_title('Online (end)')


    before.locator_params(axis = 'x', nbins = 3)
    before.locator_params(axis = 'y', nbins = 4)
    before.set_ylim([-2.5,2.5])

    after.locator_params(axis = 'x', nbins = 3)
    after.locator_params(axis = 'y', nbins = 4)
    after.set_ylim([-2.5,2.5])

    weights.locator_params(axis = 'x', nbins = 4)
    weights.locator_params(axis = 'y', nbins = 3)
    weights.set_ylim([-1,1])

    # X and Y labels
    #before.set_xlabel('t (s)')
    #after.set_xlabel('t (s)')
    weights.set_xlabel('t (s)')

    before.set_ylabel(r'$s$')
    weights.set_ylabel('weights')

    # Breaking between before and after
    before.spines['right'].set_visible(False)
    after.spines['left'].set_visible(False)

    before.yaxis.tick_left()
    after.yaxis.tick_right()

    after.tick_params(labelleft='off') # don't put tick labels at the top

    d = .05 # how big to make the diagonal lines in axes coordinates
    # arguments to pass plot, just so we don't keep repeating them
    kwargs = dict(transform=before.transAxes, color='k', clip_on=False)
    before.plot((1-d,1+d),(1-d,1+d), **kwargs)      # bottom-right diagonal
    before.plot((1-d,1+d),(-d,+d), **kwargs)    # tpop-right diagonal

    kwargs.update(transform=after.transAxes)  # switch to the bottom axes
    after.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
    after.plot((-d,+d),(-d,+d), **kwargs) # top-left diagonal

def plot_correlation_matrix(mat,x_tick_labels=[],y_tick_color=[],y_tick_labels=[],cmap='RdYlGn',subplot=[],xlabel='',title=''):

    set_font()

    if len(subplot) > 0:
        ax = plt.subplot(subplot[0])
    else:
        ax = plt.subplot(111)

    im = ax.pcolor(mat, cmap=cmap)


    ax.set_yticks(np.arange(mat.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(mat.shape[1]) + 0.5, minor=False)

    ax.set_xticklabels(x_tick_labels, minor=False)
    ax.set_yticklabels(y_tick_labels, minor=False)

    ax.grid(False)

    # Turn off all the ticks
    ax = plt.gca()

    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False

    if title: ax.set_title(title)
    if xlabel: ax.set_xlabel(xlabel)


    if len(y_tick_color) >0:
        No=0
        for i in plt.gca().get_yticklabels():
            i.set_color(y_tick_color[No])
            No += 1

    return ax,im
