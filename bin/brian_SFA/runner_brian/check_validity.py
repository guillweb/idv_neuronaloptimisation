__author__ = 'guillaume'

import numpy as np
from brian2 import *

def check_correlation(T_list,OUT,fWiskott = 1*Hz):

    K = len(OUT)
    P = np.shape(OUT[0])[0]

    table = np.zeros((K,P))

    for i in arange(K):
        if len(T_list) == 1: T= T_list[0]
        else: T = T_list[i]

        s = OUT[i]
        sol = np.sin( 2 * np. pi * fWiskott * T)
        sol /= np.std(sol)

        corr = np.dot(s,sol.T) / len(T)
        corr /= np.std(s,axis=1)

        table[i,:] = np.abs(corr)
        del s,corr,T

    return table