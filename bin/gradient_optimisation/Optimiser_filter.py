import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
import random as rd


class GradientOptimiser:
    def  __init__(self,A0,X,S0):



        # Iteration number
        self.Niterations = 100

        # Iteration on A and S at each step
        self.NitA = 1
        self.NitS = 1

        # Optimisation parameters
        self.epsilon = 0.001    #Stop criterium
        self.powerDecay = 0     #Decay over iterations of gamma

        self.gamma0 = 1         #Descent coefficient
        self.gamA = 0.33           #Descent coefficient multiplied by theorerical upper convergent value.
        self.gamS = 0.33           #Same for S

        # Initialisation of the matrix
        self.A = A0
        self.X = X
        self.S = S0
        self.R = np.matrix(np.zeros( np.shape(S0)) )

        self.Xfull = X
        self.Sfull = S0

        self.Nouput = np.shape(S0)[0]
        self.Ttotal = np.shape(S0)[1]
        self.Tw = self.Ttotal

        # Window
        self.w0 = np.ones(self.Tw)
        self.wInd = range(0,self.Tw)
        self.W = np.diag(self.w0)

        self.verbose = False

        # Regularization parameters
        self.muReg = 0
        self.muL1 = 0
        self.muTV = 0
        self.muSFA = 0
        self.muRegA = 0

        # Cosntrains parameters
        self.nuICA = 0


        # Useful calculus
        T = np.shape(self.X)[1]
        self.grad = np.diag( -np.ones(T-1), k=-1 ) + np.diag( np.ones(T) ) # gradient forward derivation scheme
        self.grad[1,0] = 1 # Reflexive condition at t=0

        self.delta = np.diag( 2*np.ones(T) ) \
                    + np.diag( -np.ones(T-1),k=+1) \
                    + np.diag( -np.ones(T-1),k=-1) # Laplacian as the gradient of the L2 norm of the gradient
        self.delta[1,0] = -2 # Reflexive boundary
        self.delta[T-2,T-1] = -2 # Reflexive boundary


        # Record values over iterations
        self.Esequence = np.zeros(self.Niterations)
        self.Csequence = np.zeros(self.Niterations)


        self.__update()

    def update(self):
        return 0

    def updateWindow(self,k):
        return 0


    def criterium(self):
            return la.norm(self.gradA()) + la.norm(self.gradS())
            #return numpy.linalg.norm(self.gradA()) + numpy.linalg.norm(self.gradS());

    def cost(self):
            return self.costPrecision() + self.costReg() + self.costConst()

    def costPrecision(self):
            return -1

    def costReg(self):
            return -1

    def costConst(self):
            return -1

    # Gradient definition
    def gradA(self):
        return 0

    def gradS(self):
        return 0

    def gradRegS(self):
        return 0

    def gradConstS(self):
        return 0

    # Projection
    def projS(self):
        return self.S

    def projA(self):
        return self.A

    __update = update

    # Solver scheme
    def solve(self):
        self.Esequence = np.zeros((3,self.Niterations))
        self.Csequence = np.zeros((3,self.Niterations))
        k=0
        gamma = self.gamma0

        if self.verbose:
            print("Initial cost:")
            print(self.cost())



        while self.criterium() > self.epsilon:

            if k >= self.Niterations:
                break

            self.updateWindow(k)

            for kA in range(1, self.NitA+1):
                c = self.gamA * 1/max(10**-5,la.norm(self.X * self.X.conj().transpose()))
                self.A -= c * self.gradA()
                self.A = self.projA()

            for kS in range(1, self.NitS+1):
                c = self.gamS * gamma
                g =  self.gradS() +  self.gradRegS() +  self.gradConstS()
                self.S[:,self.wInd] -= c * g[:,self.wInd] * self.W
                self.S[:,self.wInd] = self.projS()[:,self.wInd]

            gamma = float(self.gamma0) / pow(k+1, self.powerDecay)
            self.Esequence[0,k] = self.costPrecision()
            self.Esequence[1,k] = self.costReg()
            self.Esequence[2,k] = self.costConst()

            self.Csequence[0,k] = la.norm(self.gradS())
            self.Csequence[1,k] = la.norm(self.gradRegS())
            self.Csequence[2,k] = la.norm(self.gradA())
            k = k+1



        self.Esequence = self.Esequence[:,0:k-1]
        self.Csequence = self.Csequence[:,0:k-1]

        if self.verbose:
            print("Iteration, criterium and cost:")
            print k, self.criterium(), self.cost()


            print("Filter:")
            print(self.A)


            plt.figure(1)
            plt.subplot(331)
            plt.plot(np.log10(self.Esequence[0, :] + self.epsilon))
            plt.xlabel("Iterations")
            plt.title("Log10 of Cost Presicion")

            plt.subplot(332)
            plt.plot(np.log10(self.Esequence[1, :] + self.epsilon))
            plt.xlabel("Iterations")
            plt.title("Log10 of Cost Regularisation")

            plt.subplot(333)
            plt.plot(np.log10(self.Esequence[2, :] + self.epsilon))
            plt.xlabel("Iterations")
            plt.title("Log10 of Cost")


            plt.subplot(334)
            plt.plot(np.log10(self.Csequence[2, :]+ self.epsilon ))
            plt.xlabel("Iterations")
            plt.title("Log10 of A-Gradient norm")



            plt.subplot(335)
            plt.plot(np.transpose(self.X))
            plt.xlabel("Time")
            plt.title("X(t)")

            plt.subplot(336)
            plt.plot(np.transpose(self.S[0,:]))
            plt.xlabel("Time")
            plt.title("S(t)")

            plt.subplot(337)
            plt.plot(np.transpose(self.A[0,:]))
            plt.xlabel("Filter")
            plt.title("Delay")

            if np.shape(self.S)[0] > 1:
                plt.subplot(338)
                plt.plot(np.transpose(self.S[1,:]))
                plt.xlabel("Time")
                plt.title("S(t)")

                plt.subplot(339)
                plt.plot(np.transpose(self.A[1,:]))
                plt.xlabel("Filter")
                plt.title("Delay")



            plt.show()

        return self.criterium(), self.Niterations




class Inverser(GradientOptimiser):

        # Auxiliary function and definitions
        def Thr(self,a,lbda,mu):
            return np.multiply(a,np.maximum(1 - lbda * mu / np.maximum(np.abs(a), 10**-10), 0 ))

        def proxL1Dual(self,a,sigma,mu):
            return a - sigma * self.Thr(a /sigma,1/sigma,mu)

        # Update init object method
        def update(self):
            self.NitA = 0
            self.NitS = 1
            self.muReg = 1
            self.muSFA = 0
            self.muL1 = 0
            self.muTV = 0
            self.nuICA = 0

        # Gradient
        def gradS(self):
            return 2* (self.S - self.A * self.X - self.R)

        def gradRegS(self):
            return  2* self.muReg * self.S \
                    + 2* self.muSFA * self.S * self.delta

        def gradConstS(self):
            #c= 1
            c = 1/ np.maximum(la.norm( self.S * self.S.transpose()),10**-10)
            M = (self.S * self.S.transpose() - np.eye(np.shape(self.S)[0])) * self.S
            #M = (self.S * self.S.transpose() - np.diag(self.S * self.S.transpose() )) * self.S
            #M = np.tril(self.S * self.S.transpose() * self.S)
            return self.nuICA * c *2* M
            #M = self.S * self.S.transpose() * self.S - np.power(self.S,3)
            #M = np.tril(self.S * self.S.transpose() * self.S)
            #return self.nuICA * c *2* M
            #return self.nuICA * c * (self.S * self.S.transpose() - np.diag(self.S * self.S.transpose() )) * self.S
            #return self.nuICA * c * (self.S * self.S.transpose() - np.eye(np.shape(self.S)[0])) * self.S

        def projS(self):
            gradDual = self.grad.conj().transpose()
            #gradDual = -self.grad #dual of grad is minus divergence

            sigma = 0.5 * 1/np.sqrt(la.norm(gradDual * self.grad))
            tau = sigma
            gradStmp = self.proxL1Dual( self.S + np.multiply(sigma,self.S * self.grad), sigma,self.muTV)

            return self.Thr(self.S,self.gamS,self.muL1) + self.S - self.muTV * tau *  gradStmp * gradDual



        # Cost function
        def costPrecision(self):
            return la.norm(self.S - self.A * self.X - self.R)**2

        def costReg(self):
            return  self.muReg * la.norm(self.S)**2 \
                    + self.muSFA * la.norm(self.S * self.grad)**2 \
                    + self.muL1 * la.norm(self.S, ord=1) \
                    + self.muTV * la.norm(self.S * self.grad,ord=1) \
                    + self.muRegA * la.norm(self.A)

        def costConst(self):
            #return  self.nuICA * la.norm(self.S * self.S.transpose() - np.diag(self.S * self.S.transpose() ))**2
            return  self.nuICA * la.norm(self.S * self.S.transpose() - np.eye(np.shape(self.S)[0]))**2





class FilterLearner(Inverser):

    def update(self):
            self.NitA = 1
            self.NitS = 1

    def gradA(self):
        return 2* (self.A*self.X + self.R - self.S) * self.X.conj().transpose() + 2* self.muRegA * self.A

    #def projA(self):
    #    nA = la.norm(self.A,axis=1)
    #    return (self.A.transpose() / np.maximum(nA,10**-10)).transpose()




class StochFilterLearner(FilterLearner):

    def updateWindow(self,k):
        self.wInd = rd.sample(xrange(self.Ttotal),self.Tw)
        self.w0 = np.ones(self.Tw)

        self.W = np.diag(self.w0)
        return 0

class SlideFilterLearner(FilterLearner):

    def updateWindow(self,k):


        offset = int(np.floor(float(k)/float(self.Niterations) * (self.Ttotal - self.Tw)))
        wIndEff = range(offset,self.Tw+offset)

        self.wInd = wIndEff
        self.w0 = np.ones(self.Tw)
        self.W = np.diag(self.w0)
        return 0


class NonLinearExpanded:
    def __init__(self,X):
        self.X = X
        self.updateStats()

    def updateStats(self):
        self.meanVals = np.mean(self.X,axis=1)
        self.stdVals = np.std(self.X,axis=1)

    def normalize(self):
        self.updateStats()
        self.X = self.X - self.meanVals
        self.X = self.X / self.stdVals

        return self.X

    def addQuadratic(self):
        self.normalize()
        Nrows = np.shape(self.X)[0]

        for i in range(0,Nrows):
            for j in range(0,i):
                self.X = np.vstack((self.X, np.multiply(self.X[i, :],self.X[j, :])))

        self.normalize()
        return self.X