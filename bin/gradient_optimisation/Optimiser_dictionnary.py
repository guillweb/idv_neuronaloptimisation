import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt


class GradientOptimiser:
    def  __init__(self,A0,X,S0):



        # Iteration number
        self.Niteration = 100

        # Iteration on A and S at each step
        self.NitA = 1
        self.NitS = 1

        # Optimisation parameters
        self.epsilon = 0.001    #Stop criterium
        self.powerDecay = 0     #Decay over iterations of gamma

        self.gamma0 = 1         #Descent coefficient
        self.gamA = 0           #Descent coefficient multiplied by theorerical upper convergent value.
        self.gamS = 0           #Same for S

        # Initialisation of the matrix
        self.A = A0
        self.X = X
        self.S = S0


        self.verbose = False

        # Regularization parameters
        self.muReg = 0
        self.muL1 = 0
        self.muTV = 0
        self.muSFA = 0


        # Useful calculus
        T = np.shape(self.X)[1]
        self.grad = np.diag( -np.ones(T-1), k=-1 ) + np.diag( np.ones(T) ) # gradient forward derivation scheme
        self.grad[1,0] = 1 # Reflexive condition at t=0

        self.delta = np.diag( 2*np.ones(T) ) \
                    + np.diag( -np.ones(T-1),k=+1) \
                    + np.diag( -np.ones(T-1),k=-1) # Laplacian as the gradient of the L2 norm of the gradient
        self.delta[1,0] = -2 # Reflexive boundary
        self.delta[T-2,T-1] = -2 # Reflexive boundary


        # Record values over iterations
        self.Esequence = np.zeros(self.Niteration)
        self.Csequence = np.zeros(self.Niteration)


        self.__update()

    def update(self):
        return 0


    def criterium(self):
            return la.norm(self.gradA()) + la.norm(self.gradS())
            #return numpy.linalg.norm(self.gradA()) + numpy.linalg.norm(self.gradS());

    def cost(self):
            return -1

    # Gradient definition
    def gradA(self):
        return 0

    def gradS(self):
        return 0

    def gradRegS(self):
        return 0

    # Projection
    def projS(self):
        return self.S

    def projA(self):
        return self.A

    __update = update

    # Solver scheme
    def solve(self):
        self.Esequence = np.zeros(self.Niteration)
        self.Csequence = np.zeros(self.Niteration)
        k=0
        gamma = self.gamma0

        if self.verbose:
            print("Initial cost:")
            print(self.cost())



        while self.criterium() > self.epsilon:

            if k >= self.Niteration:
                break

            self.gamA = gamma * 0.33 * 1/max(10**-5,la.norm(self.S * self.S.conj().transpose()))
            for kA in range(1, self.NitA+1):
                self.A -= self.gamA * self.gradA()
                self.A = self.projA()

            self.gamS = gamma * 0.33 * 1/max(10**-5,la.norm(self.A * self.A.conj().transpose()))
            for kS in range(1, self.NitS+1):
                self.S -= self.gamS * self.gradS() + gamma * self.gradRegS()
                self.S = self.projS()

            gamma = float(self.gamma0) / pow(k+1, self.powerDecay)
            self.Esequence[k] = self.cost()
            self.Csequence[k] = self.criterium()
            k = k+1



        self.Esequence = self.Esequence[0:k-1]
        self.Csequence = self.Csequence[0:k-1]

        if self.verbose:
            print("Iteration, criterium and cost:")
            print k, self.criterium(), self.cost()


            print("Dictionary:")
            print(self.A)


            plt.figure(1)
            plt.subplot(221)
            plt.plot(np.log10(self.Esequence))
            plt.xlabel("Iterations")
            plt.title("Log10 of Cost")

            plt.subplot(222)
            plt.plot(np.log10(self.Csequence))
            plt.xlabel("Iterations")
            plt.title("Log10 of Gradient Norm")

            plt.subplot(223)
            plt.plot(np.transpose(self.X))
            plt.xlabel("Time")
            plt.title("X(t)")

            plt.subplot(224)
            plt.plot(np.transpose(self.S))
            plt.xlabel("Time")
            plt.title("S(t)")

            plt.show()

        return self.criterium(), self.Niteration




class Inverser(GradientOptimiser):

        # Auxiliary function and definitions
        def Thr(self,a,lbda,mu):
            return np.multiply(a,np.maximum(1 - lbda * mu / np.maximum(np.abs(a), 10**-10), 0 ))

        def proxL1Dual(self,a,sigma,mu):
            return a - sigma * self.Thr(a /sigma,1/sigma,mu)

        # Update init object method
        def update(self):
            self.NitA = 0
            self.NitS = 1
            self.muReg = 1
            self.muSFA = 0
            self.muL1 = 0
            self.muTV = 0

        # Gradient
        def gradS(self):
            return 2* self.A.conj().transpose() * ( self.A*self.S-self.X)

        def gradRegS(self):
            return  2* self.muReg * self.S \
                    + 2* self.muSFA * self.S * self.delta

        def projS(self):
            gradDual = self.grad.conj().transpose()
            sigma = 0.5 * 1/np.sqrt(la.norm(gradDual * self.grad))
            tau = sigma
            gradStmp = self.proxL1Dual( self.S + np.multiply(sigma,self.S * self.grad), sigma,self.muTV)
            #gradDual = -self.grad #dual of grad is minus divergence

            return self.Thr(self.S,self.gamS,self.muL1) + self.S - self.muTV * tau *  gradStmp * gradDual



        # Cost function
        def cost(self):
            return la.norm(self.A * self.S - self.X)**2 \
                    + self.muReg * la.norm(self.S)**2 \
                    + self.muSFA * la.norm(self.S * self.grad)**2 \
                    + self.muL1 * la.norm(self.S, ord=1) \
                    + self.muTV * la.norm(self.S * self.grad,ord=1)



class Dictionary(Inverser):

    def update(self):
            self.NitA = 1
            self.NitS = 1

    def gradA(self):
        return 2* (self.A*self.S -self.X) * self.S.conj().transpose()

    def projA(self):
        nA = la.norm(self.A,axis=0)
        return self.A / np.maximum(nA,10**-10)
