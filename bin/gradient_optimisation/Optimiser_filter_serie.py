from bin.gradient_optimisation import Optimiser_filter

__author__ = 'guillaume'

import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt

class SerieOptimiser:
    def  __init__(self,Nunits,A0,X,S0):

        self.Nunits = Nunits
        self.units = []

        self.X = X

        # Iteration parameters
        self.Niterations = 100
        self.epsilon = 0.001
        self.iterationPerStep = 1

        # Regularisation parameters
        self.muSFA = 0
        self.muReg = 1
        self.muL1 = 0
        self.muTV = 0

        self.verbose = 0

        for i in range(0, Nunits):
            f = Of.FilterLearner(A0[i,:],X,S0[i,:])

            f.verbose = 0
            f.Niterations = self.iterationPerStep
            f.epsilon = self.epsilon / Nunits

            self.units.append(f)

    def residual(self,i):
            s = np.matrix( np.zeros(np.shape( self.units[0].S )) )
            for k in range(0,self.Nunits):
                if k<i:
                    s += self.units[k].A *self.X - self.units[k].S
            return s

    def cost(self):
            s= 0
            for k in range(0,self.Nunits):
                s += self.units[k].cost()
            return s

    def criterium(self):
            s= 0
            for k in range(0,self.Nunits):
                s += self.units[k].criterium()
            return s


    def solve(self):
            # Record values over iterations
            self.Esequence = np.zeros((1,self.Niterations))
            self.Csequence = np.zeros((1,self.Niterations))

            # Set parameters to each optimiser in cases of modifications
            for i in range(0, self.Nunits):
                    f = self.units[i]
                    f.muSFA = self.muSFA
                    f.muReg = self.muReg
                    f.muL1 = self.muL1
                    f.muTV = self.muTV

                    f.verbose = 0
                    f.Niterations = self.iterationPerStep
                    f.epsilon = self.epsilon / self.Nunits

            # Solve
            for n in range(0,self.Niterations):
                if  self.criterium < self.epsilon:
                    break
                for i in range(0, self.Nunits):

                    f = self.units[i]
                    f.R = self.residual(i)
                    f.solve()
                    self.Esequence[0,n] += f.cost()
                    self.Csequence[0,n] += f.criterium()

            if self.verbose:
                plt.figure(1)

                plt.subplot(331)
                plt.plot(np.log10(self.Esequence[0,:] + self.epsilon))
                plt.xlabel("Iterations")
                plt.title("Log10 of Cost")

                plt.subplot(332)
                plt.plot(np.log10(self.Csequence[0,:] + self.epsilon))
                plt.xlabel("Iterations")
                plt.title("Log10 of Gradients norm")

                plt.subplot(333)
                plt.plot(np.transpose(self.X))
                plt.xlabel("Time")
                plt.title("X(t)")

                plt.subplot(334)
                plt.plot(np.transpose(self.units[0].S))
                plt.xlabel("Time")
                plt.title("S(t)")

                plt.subplot(337)
                plt.plot(np.transpose(self.units[0].A))
                plt.xlabel("Coefficients")
                plt.title("Filter A0")


                if self.Nunits > 1:
                    plt.subplot(335)
                    plt.plot(np.transpose(self.units[1].S))
                    plt.xlabel("Time")
                    plt.title("S(t)")

                    plt.subplot(338)
                    plt.plot(np.transpose(self.units[1].A))
                    plt.xlabel("Coefficients")
                    plt.title("Filter A1")


                if self.Nunits > 2:
                    plt.subplot(336)
                    plt.plot(np.transpose(self.units[2].S))
                    plt.xlabel("Time")
                    plt.title("S(t)")


                    plt.subplot(339)
                    plt.plot(np.transpose(self.units[2].A))
                    plt.xlabel("Coefficients")
                    plt.title("Filter A2")

                plt.show()

                return 0