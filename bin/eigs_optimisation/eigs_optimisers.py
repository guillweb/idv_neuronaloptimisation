from bin import input_loader as il

__author__ = 'guillaume'

import numpy as np
from brian2 import *


class EigOpt(object):
    def __init__(self,X,Koutput):
        self.X = X
        self.Kout = Koutput

        self.N = len(self.X.T)
        self.n = len(self.X)

    def  computeSFA(self):
        self.computePCA()

        Upca = self.U

        Z_pca = dot(self.R,self.X)

        #Compute gradients
        dZ_pca = Z_pca[:,1:self.N-1] - Z_pca[:,0:self.N-2]

        cov = np.dot(dZ_pca,dZ_pca.T)
        s, U = np.linalg.eig(cov)

        del Z_pca,dZ_pca

        self.R = np.dot(U.T,Upca.T)
        self.Rinv = np.dot(Upca,U)

        Z_tmp = np.dot(self.R,self.X)
        vals = np.std(Z_tmp,axis=1)

        del Z_tmp

        self.R = np.dot(diag(1/vals),self.R)
        self.Rinv= np.dot(self.Rinv,diag(vals))

        Z_sfa = np.dot(self.R,self.X)
        dZ_sfa = Z_sfa[:,1:self.N-1] - Z_sfa[:,0:self.N-2]

        #Save slowness parameters
        self.theta = np.std(dZ_sfa,axis=1)
        self.order = np.argsort(self.theta)

    def computePCA(self):
        cov = np.dot(self.X,self.X.T)
        s, U = np.linalg.eig(cov)
        self.U = U

        #Save sphering matrix
        vals = np.sqrt(np.abs(s) / self.N)
        if (vals < 10**-15).any():
            vals[vals < 10**-15] += 10**-15
            print "Warning: Eigenvalue under 10**-15 in PCA. Shit will happen."
        self.R = np.dot(np.diag(1/vals),U.T)
        self.Rinv = np.dot(U,np.diag(vals))

        self.vals = vals
        self.theta = np.zeros(shape(self.vals))

        order = np.argsort(self.theta)
        self.order = order[::-1]


    def setNonDegenerateOutput(self,epsi):

        ord = np.argsort(self.vals)
        ord = ord[::-1]

        max_val = max(self.vals)
        list = find(self.vals/max_val < epsi)
        if len(list) == 0:
            self.Kout = self.n
        else:
            self.Kout = list[0]

        order = ord[0:self.Kout]
        self.R_proj = self.R[ord[0:self.Kout],:]
        self.theta_proj = self.theta[ord[0:self.Kout]]
        self.vals_proj = self.vals[ord[0:self.Kout]]


    def getOutput(self):

        order = self.order[0:self.Kout]
        self.R_proj = self.R[order[0:self.Kout],:]
        self.theta_proj = self.theta[order[0:self.Kout]]
        self.vals_proj = self.vals[order[0:self.Kout]]

        self.output = np.dot(self.R_proj,self.X)

        return self.output