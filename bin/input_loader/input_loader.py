from bin.input_loader import recursive_filters as RF

__author__ = 'guillaume'

from brian2 import *
from numpy import *
import scipy.linalg as la
import scipy as sc
import scipy.io.wavfile as wave
import scipy.signal as sig
from scipy.signal import butter, lfilter
import bin.eigs_optimisation.eigs_optimisers as eo



class Input(object):
    def  __init__(self,tmin,tmax,N):
        self.tmin = tmin
        self.tmax = tmax
        self.N = N

        self.FS = N/ (tmax-tmin)
        self.dt = 1./self.FS

        self.T = array(range(0,N)) * self.dt
        self.x = zeros((N,))

    # ----------------
    # Handle choices
    # ----------------

    def repeatInput(self,repeat):
        r = (repeat-1)
        self.tmax +=  + r * (self.tmax - self.tmin)
        self.N += r * self.N
        self.T = array(range(0,self.N)) * self.dt

        xrep = self.x
        for i in range(0,r):
            xrep = hstack((xrep,self.x))
        self.x = xrep

    @staticmethod
    def buildInput(tmin,tmax,N,inputType,inputParam= [],expType= [],expParam = [],repeat=1,fWiskott=10 * Hz):
        " Define the input from choices."

        if not(inputParam):
            inputParam = [[],[],[],[]]
            inputFileName= "../../data/yourmom.wav"
            inputParam[2] = [inputFileName,0 * ms]
            # Cosin like inputs
            f0,a0,offset0 = [1000 * Hz,1,tmin] # frequency, amplitude, offset
            f1,a1,offset1 = [1100 * Hz,1,tmin]
            ard = 0.1
            inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
            inputParam[0] = [f0,a0,offset0,ard]
            # For Wiskott classic
            inputParam[3] = [fWiskott]

        if not(expParam):
            expParam = [[],[],[]]
            # Input expansion into high dimensional space
            FS = N/(tmax - tmin)

            NfiltersOrDelay = 200

            TauDelay = 1/ FS * 2
            TauInputFilters = 10* 1/FS           # Filter time constant constant
            TauMin = TauInputFilters/2
            TauMax = TauInputFilters*2

            Sphering = True
            Degenerate = False

            expParam[1] = [NfiltersOrDelay,TauDelay,Sphering,Degenerate]
            expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering,Degenerate]

        params = inputParam[inputType]

        inp = Input(tmin,tmax,N)

        # Wave File
        if inputType == 2:

            inputFileName = params[0]
            offset = tmin

            inp.addWaveFromFile(inputFileName,offset)
            inp.repeatInput(repeat)
            inp.buildExpansion(expType,expParam)
        # 2 Cosin
        elif inputType == 1:

            f0,a0,of0,f1,a1,of1 = params
            inp.addSin(f0,a0,of0)
            inp.addSin(f1,a1,of1)
            inp.repeatInput(repeat)
            inp.buildExpansion(expType,expParam)

        # Wiskott classic 2-input
        elif inputType == 3:
            f0 = params[0]
            inp.set_wiskott_classic(f0)
            inp.sphereInputs()
        # Noised sinus
        else:

            f0,a0,of0,ard = params

            inp.addCosin(f0,a0,of0)
            inp.addNoise(ard)
            inp.repeatInput(repeat)
            inp.buildExpansion(expType,expParam)

        return inp


    def buildExpansion(self,expType,expParam):

        self.x /= norm(self.x)

        param = expParam[expType]
        degenerate = False
        sphering = False

        if expType == 1:
            NfiltersOrDelay,TauDelay,sphering,degenerate = param
            self.expandWithDelay(NfiltersOrDelay,TauDelay)
            self.whitenInputs()
        elif expType == 2:
            NfiltersOrDelay,TauMin,TauMax,sphering,degenerate = param
            self.expandWithRecursiveFilterConvolution(TauMin,TauMax,NfiltersOrDelay)
            self.whitenInputs()

        if sphering:
             self.sphereInputs(degenerate)

        self.n = np.shape(self.X)[0]


    # ----------------
    # BASIC INPUTS
    # ----------------

    # add cosin to input
    def addCosin(self,f0,a,phi):
        self.x += a * cos(2* pi * f0 * (self.T + phi) )

    def addSin(self,f0,a,phi):
        self.x += a * sin(2* pi * f0 * (self.T + phi) )

    # add noise into input
    def addNoise(self,a):
        self.x += a * random.randn(1,self.N).ravel()

    # add noise into input
    def addData(self,a,offsetTime):
        offsetN = int(offsetTime / self.dt)

        Nstop = min(offsetN+self.N,len(a))
        self.x[0:Nstop-offsetN] += a.ravel()[offsetN:Nstop]


    # ----------------------------------
    # Wiskott classic for SFA validation
    # ----------------------------------

    def set_wiskott_classic(self,f0,frequency_factor= 11,cos_coeff=1,noise_coeff=0.):

        noise = np.random.randn(self.N)
        cosinus = np.cos(2* pi * f0 * frequency_factor * (self.T) )
        sinus = np.sin(2* pi * f0 * (self.T) )

        noise /= std(noise)
        cosinus /= std(cosinus)
        sinus /= std(sinus)

        noise *= noise_coeff
        cosinus *= cos_coeff

        x1 = sinus +  (noise + cosinus)**2
        x2 = noise + cosinus

        self.x = x1
        self.X= vstack((x1,x2))
        self.n = 2

        self.whitenInputs()
        self.quadraticExpansion()
        self.whitenInputs()

    def set_wiskott_modified(self,f0,frequency_factor= 11,fast_weight=1):
        x1 = cos(pi/2*fast_weight) * sin(2* pi * f0 * (self.T) ) +  sin(pi/2*fast_weight) * cos(2* pi * f0 * frequency_factor * (self.T) )**2
        x2 = cos(2* pi * f0 * frequency_factor * (self.T) )

        self.x = x1
        self.X= vstack((x1,x2))
        self.n = 2

        self.whitenInputs()
        self.quadraticExpansion()
        self.whitenInputs()
    # ----------------
    # AUDIO INPUTS
    # ----------------

    # Use audio input
    def addWaveFromFile(self,fileName,offset):
        fswav,wav = wave.read(fileName)

        wav = wav - mean(wav)
        wav = wav/ np.max(wav)

        #wav = sig.resample(wav,len(wav)*float(self.FS)/fswav)
        print "WARNING: audio resampling disabled"

        if len(shape(wav)) == 2:
            wav = wav[:,0]
        offsetN = int(offset /self.dt)
        Nstop = min(offsetN+self.N,len(wav))

        self.x[0:Nstop-offsetN] += wav[offsetN:Nstop]
        self.x = self.x / norm(self.x)

    # FILTER audio input
    def butter_bandpass(self,lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a

    def butter_lowpass(self,lowcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        b, a = butter(order, low, btype='low')
        return b, a



    def butter_bandpass_filter(self, lowcut, highcut, order=5):
        b, a = self.butter_bandpass(lowcut, highcut, self.FS, order=order)
        self.x = lfilter(b, a, self.x)
        return self.x

    # ----------------
    # INPUT EXPANSIONS
    # ----------------

    # Expand input into a higher dimensional space
    def expandWithDelay(self,Ndelays,tau):

        self.n = Ndelays

        erased_offset = Ndelays * tau
        erased_idx = int(Ndelays * tau / self.dt)
        x = self.x
        N = self.N

        self.x = x[erased_idx:-1]
        self.T = self.T[erased_idx:-1]
        self.N = self.N - erased_idx-1
        self.tmin += erased_offset

        self.X = np.zeros((Ndelays,N-1-erased_idx))
        for iDel in range(0, Ndelays):
            offset = int(iDel * tau / self.dt)
            self.X[iDel,:] = x[erased_idx-offset:N-offset-1]
        return self.X

    def expand_with_delay_sparse(self,Ndelays,tau):

        self.X = scipy.sparse.dok_matrix((Ndelays,self.N))

        for i in arange(Ndelays):
            for t in arange(self.N):
                dt = int(i*tau/self.dt)
                ind = np.mod(t-dt,self.N)
                self.X[i,t] = self.x[ind]

    def expandWithRecursiveFilterConvolution(self,tauMin,tauMax,K):
        filters = RF.recursiveFilters(tauMin,tauMax,K,self.dt)

        self.n = K

        X = zeros((K,self.N))
        for i in range(0,self.N-1):
            X[:,i] = filters.iterateFiltering(self.x[i])

        self.X = X
        return self.X

    def addNoiseToExpanded(self,a):
        self.X += a * random.randn(self.n,self.N)

    # ----------
    # Operations
    # ----------

    def whitenInputs(self):
        if hasattr(self, 'X'):
            self.X = (self.X.T - mean(self.X,axis=1)).T
            sd = np.maximum(std(self.X,axis=1),10 ** -15)
            self.X = (self.X.T / sd).T

        self.x -= mean(self.x)
        self.x /= max(std(self.x),10**-15)

    def set_between_0_and_1(self):
        mX = np.min(self.X,axis=1)
        self.X = (self.X.T - mX).T
        x_max = np.max(self.X)
        self.X /= x_max

    def sphereInputs(self,degenerate=True,epsi=10**-6):
        self.whitenInputs()

        opt = eo.EigOpt(self.X,self.n)
        opt.computePCA()
        if not(degenerate):
            opt.setNonDegenerateOutput(epsi)
        self.X = opt.getOutput()
        self.n = np.shape(self.X)[0]

        return self.X,opt.R_proj

    def quadraticExpansion(self):

        X = self.X
        for i in range(0,self.n):
            for j in range(0,i+1):
                X = vstack((X,np.multiply(X[i,:],X[j,:])))
        self.X = X
        self.n = len(self.X)

    def getTimedArray(self,unit=1.0):
        return TimedArray(self.X * unit,self.dt)