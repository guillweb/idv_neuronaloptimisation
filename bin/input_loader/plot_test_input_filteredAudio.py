from bin.input_loader import input_loader as il

__author__ = 'guillaume'

from brian2 import *
import matplotlib.pyplot as plt


f0 = 2000 * Hz

Tau = 10 * ms
tmin = 0 * second
tmax = 1 * second
N = 10000

inp = il.Input(tmin,tmax,N)
inp.addWaveFromFile("yourmom.wav",0 * ms)
inp.butter_bandpass_filter(f0/2,f0)
inp.expandWithRecursiveFilterConvolution(Tau,Tau*2,5)

plt.figure(1)
plt.subplot(2,1,1)
plt.plot(inp.T,inp.x)
plt.subplot(2,1,2)
plt.plot(inp.T,inp.X.T)
plt.show()

