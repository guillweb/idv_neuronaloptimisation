from bin.input_loader import input_loader as il

__author__ = 'guillaume'

from brian2 import *
import matplotlib.pyplot as plt


Tau = 0.001 * second
tmin = -10 * Tau
tmax = 10 * Tau
N = 1000

inp = il.Input(tmin,tmax,N)
inp.addCosin(1/Tau,1,0 * ms)
inp.expandWithDelay(10,Tau/5)

inp.addNoiseToExpanded(0.1)

plt.figure(1)
plt.plot(inp.X.T)
plt.show()

