__author__ = 'guillaume'

import numpy as np
import matplotlib.pyplot as plt

class recursiveFilters:

    def __init__(self,tauMin,tauMax,K,dt,n=1):
        tauMin = float(tauMin)
        tauMax = float(tauMax)
        dt = float(dt)

        # Define parameters
        self.gamma = np.power(tauMax / tauMin,1/float(K))
        self.scales = np.power(self.gamma,range(0,K)) * tauMin

        # Define usefull ints
        self.K = K
        self.n = n
        self.dt = dt

        # Define Means without unity
        self.means = - 0.5 + 0.5 * np.sqrt(1 + 4 * self.scales/dt)

        self.filters = np.zeros((n,self.K))


    def iterateFiltering(self,f):
        matf = np.dot(np.array(f).reshape(self.n,1),np.ones((1,self.K)))
        df = matf - self.filters
        self.filters += np.dot(df,np.diag(1/(1 + self.means)))
        return self.filters


    # Compute the coefficients needed to get the derivative of order ord at time scale scale.
    def d(self,ord,scale):

        if(scale < 0):
            raise Exception('Negative scale requested.')

        if(ord > scale ):
            raise Exception('Order bigger than scale, negative scale will raise.')


        M = - np.diag( 1 /self.means,0) + np.diag(1/self.means[1:self.K],1)
        v = np.zeros((self.K,1))
        v[scale] = 1

        for i in range(0,ord):
            v = np.dot(M,v)

        return v
    def getDerivativeFilter(self,ord,scale,T,offset=0):

        N = int( T / self.dt)
        offsetN = int(offset/self.dt)

        vec = self.d(ord,scale)
        displ = np.zeros(N)

        for i in range(0,offsetN):
            self.iterateFiltering(0)
            displ[i] = np.dot(self.filters,vec)

        self.iterateFiltering(1)
        displ[offsetN] = np.dot(self.filters,vec)

        for i in range(offsetN+1,N-1):
            self.iterateFiltering(0)
            displ[i] = np.dot(self.filters,vec)

        return displ

    def showDerivativeFilter(self,ord,scale,T):
        displ = self.getDerivativeFilter(ord,scale,T)

        plt.figure(1)
        plt.plot(displ)
        plt.show()
        return displ

    def normCheck(self,scale,T):
        N = int( T / self.dt)
        vec = self.d(0,scale)
        displ = np.zeros(N)

        self.iterateFiltering(1)
        displ[0] = np.dot(self.filters,vec)

        for i in range(1,N-1):
            self.iterateFiltering(0)
            displ[i] = np.dot(self.filters,vec)

        return sum(displ)