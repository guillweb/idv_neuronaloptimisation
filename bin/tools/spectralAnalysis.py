__author__ = 'guillaume'

from numpy import sin, linspace, pi
import numpy as np
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import arange
from scipy.fftpack import fft
from scipy.fftpack import fftfreq

def getSpectrum(y,Fs,pad_to=-1):
     """
     Plots a Single-Sided Amplitude Spectrum of y(t)
     """
     if pad_to < len(y):
        n = len(y) # length of the signal
     else:
        n = pad_to
     k = np.array(arange(n))
     T = n/float(Fs)

     frq = fftfreq(n,1/Fs) # two sides frequency range
     Y = fft(y,n) / float(n) # fft computing and normalization


     rg = range(1,n/2-1)
     frq = frq[rg] # one side frequency range

     Y = Y[rg]

     pY = abs(Y)**2

     return frq,pY

def getLogLogSpectrum(y,Fs,fmin,points):

     frq,spc = getSpectrum(y,Fs)


     logFrq = np.log10(frq)
     logSpc = np.log10(spc)

     FrStep = 2**(np.log2(max(frq)/frq[1])/ float(points))

     expIndicesFloat = np.floor(FrStep ** range(0,points))+1
     expIndicesInt = np.int_(expIndicesFloat)

     avBig = np.where(expIndicesInt < len(logFrq)-1)          # Freq small than FS/2
     expIndicesInt = expIndicesInt[avBig]

     avSmall = np.where(np.float_(frq[expIndicesInt]) >  np.float_(fmin))    # Freq bigger than Fmin
     expIndicesInt = expIndicesInt[avSmall]

     logFrqToPrint = logFrq[expIndicesInt]
     logSpcToPrint = logSpc[expIndicesInt]

     return logFrqToPrint,logSpcToPrint
