__author__ = 'guillaume'

from numpy import *
from scipy import signal

def logSpace(amin,amax,K):
    flK = float(K)
    ra = (amax/amin)**(1/flK)

    return amin * ra**array(range(0,K))

def spike_times_to_train(spk_times,dt,N):
    train = zeros(N,dtype=bool)
    for spk in spk_times:
        t_idx = int(spk / dt)
        train[t_idx] = True

    return train

def get_rate(train,tau_omega,dt):
    K = int(5 * tau_omega / dt)
    t_filt = arange(K) * dt
    filt = t_filt / tau_omega**2 * exp(- t_filt / tau_omega)

    rates = signal.fftconvolve(train,filt)
    return rates[0:train.size]