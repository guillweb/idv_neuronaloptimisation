__author__ = 'guillaume'


from scipy.io.wavfile import write,read
import scipy.signal as sig
from numpy import max


def resample(file_path,suffix,FS_out):

    file_path_in = file_path + suffix
    file_path_out = file_path + '_%d' % FS_out + '.wav'

    FS_in,wav = read(file_path_in)
    wav = sig.resample(wav,len(wav)*float(FS_out)/FS_in)
    wav /= max(wav)
    write(file_path_out,FS_out,wav)
