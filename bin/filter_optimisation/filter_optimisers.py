from bin import input_loader as il

__author__ = 'guillaume'

import numpy as np
from brian2 import *
import numpy.linalg as la
import filter_builder as tr
import numpy.random as rd


class FilterOpt(object):


    # --------------
    # Construction
    #---------------

    def __init__(self,inputObj,filterObj,optType=0,optParam=[[10**4,0.9,10**2],[10**4,10**2]],renormType=0,w_0=[]):

        # Temporal series and filter to optimise
        self.input = inputObj
        self.Omega = filterObj

        # params
        self.Kout = 1
        self.renormType = renormType

        # Init
        self.computeKernel()

        if np.shape(w_0) == (self.input.n,self.Kout):
            self.W = w_0.copy()
            self.renorm()
        else:
            self.randomInit()

        # Compute kernel before to save time

        # Initialise solution storage
        self.Wbest = self.W
        self.minCost = self.get_objective(self.W)
        self.minIteration = 0

        # Choice parameters
        self.optType = optType
        self.optParam = optParam[optType]



    def randomInit(self):
        self.W = rd.randn(self.input.n,self.Kout)
        self.renorm()

    # ----------
    # Operations
    #-----------

    def computeKernel(self):
        """
        Compute the kernel: X Omega X^T to save later computation time.
        """

        XOm = self.Omega.fourier_convolveSignal(self.input.X)   # sparse matrix multiplication
        gT = XOm.dot(self.input.X.T)      # sparse matrix multiplication

        self.kernel = gT / float(self.input.N)
        self.symKernel = gT + gT.transpose()
        self.varKernel = dot(self.input.X,self.input.X.T) / float(self.input.N)

    def getGrad(self,w):
        """
        Compute gradient of the from: (X Omega X^T + X Omega^T X^T) w
        """
        g = self.kernel.dot(w)  # sparse matrix multiplication

        return g

    def get_objective(self,w):
        # Objective of the form: 0.5 * w^T W Omega X^T w = w^T grad(w)
        return  0.5 * dot(w.T,self.kernel.dot(w))

    def renorm(self):
        type = self.renormType
        if type == 0:
            self.W /= norm(self.W)
        elif type == 1:
            stdS = abs(dot(dot(self.W.T,self.varKernel),self.W)) # Normalise so that
            self.W /= sqrt(stdS)

    def record(self,i):
        if mod(i,self.recordEvery) == 0:self.Wsequence[:,i/self.recordEvery] = self.W[:,0]
        if abs(self.get_objective(self.W)) < abs(self.minCost):
            self.Wbest = self.W
            self.minCost = self.get_objective(self.W)
            self.minIteration = i

    # ------
    # Solve
    #-------

    def solve(self):

        if self.optType == 1:
            Niteration,RecordEvery = self.optParam
            self.computeIterationsFISTA(Niteration,RecordEvery)
        elif self.optType == 0:
            Niteration,alOpt,RecordEvery = self.optParam
            self.computeIterations(Niteration,alOpt,RecordEvery)

    def iterate(self,alOptNormed):

        dw = alOptNormed * self.getGrad(self.W)
        self.epsi = max(np.abs(dw))
        self.W += alOptNormed * self.getGrad(self.W)
        self.renorm()

    def iterateFISTA(self,muFista,alOptNormed):

        # Go forward
        dw = alOptNormed * self.getGrad(self.W)
        self.W += dw
        self.epsi = max(np.abs(dw))
        self.renorm()

        # Update storage of past optimal descents
        self.Wback2 = self.Wback1.copy()
        self.Wback1 = self.W.copy()

        # Go backward
        self.W += muFista * (self.Wback1 - self.Wback2)
        self.renorm()




    # Mother function for solving
    def computeIterations(self,ItNumber,alOpt,recordEvery):

        self.ItNumber = ItNumber
        self.alOpt = alOpt
        self.recordEvery = recordEvery


        # Normalise by the theoretical convergence bound: || X Omega X^T ||
        alOptNormed = alOpt / norm(self.kernel)

        # init storage
        self.Wsequence = zeros((self.input.n,ItNumber/recordEvery+1))
        self.Wback1 = self.W.copy()


        for i in range(0,ItNumber):
            self.record(i)
            self.iterate(alOptNormed)
            #if i>0 and self.epsi < 10**-20:
            #    print "Converged. iteration: %s" % [i,self.epsi]
            #    break





    # Mother function for solving
    def computeIterationsFISTA(self,ItNumber,recordEvery):

        self.ItNumber = ItNumber
        self.recordEvery = recordEvery

        # Normalise by the theoretical convergence bound: X Omega X^T
        alOptNormed = 1 / norm(self.kernel)

        # init storage
        self.Wsequence = zeros((self.input.n,ItNumber/recordEvery+1))

        tFista = 1
        self.Wback1 = self.W.copy()
        self.Wback2 = self.W.copy()

        for i in range(0,ItNumber):
            tOld = tFista
            tFista = 0.5 + sqrt(0.25 + 0.5*tFista)
            muFista = (tOld- 1)/tFista

            self.record(i)
            self.iterateFISTA(muFista,alOptNormed)




    # ------
    # Output
    #-------

    def get_objective_sequence(self):

        Nrec = len(self.Wsequence.T)-1
        costSequence = zeros((Nrec,))


        for i in range(0,Nrec):
            w = self.Wsequence[:,i]
            costSequence[i] = self.get_objective(w)

        lowBounded = np.maximum(abs(costSequence),10**-20)
        logCostSeq = np.log10( lowBounded )

        return costSequence,logCostSeq


    def getOutput(self):
        out = dot(self.W.T,self.input.X)
        return out


    def getBestOutput(self):
        out = dot(self.Wbest.T,self.input.X)
        return out




