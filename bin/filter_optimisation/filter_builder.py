__author__ = 'guillaume'

from numpy import *
import scipy
import scipy.fftpack as ff
import scipy.linalg as la
import scipy.sparse as sp
from brian2 import *




class FilterBuilder(object):

    # -------------------
    # Object construction
    # -------------------

    def __init__(self,T,TauPeriod):

        # Data parameters over signal space
        self.N = len(T)
        self.dt = (T[self.N-1] - T[0])/(self.N -1)
        self.Tdata = T

        # Data parameters over filter space
        self.NPeriod = int(np.round(TauPeriod / self.dt))+1

        if self.NPeriod > self.N:
            self.NPeriod = self.N
            self.zeroIndex = self.NPeriod/2
            self.TauPeriod = (T[self.N-1] - T[0])
            self.T = self.Tdata
            print "Warning: Period of filters longer than data duration. Shortened to data duration."
        else:
            self.zeroIndex = self.NPeriod/2
            self.TauPeriod = TauPeriod
            self.T = range(0,self.NPeriod)*self.dt
            self.T -= self.T[self.zeroIndex]

    def newFilter(self):
        F = Filter(self.Tdata,self.TauPeriod)
        return F



    # --------------------------
    # Filter - matrix Conversion
    # ---------------------------

    def filterToMatrix(self,f):

        fil = f.reshape(self.NPeriod,1)
        fil = np.array(fil, dtype=np.float32)
        data = fil.repeat(self.N,axis=1)


        Nstart = -self.zeroIndex
        Nstop = Nstart + self.NPeriod
        M = sp.spdiags(data,range(Nstart,Nstop),self.N,self.N,)

        return M

    def matrixToFilter(self,F):
        f0 = F[:,self.zeroIndex]
        f0 = f0[0:self.NPeriod]
        return ravel(f0[::-1].todense())




    # -------------------------
    # Basic filter construction
    # -------------------------

    def getGaussFilter(self,Tau):
        al = self.dt / Tau
        g = al * exp(-self.T**2/(2*Tau**2))

        F = self.newFilter()
        F.setFilter(g)
        return F

    def getRoughFilter(self,Tau):
        al = self.dt / Tau
        g = al * (self.T >= 0) * exp(-self.T/Tau)

        F = self.newFilter()
        F.setFilter(g)
        return F

    def getExpFilter(self,Tau):
        al = self.dt / Tau
        g = al * (self.T >= 0) * exp(-self.T/Tau)

        F = self.newFilter()
        F.setFilter(g)
        return F


    def getExpTransposedFilter(self,Tau):
        F = self.getExpFilter(Tau)
        F.transpose()
        return F


    # Define Alpha filter as g \ast g
    def getAlphaFilter(self,Tau,set_matrix=True):
        al = (self.T >= 0) * self.T/Tau**2 * exp(-self.T/Tau) * self.dt
        F = self.newFilter()
        F.setFilter(al,set_matrix)

        return F


    def getDerivativeFilter(self,Tau):
        g = self.getExpFilter(Tau).getFilter()
        gT = self.getExpTransposedFilter(Tau).getFilter()

        d1tau =  0.5 * (gT - g)

        F = self.newFilter()
        F.setFilter(-d1tau)
        return F

    def getSigmaFilter(self,Tau):
        al = self.dt / Tau
        sig =  0.5 * np.exp( - np.abs(self.T)/Tau ) * al

        F = self.newFilter()
        F.setFilter(sig)
        return F

    def getDiracFilter(self):

        dirac = zeros(shape(self.T))
        #dirac[(2* self.zeroIndex +1)/2,] = 0.5 * self.N
        dirac[self.zeroIndex] = self.N

        #f = self.matrixToFilter(sp.identify(self.N))

        F = self.newFilter()
        F.setFilter(dirac)
        return F

    def getSecondDerivativeFilter(self,Tau):

        DD = self.getDerivativeFilter(Tau)
        D = self.getDMatrix(1)
        DD.matrixDot(D)
        #sig = self.getSigmaFilter(Tau).getFilter()
        #dirac = self.getDiracFilter().getFilter()


        #F = self.newFilter()
        #F.setFilter(2*dirac - sig)
        return DD


    def getDerivativeSquaredFilter(self,Tau,set_matrix=True):
        al = self.dt / Tau
        lin =  1- abs(self.T) /Tau
        sig = 0.5 * np.exp( - np.abs(self.T)/Tau ) * al
        f = 0.5 * multiply(lin,sig)

        F = self.newFilter()
        f /= sum(np.abs(f))
        F.setFilter(f,set_matrix)
        #DD = self.getDerivativeFilter(Tau)
        #DD.matrixDot(DD.transpose().matrix)
        return F

    def getSFAFilter(self,Tau,set_matrix=True):
        F = self.getDerivativeSquaredFilter(Tau,set_matrix)
        F.setFilter(- F.getFilter(),set_matrix=set_matrix)

        return F

    def getDMatrix(self,r):
        D = self.N * 0.5 *(sp.eye(self.N,self.N,1)  - sp.eye(self.N,self.N,-1))
        M = D

        for i in range(1,r):
            M = M.dot(D)

        return M

    def get_D_filter(self,r):

        F = self.newFilter()
        F.setMatrix(self.getDMatrix(int(r)))

        return F



class Filter(FilterBuilder):

    def setFilter(self,f,set_matrix=True):
        self.filter = f
        if set_matrix:
            self.matrix = self.filterToMatrix(f)
        else:
            self.matrix = np.zeros((0,0))

    def setMatrix(self,M):
        fil = self.matrixToFilter(M)
        self.setFilter(fil)

    def getFilter(self):
        return ravel(self.filter)


    # ----------
    # Operations
    # ----------

    # Matrix operators
    def transpose(self):
        M = self.matrix.transpose()
        self.setMatrix(M)
        return self

    def inverse(self):
        M = sp.linalg.inv(self.matrix)
        self.setMatrix(M)
        return self

    @staticmethod
    def compute_fourier_inverse(f,epsi=10**-3):
        ft = ff.fft(f)
        ftc = ft.conj()
        inv = ftc / np.maximum(ft * ftc, epsi)
        return ff.ifft( inv )



    def fourier_inverse(self):
        fil = self.compute_fourier_inverse(self.getFilter())

        self.setFilter(fil)
        return self

    # Multiplication / Convolution
    def matrixDot(self,F):
        M = self.matrix.dot(F)
        self.setMatrix(M)
        return self

    def convolve(self,f):
        F = self.filterToMatrix(f)
        M = self.matrixDot(F)
        self.setMatrix(M)
        return self

    def convolveSignal(self,x):
        return self.matrix.transpose().dot(x.T).transpose()

    def fourier_convolveSignal(self,x):
        shp = shape(x)

        X = np.zeros(shp)
        if len(shp) == 2:
            for Ni in range(0,shp[0]):
                X[Ni,:] = scipy.signal.fftconvolve(x[Ni,:],self.getFilter(),'same')
        else:
            X = scipy.signal.fftconvolve(x,self.getFilter(),'same')
        return X