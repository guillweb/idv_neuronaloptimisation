__author__ = 'guillaume'

from numpy import *
from bin.eigs_optimisation.eigs_optimisers import EigOpt
import bin.input_loader.input_loader as il
from brian2 import *
import matplotlib.pyplot as plt



f0 = 2000 * Hz

Tau = 10 * ms
tmin = 0 * second
tmax = 1 * second
N = 10000
dt = (tmax - tmin)/N

inp = il.Input(tmin,tmax,N)
inp.addWaveFromFile("data/yourmom.wav",0 * ms)
inp.butter_bandpass_filter(f0/2,f0)
inp.expandWithRecursiveFilterConvolution(dt,Tau*10,20)
inp.whitenInputs()


S = EigOpt(inp.X,1)
S.computeSFA()
out = S.getOutput()

a = dot(S.R,S.X)
print 'Diagonal of the covariance RX:'
print 1/ float(S.N) * diag(dot(a,a.T))


print 'Variances of indep components before normalisation:'
print S.vals

print 'Slowness:'
print S.theta

plt.figure(1)
plt.plot(out)
plt.show()

