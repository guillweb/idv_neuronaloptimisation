__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import time
"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""
#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

optType = 2         # 2: Wiskott SFA
                    # 1: FISTA
                    # 0: Projected gradient

renormType = 0      # 0: weight,
                    # 1: output variance,
                    # else: none

# Sampling parameters
tmin = 0 * second
tmax = 0.2 * second
FS = 22050 * Hz

# SFA parameters
TauOmega = 0.5 * ms                      # STDP window time constant constant
TauPeriod = TauOmega * 50                # Filer length (Limited for memory error issue)

# Optimisation Param
Niteration = 10**5
RecordEvery = Niteration/1000

# Post-processing Output filtering time constant (To simulate neural integration)
TauOut = 10 * ms
Noutputs = 2


# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "data/dnb.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 400

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = not(optType == 2)

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]
# -----------------------
# Optimisation PARAMETERS
# -----------------------
alOpt = 10**-3
optParam[1] = [Niteration,RecordEvery]
optParam[0] = [Niteration,alOpt,RecordEvery]



# -------
# Solver
# -------

t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam)
t = ti.timeCount(t,'Input setup (s):')

# Create STDP window filter (Omega)
builder = fb.FilterBuilder(inp.T,TauPeriod)
F = builder.getDerivativeSquaredFilter(TauOmega)
t = ti.timeCount(t,'Filter setup (s):')


# Create optimiser object and run optimisation
if optType < 2:
    opt = fo.FilterOpt(inp,F,optType,optParam,renormType)
    opt.solve()

    # Get out output and cost sequence
    CostSequence,logCostSeq = opt.get_objective_sequence()
    output = opt.getOutput().T
    bestOutput = opt.getBestOutput().T

    t = ti.timeCount(t,'Optim (s):')
    whNeg = find(CostSequence < 0.)
    if sum(whNeg) >0:
        print "Warning: Cost gets negative."
        print whNeg
        print CostSequence[whNeg]

else:
    opt = eo.EigOpt(inp.X,Noutputs)
    opt.computeSFA()

    out = opt.getOutput()
    output = out[1,:]
    bestOutput = out[0,:]
    a = dot(opt.R,opt.X)
    print 'Diagonal of the covariance RX:'
    print 1/ float(opt.N) * diag(dot(a,a.T))


    print 'Variances of indep components before normalisation:'
    print opt.vals


    print 'Slowness:'
    print opt.theta

    print 'Order:'
    print opt.order
    logCostSeq = np.log(opt.vals)

    print 'Output:'
    print opt.theta_proj

# -------
# Plots
# ------

plt.figure(1)

# Plot input
plt.subplot(5,2,1)
plt.plot(inp.T,inp.x)

#frq,spc = to.getLogLogSpectrum(inp.x,inp.FS,1 * Hz,10**4)
frq,spc = to.getSpectrum(inp.x,inp.FS)
plt.subplot(5,2,2)
plt.plot(frq,spc)

# Plot optimisation stuff
plt.subplot(5,2,3)
plt.plot(inp.T,inp.X.T)

plt.subplot(5,2,4)
plt.plot(logCostSeq)


# Plot output
plt.subplot(5,2,5)
plt.plot(inp.T,output)

#frq,spc = to.getLogLogSpectrum(output,inp.FS,1 * Hz,10**4)
frq,spc = to.getSpectrum(output,inp.FS)
plt.subplot(5,2,6)
plt.plot(frq,spc)



# Plot output
plt.subplot(5,2,7)
plt.plot(inp.T,bestOutput)

#frq,spc = to.getLogLogSpectrum(output,inp.FS,1 * Hz,10**4)
frq,spc = to.getSpectrum(bestOutput,inp.FS)
plt.subplot(5,2,8)
plt.plot(frq,spc)

outputLowPassed = il.Input(tmin,tmax,N)
outputLowPassed.addData(bestOutput,0 * second)
outputLowPassed.expandWithRecursiveFilterConvolution(TauOut,TauOut,1)
# Plot output
plt.subplot(5,2,9)
plt.plot(inp.T,outputLowPassed.x)

#frq,spc = to.getLogLogSpectrum(bestOutput,inp.FS,1 * Hz,10**4)
frq,spc = to.getSpectrum(outputLowPassed.x,inp.FS)
plt.subplot(5,2,10)
plt.plot(frq,spc)
plt.show()

t = ti.timeCount(t,'Plot (s):')