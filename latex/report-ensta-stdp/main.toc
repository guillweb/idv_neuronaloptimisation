\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Establishment of batch, online and spiking implementation of SFA}{6}
\contentsline {subsection}{\numberline {2.1}Notations}{6}
\contentsline {subsection}{\numberline {2.2}SFA as an unsupervised learning problem}{6}
\contentsline {subsubsection}{\numberline {2.2.1}SFA as an unsupervised learning problem: existing solving algorithms and probabilistic interpretations}{6}
\contentsline {subsubsection}{\numberline {2.2.2}First test are run on a toy example}{6}
\contentsline {subsubsection}{\numberline {2.2.3}Simplification of the problem and batch implementation}{7}
\contentsline {subsection}{\numberline {2.3}Online adaptation of the batch algorithm}{8}
\contentsline {subsection}{\numberline {2.4}Relating STDP to learning rules expressed in terms of neurons spiking rates}{9}
\contentsline {subsubsection}{\numberline {2.4.1}Time scales assumptions}{9}
\contentsline {subsubsection}{\numberline {2.4.2}The spiking neuron model}{9}
\contentsline {subsubsection}{\numberline {2.4.3}STDP formalisation in terms of empirical activity}{10}
\contentsline {subsection}{\numberline {2.5}Giving a literal expression to the STDP window that implement SFA}{10}
\contentsline {subsubsection}{\numberline {2.5.1}A spike counting filter adapted to exhibit the STDP kernels associated to a learning rule expressed with derivatives}{11}
\contentsline {subsubsection}{\numberline {2.5.2}Literal expression and representation of the STDP window}{11}
\contentsline {subsection}{\numberline {2.6}Spiking simulations}{11}
\contentsline {subsection}{\numberline {2.7}Comparison with other STDP windows and dependency to parameters}{12}
\contentsline {subsubsection}{\numberline {2.7.1}Comparison with other STDP windows}{12}
\contentsline {subsection}{\numberline {2.8}Application of SFA to audio processing}{12}
\contentsline {section}{\numberline {3}Results}{13}
\contentsline {subsection}{\numberline {3.1}A hebbian learning rule implements a gradient ascent to solve SFA}{13}
\contentsline {subsection}{\numberline {3.2}STDP can implement SFA on the empirical activities}{13}
\contentsline {subsubsection}{\numberline {3.2.1}Summary of the leaning rules that implement SFA}{13}
\contentsline {subsection}{\numberline {3.3}A projected gradient implementation of SFA is robust}{13}
\contentsline {subsection}{\numberline {3.4}SFA learns pitch and tempo tuned filter in audio recordings}{14}
\contentsline {subsubsection}{\numberline {3.4.1}SFA can learn pitch on a guitar recording}{14}
\contentsline {subsubsection}{\numberline {3.4.2}SFA can learn the tempo on a guitar recording}{17}
\contentsline {subsubsection}{\numberline {3.4.3}Heterogeneous population of neurons implementing SFA exhaustively analyse a music piece}{17}
\contentsline {subsubsection}{\numberline {3.4.4}Extension to speech recording}{19}
\contentsline {subsection}{\numberline {3.5}Realistic adaptation of the algorithm batch, online and then spiking}{22}
\contentsline {section}{\numberline {4}Discussion}{23}
\contentsline {subsection}{\numberline {4.1}Comparison with previous work}{23}
\contentsline {subsubsection}{\numberline {4.1.1}Implementing SFA with spiking neurons: what's new ?}{23}
\contentsline {subsubsection}{\numberline {4.1.2}Generalisation of the resolution scheme and optimisation interpretation of any STDP window}{23}
\contentsline {subsection}{\numberline {4.2}Main assumptions consider in our study}{23}
\contentsline {subsubsection}{\numberline {4.2.1}Linearity of the spiking neuron}{24}
\contentsline {subsubsection}{\numberline {4.2.2}Rate code or spike timing code}{24}
\contentsline {subsubsection}{\numberline {4.2.3}Influence of spike time stochastic dependency}{24}
\contentsline {section}{\numberline {5}Conclusion}{24}
