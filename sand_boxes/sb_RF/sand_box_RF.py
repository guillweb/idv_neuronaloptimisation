import numpy as np
import bin.input_loader.recursive_filters as rf
from brian2 import *
import matplotlib.pyplot as plt


RF = rf.recursiveFilters(1,2,40,1)

order = 2
points = 1000

f1 = RF.getDerivativeFilter(order,16,points,10)
f2 = RF.getDerivativeFilter(order,12,points,10)
f3 = RF.getDerivativeFilter(order,6,points,10)

plt.figure(1)
plt.plot(np.array([f1,f2,f3]).transpose())
plt.show()
