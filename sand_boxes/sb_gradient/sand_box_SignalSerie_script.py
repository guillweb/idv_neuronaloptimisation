import numpy as np
import numpy.linalg as la
from numpy import *
from bin.gradient_optimisation import Optimiser_filter_serie


Npoints = 300
Texp = 1.5
Ndelays = 10
Noutput = 3
f0 = 3
f1 = 50
q1 = 0.1
qrand = 0.05

FS = Npoints/Texp
dt = 1/FS * 2
t = 1/FS * np.matrix(range(1, Npoints+1))


T = t
for iDel in range(1, Ndelays):
    T = np.vstack((T, t - iDel * dt))

A0 = np.matrix(np.random.randn(Noutput,Ndelays))
S0 = np.matrix(np.random.randn(Noutput,Npoints))
X = np.cos(2* np.pi * f0 *T ) \
    + q1 * np.cos(2* np.pi * f1 *T ) \
    + qrand * np.random.randn(Ndelays,Npoints)
    #+ q1 * np.cos(2* np.pi * f1 *T )
    #+ q1 * np.random.randn(Ndelays,Npoints)


seryLearner = Optimiser_filter_serie.SerieOptimiser(Noutput,A0,X,S0)

seryLearner.Niterations = 10
seryLearner.iterationPerStep = 10

seryLearner.gamma0 = 1
seryLearner.muReg = 0
seryLearner.muSFA = 0.01
seryLearner.muTV = 0
seryLearner.muL1 = 0

seryLearner.verbose = True

seryLearner.solve()
