import numpy as np
import numpy.linalg as la
from numpy import *
from bin.gradient_optimisation import Optimiser_filter


Npoints = 1000
Texp = float(1.)
Ndelays = 20
Noutput = 1
f0 = float(15)
f1 = float(0)
q1 = 2
qrand = 0.05

FS = Npoints/Texp
dt = 1/FS

t = 1/FS * np.matrix(range(1, Npoints+1))


T = t
for iDel in range(1, Ndelays):
    T = np.vstack((T, t - iDel * dt))

A0 = np.matrix(np.random.randn(Noutput,Ndelays))
S0 = np.matrix(np.random.randn(Noutput,Npoints))
X = np.cos(2* np.pi * f0 *T )
    #+ q1 * np.sin(2* np.pi * f1 *T ) \
    #+ qrand * np.random.randn(Ndelays,Npoints)
    #+ q1 * np.cos(2* np.pi * f1 *T )
    #+ q1 * np.random.randn(Ndelays,Npoints)


fLearner = Optimiser_filter.StochFilterLearner(A0,X,S0)

fLearner.Niterations = 500
fLearner.powerDecay = 0

fLearner.Tw = 200

fLearner.gamma0 = 1
fLearner.muReg = 0
fLearner.muSFA = 0
fLearner.muTV = 0
fLearner.muL1 = 0
fLearner.nuICA = 0

fLearner.verbose = True

fLearner.solve()
