import numpy as np
import numpy.linalg as la
from numpy import *
from bin.gradient_optimisation import Optimiser_filter

q1 = 2
qrand = 0.05



Npoints = 100
Texp = float(.2)
Ndelays = 4
Noutput = 2
f0 = 31.
f1 = 77.
FS = Npoints/Texp
dt = 1/FS

t = 1/FS * np.matrix(range(1, Npoints+1))


T = t
for iDel in range(1, Ndelays):
    T = np.vstack((T, t - iDel * dt))

X = np.cos(2* np.pi * f0 *T ) \
    + q1 * np.sin(2* np.pi * f1 *T ) \
    + qrand * np.random.randn(Ndelays,Npoints)
    #+ q1 * np.cos(2* np.pi * f1 *T )
    #+ q1 * np.random.randn(Ndelays,Npoints)
X = Optimiser_filter.NonLinearExpanded(X).addQuadratic()

A0 = np.matrix(np.random.randn(Noutput,np.shape(X)[0]))
S0 = np.matrix(np.random.randn(Noutput,Npoints))

fLearner = Optimiser_filter.StochFilterLearner(A0,X,S0)

fLearner.Niterations = 1000
fLearner.powerDecay = 0

fLearner.Tw = Npoints

fLearner.gamma0 = 1
fLearner.muReg = 0
fLearner.muSFA = 0.5
fLearner.muTV = 0
fLearner.muL1 = 0
fLearner.muRegA = 0
fLearner.nuICA = 5

fLearner.NitA = 50
fLearner.NitS = 50
fLearner.gamA = 0.01
fLearner.gamS = 0.1

fLearner.verbose = True

fLearner.solve()
