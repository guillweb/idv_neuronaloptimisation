import numpy as np
import numpy.linalg as la
from numpy import *
from bin.gradient_optimisation import Optimiser_filter


Npoints = 1000
Nspike = 20

rds = np.floor(Npoints * np.random.rand(1,Nspike)).astype(int)
rdsAmp = np.random.randn(1,Nspike)


A0 = np.matrix(np.ones((1,1)))
S0 = np.matrix(np.random.randn(1,Npoints))
X = np.matrix(np.zeros((1,Npoints)) )

X[0,rds] = rdsAmp
X = cumsum(X)
    #+ q1 * np.cos(2* np.pi * f1 *t)


fLearner = Optimiser_filter.FilterLearner(A0,X,S0)

fLearner.Niterations = 200
fLearner.powerDecay = 0
fLearner.muReg = 0
fLearner.muSFA = 0
fLearner.muTV = 22
fLearner.muL1 = 0

fLearner.verbose = True

fLearner.solve()
