import numpy as np
import numpy.linalg as la
from bin.gradient_optimisation import Optimiser_filter


A0 = np.matrix(np.random.randn(2,4));
S0 = np.matrix(np.zeros((4,50)));
X = np.matrix(np.random.randn(2,50));


fLearner = Optimiser_filter.FilterLearner(A0,X,S0)

fLearner.Niterations = 300
fLearner.powerDecay = 0.5
fLearner.muReg = 1
fLearner.muSFA = 3

fLearner.verbose = True

fLearner.solve()