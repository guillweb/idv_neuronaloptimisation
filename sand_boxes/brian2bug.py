from brian2 import *
from numpy import *


tau_omega = 10 * ms

Dt = 0.1 * ms
X_in = np.sin(arange(10**4)*Dt*Hz)

ta0 = TimedArray(X_in,Dt)

G_pre = NeuronGroup(1, '''  x_th = ta0(t)  : 1
                            d x_exp /dt = (x_th - x_exp ) / tau_omega : 1
                            d x_alpha /dt = (x_exp - x_alpha) / tau_omega : 1

                            x_al_old : 1
                            Dx_al : 1
                            D2x_al : 1''')
mon_pre = StateMonitor(G_pre, ['x_th','x_exp'], record=0)
run(0.1 * second)

print np.max(np.abs(mon_pre.x_exp),axis=1)
print np.max(np.abs(mon_pre.x_th),axis=1)
