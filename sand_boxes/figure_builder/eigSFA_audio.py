__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx

from scipy.interpolate import spline

import os

import bin.tools.general as genTools
"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""
#Input Type
inputType = 2       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

# Sampling parameters
tmin = .0 * second
tmax = 5. * second
FS = 11025 * Hz

Noutput = 1

# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/countryguit_11025.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]

# --------------------
# EXPANSION PARAMETERS
# --------------------

# Input expansion into high dimensional space
NfiltersOrDelay = 256

TauDelay = 1/ FS


Sphering = True
Degenerate = False

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering,Degenerate]
#expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering,Degenerate]


# -------
# Solver
# -------
print [tmin,tmax]
print FS
print NfiltersOrDelay
t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam)
x_in = inp.x
t = ti.timeCount(t,'Input setup (s):')


opt = eo.EigOpt(inp.X,Noutput)
opt.computeSFA()
OUT = opt.getOutput()

slowness = opt.theta_proj
Weights = opt.R_proj
freqs = np.zeros((Noutput,))

for i_out in range(0,Noutput):
    output = OUT[i_out,:].ravel()

    frq,spc = to.getSpectrum(output,FS)
    freqs[i_out] = frq[np.argmax(spc)]


print "Slowness:"
print slowness

print "Frequencies:"
print freqs


matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')


for i_out in range(0,Noutput):
    plt.figure()

    output = OUT[i_out,:].ravel()
    plt.plot(inp.T,x_in,color='gray',linewidth=2.0)
    plt.plot(inp.T,output,color='blue',linewidth=2.0)

    plt.locator_params(axis='x', nbins = 4)
    plt.locator_params(axis='y', nbins = 4)

    ylabel(r'$s_{0}$'.format(i_out))

    #xlim(0.05,0.20)
    #ylim(-0.07,0.07)

    xlabel('t (s)')
    tight_layout()
    savefig('../../figures/audio_eig_wav_{0}.pdf'.format(i_out))

show()

for i_out in range(0,Noutput):
    output = OUT[i_out,:].ravel()

    plt.figure()



    frq,spc = to.getSpectrum(x_in,FS)
    fstart = find(frq > 1)[0]
    fend = find(frq < 3000)[-1]

    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='gray',linewidth=2.0)

    frq,spc = to.getSpectrum(output,FS)
    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='blue',linewidth=2.0)

    xlim([50,3000])
    #ylim([10**-9,10**-4])

    ylabel(r'$| \mathcal{F} (s_%d) |^2$' % (i_out))
    xlabel('f (Hz)')
    tight_layout()
    savefig('../../figures/audio_eig_spc_{0}.pdf'.format(i_out))
show()


for i_out in range(0,Noutput):
    weights = Weights[i_out,:].ravel()

    plt.figure()
    plt.plot(range(0,NfiltersOrDelay)*TauDelay,weights,color='green',linewidth=2.0)

    ylabel('Weights')
    xlabel('Delay (s)')
    tight_layout()
    savefig('../../figures/audio_eig_weights_{0}.pdf'.format(i_out))
show()






TauPeriodDraw =  max(TauOmegaArray) * 14
dt_draw =  min(TauOmegaArray) /5

Tdraw = np.array(range(0,int(TauPeriodDraw/dt_draw))) * dt_draw
Tdraw = Tdraw - Tdraw[-1]/2.

for iTau in range(0,len(TauOmegaArray)):
    tau = TauOmegaArray[iTau]
    color = scalarMap.to_rgba(np.log(TauOmegaArray[iTau]/ms))
    drawer = fb.FilterBuilder(Tdraw,TauPeriodDraw)
    plt.figure()

    plt.plot(Tdraw,drawer.getDerivativeSquaredFilter(TauOmegaArray[iTau]).getFilter(),color='green',linewidth=2.0)

    plt.ylim((-0.001,0.01))
    plt.xlim((-TauPeriodDraw/2,TauPeriodDraw/2))

    xlabel('t (s)')
    ylabel(r'$\tau$ = {0}'.format(tau))

    tight_layout()
    savefig('../../figures/audio_win_{0}.pdf'.format(tau))

show()

plt.figure()
plt.plot(inp.T,inp.x)
xlabel('t (s)')
savefig('../../figures/audio_source.pdf')


plt.figure()
frq,spc = to.getSpectrum(inp.x,FS)
plt.loglog(frq,spc)
ylabel('Spectrum')
xlabel('f (Hz)')
savefig('../../figures/audio_source_spc.pdf')
show()