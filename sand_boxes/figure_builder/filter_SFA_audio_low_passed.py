__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx

from scipy.interpolate import spline

import os

import bin.tools.general as genTools

t = time.clock()


# Sampling parameters
tmin = .0 * second
tmax = 5. * second
FS = 11025 * Hz

tau_omega = 0.5 * ms

Noutput = 1
N_iteration = 10 **4
al_opt = 0.9
record_every = N_iteration / 200

# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputFileName= "../../data/countryguit_11025.wav"
N_delays = 200
tau_delay = tau_omega #1/ FS

# -------
# Solver
# -------
print  [[tmin,tmax],FS,N_delays]

# Define input
offset = tmin
inp = il.Input(tmin,tmax,N)
inp.addWaveFromFile(inputFileName,offset)
inp.whitenInputs()
inp.expandWithDelay(N_delays,tau_delay)
X_in,R_proj = inp.sphereInputs(False)

builder = fb.Filter(inp.T,tau_omega *20)
F = builder.getSFAFilter(tau_omega)

t = ti.timeCount(t,'Input setup (s):')


opt = fo.FilterOpt(inp,F,optParam=[[N_iteration,al_opt,record_every]])
opt.solve()
output = opt.getOutput().ravel()
cost_sequence,log_cost_seq = opt.get_objective_sequence()

figure()
plt.plot(log_cost_seq)

t = ti.timeCount(t,'Filter optimisation (s):')



weights = opt.W
freqs = np.zeros((Noutput,))

for i_out in range(0,Noutput):

    frq,spc = to.getSpectrum(output,FS)
    freqs[i_out] = frq[np.argmax(spc)]


print "Frequencies:"
print freqs


matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')



for i_out in range(0,Noutput):
    weights = np.dot(R_proj.T,weights).ravel()

    plt.figure()
    plt.plot(weights,linewidth=2.0)

    plt.figure()
    plt.plot(range(0,N_delays)*tau_delay,weights,color='green',linewidth=2.0)

    ylabel('Weights')
    xlabel('Delay (s)')
    tight_layout()
    savefig('../../figures/audio_eig_weights_{0}.pdf'.format(i_out))
show()



for i_out in range(0,Noutput):
    plt.figure()

    plt.plot(inp.T,inp.x,color='gray',linewidth=2.0)
    plt.plot(inp.T,output,color='blue',linewidth=2.0)

    plt.locator_params(axis='x', nbins = 4)
    plt.locator_params(axis='y', nbins = 4)

    ylabel(r'$s_{0}$'.format(i_out))

    #xlim(0.05,0.20)
    #ylim(-0.07,0.07)

    xlabel('t (s)')
    tight_layout()
    savefig('../../figures/audio_eig_wav_{0}.pdf'.format(i_out))

show()

f_min = 1/ (tau_delay * N_delays)
f_max = 1/ tau_delay

for i_out in range(0,Noutput):
    plt.figure()

    frq,spc = to.getSpectrum(weights,1/tau_delay)
    plt.loglog(frq,spc,color='blue',linewidth=2.0)

    xlim([f_min,f_max])
    #ylim([10**-9,10**-4])

    ylabel(r'$| \mathcal{F} (s_%d) |^2$' % (i_out))
    xlabel('f (Hz)')
    tight_layout()
    savefig('../../figures/audio_eig_spc_{0}.pdf'.format(i_out))
show()


for i_out in range(0,Noutput):
    plt.figure()

    frq,spc = to.getSpectrum(inp.x,FS)
    fstart = find(frq > 1)[0]
    fend = find(frq < 3000)[-1]

    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='gray',linewidth=2.0)

    frq,spc = to.getSpectrum(output,FS)
    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='blue',linewidth=2.0)

    xlim([50,3000])
    #ylim([10**-9,10**-4])

    ylabel(r'$| \mathcal{F} (s_%d) |^2$' % (i_out))
    xlabel('f (Hz)')
    tight_layout()
    savefig('../../figures/audio_eig_spc_{0}.pdf'.format(i_out))
show()




plt.figure()
plt.plot(inp.T,inp.x)
xlabel('t (s)')
savefig('../../figures/audio_source.pdf')


plt.figure()
frq,spc = to.getSpectrum(inp.x,FS)
plt.loglog(frq,spc)
ylabel('Spectrum')
xlabel('f (Hz)')
savefig('../../figures/audio_source_spc.pdf')
show()