__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_spiking_lambda import run_SFA_spiking_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_lambda
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy

import bin.brian_SFA.runner_brian.result_saver as result_saver
from bin.filter_optimisation.filter_builder import *
from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import bin.brian_gradient_optimisation.tools as bt
import time


## plot param
t_period = 4 * second
t_start = 0 * second
t_converged = 200 * second

s_thr = 0

# fig param
tmax = 400 * second
FS = 10000 * Hz

# Filtering and window length parameter
tau_omega = 10 * ms

# rate paramters
nu0 = 80 * Hz
nu_av = 100 * Hz

# Time constants
tau_average = 5 * second
tau_derivation = 0.01/(1 * Hz)
tau_w = .5 * ms
Noutputs = 1
dtRecord = 1/(500 * Hz)
Vthr = 10 * mV

np.random.seed(1)
w_0 =  - ones((5,1))#np.random.randn(5,1)
w_0 /= norm(w_0)

db_path= 'results_fig8/'
params = { 'tmax': tmax, 'tau_omega': tau_omega, 'tau_w': tau_w, 'tau_derivation':tau_derivation, 'tau_average': tau_average, 'Noutputs': Noutputs, 'nu0':nu0,'nu_av':nu_av}
count,file_list = result_saver.find_results_in_db(db_path,params)


T,spikes_t,spikes_i,x,x_alpha,s,s_alpha,w,w_end,post_idx  = run_SFA_spiking_lambda(
                                                            tau_omega=tau_omega,
                                                            tmax=tmax,
                                                            tau_w0=tau_w,
                                                            tau_derivation=tau_derivation,
                                                            tau_average=tau_average,
                                                            Noutputs=1,
                                                            w_0=w_0,
                                                            nu0=nu0,
                                                            nu_av=nu_av)


#     if save:
#             result_saver.save_result(db_path,params,[s,s_alpha,w,w_end],[T,spikes_t,spikes_i,x,x_alpha,post_idx])
# else:
#     file_name = db_path + 'objects/' + file_list[0]
#     [T,spikes_t,spikes_i,x,x_alpha,post_idx],[s,s_alpha,w,w_end],param_check = result_saver.get_data(file_name)
#     print 'RESULT LOADED'



plt.figure(figsize=(10,10))
gs = gridspec.GridSpec(3,5)
gs.update(wspace=0.3,hspace=0.6)
plot_fancy.set_font()

ax0 = plt.subplot(gs[0,0:2])
ax0.plot(spikes_t, spikes_i, '.', mew=0)
ax0.set_title(r'Inputs')
ax0.set_yticks(arange(5))
ax0.set_yticklabels([r'$x_1$',r'$x_2$',r'$x_3$',r'$x_4$',r'$x_5$'])
ax0.set_ylim([-0.5,4.5])
ax0.set_xticks([0 ,1.0])
ax0.set_xlim([0*second,1*second])
ax0.set_yticks([])
ax0.set_xticklabels(['0','1 s'])

ax0.set_xticks([0,1.0])

ax2 = plt.subplot(gs[0,3:5])
ax2.set_title(r'$\widehat{s}$ (linearised $r^{out}$) ')
ax2.plot(T,s_alpha.T,linewidth=2,color='black')
ax2.set_xlim([0*second,1*second])
ax2.set_yticks([])
ax2.set_xticks([0,1.0])
ax2.set_xticklabels(['0','1 s'])


ax3 = plt.subplot(gs[1,0:2])
ax3.plot(T,s_alpha.T,linewidth=2,color='black')
ax3.set_xlim([t_start,t_start+t_period])

ax3.set_yticks([-0.03,0,0.03])
ax3.set_ylim([-0.03,0.03])
ax3.set_ylabel(r'$\widehat{s}$')
locator_params(axis = 'x', nbins = 2)

#ax4 = plt.subplot(gs[1,1])
#ax4.plot(T,s_alpha.T,linewidth=2,color='black')
#ax4.set_xlim([t_converged,t_converged+t_period])


ax5 = plt.subplot(gs[1,2:4])
ax5.plot(T,s_alpha.T,linewidth=2,color='black')
ax5.set_xlim([tmax-t_period,tmax])

ax5.set_yticks([-0.03,0,0.03])
ax5.set_ylim([-0.03,0.03])

locator_params(axis = 'x', nbins = 2)


# Breaking between before and after
ax3.spines['right'].set_visible(False)
#ax4.spines['left'].set_visible(False)

#ax4.spines['right'].set_visible(False)
ax5.spines['left'].set_visible(False)

ax3.yaxis.tick_left()
#ax4.yaxis.tick_right()

#ax4.yaxis.tick_left()
ax5.yaxis.tick_right()

#ax4.tick_params(labelleft='off') # don't put tick labels at the top
ax5.tick_params(labelleft='off') # don't put tick labels at the top

d = .05 # how big to make the diagonal lines in axes coordinates
# arguments to pass plot, just so we don't keep repeating them
kwargs = dict(transform=ax3.transAxes, color='k', clip_on=False)
ax3.plot((1-d,1+d),(1-d,1+d), **kwargs)      # bottom-right diagonal
ax3.plot((1-d,1+d),(-d,+d), **kwargs)    # tpop-right diagonal

#kwargs = dict(transform=ax4.transAxes, color='k', clip_on=False)
#ax4.plot((1-d,1+d),(1-d,1+d), **kwargs)      # bottom-right diagonal
#ax4.plot((1-d,1+d),(-d,+d), **kwargs)    # tpop-right diagonal

#kwargs.update(transform=ax4.transAxes)  # switch to the bottom axes
#ax4.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
#ax4.plot((-d,+d),(-d,+d), **kwargs) # top-left diagonal

kwargs.update(transform=ax5.transAxes)  # switch to the bottom axes
ax5.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
ax5.plot((-d,+d),(-d,+d), **kwargs) # top-left diagonal


#plt.setp(ax4.get_yticklabels(), visible=False)
plt.setp(ax5.get_yticklabels(), visible=False)



ax3.set_title('Start')
ax5.set_title('End')














ax6 = plt.subplot(gs[2:3,:])
ax6.plot(T,w.T,linewidth=2,color='black')

ax6.set_ylim([-1,1])
locator_params(axis = 'x', nbins = 2)
locator_params(axis = 'y', nbins = 3)
ax6.set_ylabel('Weights')
ax6.set_xlabel('t (s)')

print(T)
print(s_alpha)
print(t_converged)

T_summed,s_summed = bt.period_sum(T,s_alpha,t_converged,2/t_period)


ax7 = plt.subplot(gs[1,4])
ax7.plot(T_summed,s_summed.ravel(),linewidth=2,color='black')

ax7.set_yticks([])
ax7.set_ylim([-0.03,0.03])

ax7.set_title('Averaged period')
ax7.set_xlim([0*second,t_period/2])
locator_params(axis = 'x', nbins = 1)

#tight_layout()
savefig('../../figures/fig8_spiking.pdf')

plt.show()