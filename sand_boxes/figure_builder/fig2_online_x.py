__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_continuous_lambda import run_SFA_continuous_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_lambda
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy

from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

tmax_short = 4 * second
tmax = 20 * second

tau_omega = 10 * ms
tau_w = 2 * second
tau_derivation = tau_omega


w_0 =  np.random.randn(5,1)
w_0 /= norm(w_0)

T_online,x_th,x_alpha,s_online,s_al,w_online,w_al,post_idx = run_SFA_continuous_lambda(tau_omega=tau_omega,tmax=tmax,tau_w=tau_w,
                                                                                       tau_derivation=tau_derivation,w_0=w_0,Noutputs=1,
                                                                                       dtRecord=1*ms)

x_online = np.zeros((1,np.shape(x_th)[1]))
x_online[0,:] = x_th[2,:]

plt.figure(figsize=(10,12))
gs = gridspec.GridSpec(3,1)


ax = plot_fancy.plot_x_embed(T_online[0:4*10**3],x_online[:,0:4*10**3],Ninputs=1,subplot=[gs[0]],titles=['Batch'],xlabels=[r't(s)'],ylabels=[r'$x_{2}$'],colors=['blue'])
plot_fancy.plot_online(T_online,w_online,x_online,post_idx,subplot=[gs[1:3]],color='blue')


tight_layout()
savefig('../../figures/fig2_online_x.pdf')



plt.show()