__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time


"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""
#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

optType1 = 0        # 2: Wiskott SFA
                    # 1: FISTA
                    # 0: Projected gradient

optType2 = 2        # 2: Wiskott SFA
                    # 1: FISTA
                    # 0: Projected gradient

renormType = 0      # 0: weight,
                    # 1: output variance,
                    # else: none

# Sampling parameters
tmin = 0 * second
tmax = 0.25 * second
FS = 22050 * Hz

# SFA parameters
TauOmega = 10 / FS                  # STDP window time constant constant
TauPeriod = 50 * ms                # Filer length (Limited for memory error issue)

# Optimisation Param
Niteration = 10**5
RecordEvery = Niteration/1000


# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 200

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]
# -----------------------
# Optimisation PARAMETERS
# -----------------------
alOpt = 0.9
optParam[1] = [Niteration,RecordEvery]
optParam[0] = [Niteration,alOpt,RecordEvery]



# -------
# Solver
# -------

t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam)
t = ti.timeCount(t,'Input setup (s):')

# Create STDP window filter (Omega)
builder = fb.FilterBuilder(inp.T,TauPeriod)
F = builder.get_D_filter(2)

plt.figure()
plt.plot(F.getFilter())
plt.show()

t = ti.timeCount(t,'Filter setup (s):')


# Create optimiser object and run optimisation
opt1 = fo.FilterOpt(inp,F,optType1,optParam,renormType)
opt1.solve()

# Get out output and cost sequence
output1 = opt1.getOutput().T
t = ti.timeCount(t,'Output1 (s):')

opt2 = eo.EigOpt(inp.X,1)
opt2.computeSFA()

output2 = opt2.getOutput().ravel()
t = ti.timeCount(t,'Output2 (s):')
# -------
# Plots
# ------

font = {'size'   : 25}

matplotlib.rc('font', **font)


for i in range(0,5):
    plt.figure()
    plt.plot(inp.T,inp.X[i,:],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'$x_{0}$'.format(i+1))
    plt.locator_params(axis = 'x', nbins = 5)
    plt.locator_params(axis = 'y', nbins = 2)
    tight_layout()
    savefig('../../figures/x_{0}.pdf'.format(i+1))
    #show()

plt.figure()
plt.plot(inp.T,output2,linewidth=2.0)
xlabel(r't (s)')
ylabel(r'$s$')
tight_layout()
savefig('../../figures/s_eig.pdf')
show()


plt.figure()
plt.pcolor(opt2.R_proj.reshape(5,1),cmap='BrBG')
savefig('../../figures/w_flag_eig.pdf')
print "Eig flag:"
print opt2.R_proj.reshape(5,1)

plt.figure()
plt.pcolor(opt1.Wbest.reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
print "D2 flag:"
print opt1.Wbest.reshape(5,1)
savefig('../../figures/w_flag_d2_comp.pdf')


plt.figure()

# Plot input
plt.subplot(3,1,1)
plt.plot(inp.T,inp.x)
#frq,spc = to.getSpectrum(inp.x,inp.FS)
#plt.subplot(3,1,2)
#plt.plot(frq,spc)

# OUTPUT 1
plt.subplot(3,1,2)
plt.plot(inp.T,output1)

#frq,spc = to.getSpectrum(output1,inp.FS)
#plt.subplot(3,2,4)
#plt.plot(frq,spc)

# OUTPUT 2
plt.subplot(3,1,3)
plt.plot(inp.T,output2)
#frq,spc = to.getSpectrum(output2,inp.FS)
#plt.subplot(3,2,6)
#plt.plot(frq,spc)

t = ti.timeCount(t,'Plot (s):')
plt.show()