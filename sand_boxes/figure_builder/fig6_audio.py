__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_continuous_lambda import run_SFA_continuous_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_audio
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy

from bin.brian_SFA.runner_brian import result_saver

from matplotlib.mlab import specgram
from matplotlib.colors import LogNorm

from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from bin.input_loader.input_loader import Input
import bin.tools.timeCheck as ti
import cPickle as pickle
import time

import bin.tools.spectralAnalysis as to
from bin.eigs_optimisation.eigs_optimisers import EigOpt
from bin.filter_optimisation.filter_optimisers import *
from bin.filter_optimisation.filter_builder import *


## plot param

pitch = 98 *Hz

NFFT = 256
pad_to = 4*NFFT

frq_min = 50
frq_max = 1000

t_min_zoomed = 4.7
t_max_zoomed = 4.8

frq_min_zoomed = 50
frq_max_zoomed = 120

tmax = 5 * second
FS = 11025 * Hz

colors = ['blue','red','black']
alpha = [0.5,1,0.8]



## data param
#input_file= '../../data/LDC93S1_16000.wav'
input_file= '../../data/countryguit_11025.wav'
sphering = True


N_delays = 256
epsi = 10**-9
N_iteration = 10**3
al_opt = 0.9

tau_delay = 9 /FS


save = False

db_path='results_fig6/'

tCount = time.clock()



params = { 'tmax': tmax, 'FS': FS, 'N_delays': N_delays, 'epsi': epsi, 'tau_delay':tau_delay, 'N_iteration': N_iteration, 'al_opt': al_opt, 'sphering': sphering, 'input_file':input_file}
count,file_list = result_saver.find_results_in_db(db_path,params)

tCount = ti.timeCount(tCount,'Result search (s):')

if count == 0:
    inp = Input(0*second,tmax,int( FS * tmax))
    inp.addWaveFromFile(input_file,0*second)
    inp.whitenInputs()
    inp.expandWithDelay(N_delays,tau_delay)
    if sphering:
        _,R = inp.sphereInputs(False,epsi)
    else:
        R = np.eye(N_delays)


    ## Optimisation with Wistkott's algorithm (eigenvalue decomposition involved)
    eig_opt = EigOpt(inp.X,1)
    eig_opt.computeSFA()
    eig_output = eig_opt.getOutput().ravel()
    eig_weights = eig_opt.R_proj
    eig_weights = np.dot(eig_weights,R).ravel()

    ## Optimisation with our algorithm
    w_0 = np.ones((N_delays,1))

    F = Filter(inp.T,10/FS).get_D_filter(2)
    fil_opt = FilterOpt(inp,F,optParam=[[N_iteration,al_opt,N_iteration/10**2]])
    fil_opt.solve()
    fil_output = fil_opt.getOutput().ravel()
    fil_weights = fil_opt.W.T
    fil_weights = np.dot(fil_weights,R).ravel()

    cost_seq,log_cost_seq = fil_opt.get_objective_sequence()
    plt.figure()
    plt.plot(log_cost_seq)


    plt.figure()
    plt.plot(eig_weights,'red')
    plt.plot(fil_weights,'black')
    plt.show()

    T = inp.T
    x_in = inp.x.ravel()

    tCount = ti.timeCount(tCount,'Result compute (s):')

    if save:
        result_saver.save_result(db_path,params,[eig_output,eig_weights,fil_output,fil_weights],[T,x_in])

else:

    file_name = db_path + 'objects/' + file_list[0]
    [T,x_in],[eig_output,eig_weights,fil_output,fil_weights],params_loaded = result_saver.get_data(file_name)

    tCount = ti.timeCount(tCount,'Result loaded (s):')
x_in = x_in.ravel()

print np.shape(T)
print np.shape(x_in)
delays = arange(N_delays) * tau_delay

## Printing
plt.figure(figsize=(22,9))
gs = gridspec.GridSpec(2,4)
gs.update(wspace=0.5,hspace=0.5)


## Display WISKOTT


print "Variance eig_output"
print np.var(eig_output)


print "norm eig_output"
print norm(eig_weights)

print "Variance fil_output"
print np.var(fil_output)
print "Norm fil_weights"
print norm(fil_weights)

plt.subplot(gs[0,0])
plt.plot(T,x_in,color=colors[0],alpha=alpha[0],linewidth=2.0,label=r'$x_0$')
plt.plot(T,eig_output,color=colors[1],alpha=alpha[1],linewidth=2.0,label=r'$s$ (SFA)')
plt.plot(T,fil_output,color=colors[2],alpha=alpha[2],linewidth=1.4,label=r'$s$ (Batch)')

ylabel(r'$x_0 / s $')
xlabel('t (s)')
ylim([-5,5])

ind_start = find(np.abs(x_in) > 0)[0]
xlim([T[ind_start]-0.20*second,max(T)+0.20*second])

locator_params(axis = 'x', nbins = 4)
locator_params(axis = 'y', nbins = 4)

#plot_fancy.1lot_s_embed(T,eig_output,background=x_in,Noutputs=1,subplot=[gs00[0,0]],titles=['SFA'],ylabels=[r'$s$'],colors=['red'],lim=False)
#plot_fancy.plot_s_embed(T,eig_output,background=x_in,Noutputs=1,subplot=[gs00[0,1]],titles=['SFA'],ylabels=[r'$s$'],colors=['red'],tmin=4.6*second,tmax=5*second)


dt = T[1] - T[0]
int_min = int(t_min_zoomed/dt)
int_max = int(t_max_zoomed/dt)+1

T_zoomed= T[int_min:int_max]
x_in_zoomed = x_in[int_min:int_max]
eig_out_zoomed = eig_output[int_min:int_max]
fil_out_zoomed = fil_output[int_min:int_max]

plt.subplot(gs[0,1])
# plt.plot(T_zoomed,x_in_zoomed,color='blue',alpha=0.5,linewidth=2.0,label=r'x_0')
# plt.plot(T_zoomed,eig_out_zoomed,color='red',linewidth=2.0,label=r'$s$ (SFA)')
# plt.plot(T_zoomed,fil_out_zoomed,color='black',linewidth=1.0,alpha=1,label=r'$s$ (Batch)')
plt.plot(T,x_in,color=colors[0],alpha=alpha[0],linewidth=2.0,label=r'$x_0$')
plt.plot(T,eig_output,color=colors[1],alpha=alpha[1],linewidth=2.0,label=r'$s$ (SFA)')
plt.plot(T,fil_output,color=colors[2],alpha=alpha[2],linewidth=1.4,label=r'$s$ (Batch)')

xlim(t_min_zoomed,t_max_zoomed)

ylabel(r'$x_0 / s $')
xlabel('t (s)')
ylim([-5,5])

locator_params(axis = 'x', nbins = 4)
locator_params(axis = 'y', nbins = 4)





frq,spc_in = to.getSpectrum(x_in,FS/Hz)
frq,spc_eig = to.getSpectrum(eig_output,FS/Hz)
frq,spc_fil = to.getSpectrum(fil_output,FS/Hz)


plt.subplot(gs[0,2:4])
plot_fancy.set_font()

plot(frq,10* np.log10(spc_in),color=colors[0],alpha=alpha[0],linewidth=2.0,label=r'$x_0$')
plot(frq,10* np.log10(spc_eig),color=colors[1],linewidth=2.0,label=r'$s$ (SFA)')
plot(frq,10* np.log10(spc_fil),color=colors[2],alpha=alpha[2],linewidth=2.0,label=r'$s$ (Batch)')
xlim([frq_min,frq_max])
ylim([-80,0])
ylabel(r'$| \mathcal{F} (s) |$ (dB)')
xlabel('f (Hz)')
title('Power spectrum')

legend(bbox_to_anchor=(1.05, 1), loc=2, mode="expand")

xscale('log')
locator_params(axis = 'y', nbins = 4)


#
# df = 1/tmax
# int_min = int(frq_min_zoomed/df)
# int_max = int(frq_max_zoomed/df)
#
# frq_zoomed = frq[int_min:int_max]
# spc_in_zoomed = spc_in[int_min:int_max]
# spc_eig_zoomed = spc_eig[int_min:int_max]
# spc_fil_zoomed = spc_fil[int_min:int_max]
#
#
# pt = plt.subplot(gs01[1])
# pt.loglog(frq,spc_in,color=colors[0],alpha=alpha[0],linewidth=2.0)
# pt.loglog(frq,spc_eig,color=colors[1],linewidth=2.0)
# pt.loglog(frq,spc_fil,color=colors[2],alpha=alpha[2],linewidth=2.0)
# xlim([frq_min_zoomed,frq_max_zoomed])
# ylim([10**-4,10**-1])
# ylabel(r'$| \mathcal{F} (s) |^2$')
# xlabel('f (Hz)')
# title('(Zoom)')




frq_wei,spc_weights_eig = to.getSpectrum(eig_weights,1/tau_delay,pad_to=1024)
frq_wei,spc_weights_fil = to.getSpectrum(fil_weights,1/tau_delay,pad_to=1024)

spc_weights_eig /= norm(spc_weights_eig)
spc_weights_fil /= norm(spc_weights_fil)

spc_weights_eig_dB = 10 * np.log10(spc_weights_eig)
spc_weights_fil_dB = 10 * np.log10(spc_weights_fil)





ax = plt.subplot(gs[1,0:2])
plot_fancy.set_font()
ax.plot(delays,eig_weights,color=colors[1],linewidth=2.0,label='s (SFA)')
ax.plot(delays,fil_weights,color=colors[2],linewidth=2.0,alpha=alpha[2],label='s (Batch)')
ax.set_title('Learnt filter')
ax.set_ylabel('Weights')
ax.set_xlabel('Delays (s)')
ax.set_xlim([min(delays),max(delays)])
#ylim([-0.05,0.05])



locator_params(axis = 'x', nbins = 4)
locator_params(axis = 'y', nbins = 4)

ax = plt.subplot(gs[1,2])
plot_fancy.set_font()
ax.plot(frq_wei,spc_weights_eig_dB,color=colors[1],linewidth=2.0)
ax.plot(frq_wei,spc_weights_fil_dB,color=colors[2],alpha=alpha[2],linewidth=2.0)
ax.set_title('Frequency response')
ax.set_ylabel('Gain (dB)')
ax.set_xlabel('f (Hz)')
ax.set_ylim([-70,0])
ax.set_xscale('log')
ax.set_xlim([frq_min,frq_max])

locator_params(axis = 'y', nbins = 4)


ax = plt.subplot(gs[1,3])
plot_fancy.set_font()
ax.plot(frq_wei,spc_weights_eig_dB,color=colors[1],linewidth=2.0)
ax.plot(frq_wei,spc_weights_fil_dB,color=colors[2],alpha=alpha[2],linewidth=2.0)
ax.set_title('Frequency response')
ax.set_ylabel('Gain (dB)')
ax.set_xlabel('f (Hz)')
ax.set_ylim([-40,0])
ax.set_xscale('log')
ax.set_xlim([frq_min_zoomed,frq_max_zoomed])

locator_params(axis = 'y', nbins = 4)

ax.axvline(pitch/Hz,linestyle='--',linewidth=4.0, color='green')
#plt.axvline(92.5,linewidth=2.0, color='green')
#plt.axvline(103.8,linewidth=2.0, color='green')

#
#
# ## Display BATCH
# plot_fancy.plot_s_embed(T,fil_output,background=x_in,Noutputs=1,subplot=[gs01[0,0]],titles=['SFA'],ylabels=[r'$s$'],colors=['black'])
# plot_fancy.plot_s_embed(T,fil_output,background=x_in,Noutputs=1,subplot=[gs01[0,1]],titles=['SFA'],ylabels=[r'$s$'],colors=['black'],tmin=4.6*second,tmax=5*second)
#
# frq,spc_in = to.getSpectrum(x_in,FS/Hz)
# frq,spc = to.getSpectrum(fil_output,FS/Hz)
# plt.subplot(gs01[1,0])
# plt.loglog(frq,spc_in,color='gray',linewidth=2.0)
# plt.loglog(frq,spc,color='black',linewidth=2.0)
# xlim([frq_min,frq_max])
# ylim([10**-4,10**-1])
# ylabel(r'$| \mathcal{F} (s) |^2$')
# xlabel('f (Hz)')
# title('Power spectrum')
#
# plt.axvline(98, color='red')
#
#
# plot_fancy.plot_s_embed(delays,fil_weights,Noutputs=1,subplot=[gs01[1,1]],titles=['Learnt filter'],ylabels=[r'weights'],colors=['black'],lim=False)





#tight_layout()
savefig('../../figures/fig6_audio_guit.pdf')

plt.show()

plt.figure()

gs_spec = gridspec.GridSpec(1,2)

P_in,freq,tms = matplotlib.mlab.specgram(x_in,Fs=11025,NFFT=NFFT,pad_to=pad_to)

inds = np.intersect1d(np.where(freq < frq_max),np.where(freq>frq_min))
freq = freq[inds]
P_in = P_in[inds,:]
print inds

ha = plt.subplot(gs_spec[0])
ha.pcolor(tms, freq, P_in)
xlabel('t (s)')
ylabel('f (Hz)')
ha.set_yscale('log')
ha.set_ylim([frq_min,frq_max])

P_eig,freq,tms = matplotlib.mlab.specgram(eig_output.ravel(),Fs=11025,NFFT=NFFT,pad_to=pad_to)
inds = np.intersect1d(np.where(freq < frq_max),np.where(freq>frq_min))
freq = freq[inds]
P_eig = P_eig[inds,:]


ha = plt.subplot(gs_spec[1])
ha.pcolor(tms, freq, P_eig)
xlabel('t (s)')
ylabel('f (Hz)')
ha.set_yscale('log')
ha.set_ylim([frq_min_zoomed,frq_max])

savefig('../../figures/fig7_spectogram.pdf')


plt.show()