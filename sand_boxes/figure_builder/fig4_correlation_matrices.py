__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_continuous_lambda import run_SFA_continuous_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_lambda
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy
from bin.brian_SFA.runner_brian import result_saver

import matplotlib.pyplot as plt
import bin.input_loader.input_loader as il
import bin.filter_optimisation.filter_builder as fb

from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

number = 20

cos_coeff_list = np.power(100.,range(-3,4))
tau_omega_list = [0,0.1,1.,10.,100.]*ms
db_path= '../../bin/brian_SFA/results_alpha/batch/'

fig = plt.figure(figsize=(16, 8))

gs0 = gridspec.GridSpec(3,1)
gs0.update(hspace=2.5)


gs00 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs0[0])
gs0.update(wspace=5)

gs000 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs00[0])
gs001 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs00[1])

gs01 = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec=gs0[1:3])


inp = il.Input(0*second,2*second,10**4)
sinus = sin( 2 * np.pi * Hz * inp.T)
cosinus = sin( 2 * np.pi * 11 * Hz * inp.T)

X0 = sinus + 0.5 * cosinus
X1 = sinus + 2 * cosinus

X0 /= std(X0)
X1 /= std(X1)

plot_fancy.plot_x_embed(inp.T,X0,Ninputs=1,subplot=[gs000[0]],ylabels=[r'$x_1$'],titles=[r'$\alpha=0.5$'],xlabels=[r'$t (s)$'])
ax_list = plot_fancy.plot_x_embed(inp.T,X1,Ninputs=1,subplot=[gs000[1]],titles=[r'$\alpha=2$'],xlabels=[r'$t (s)$'])
ax_list[0].get_yaxis().set_ticklabels([])

F = fb.Filter(inp.T,70 * ms)
f1 = F.getSFAFilter(0.5 * ms).getFilter()
f2 = F.getSFAFilter(2 * ms).getFilter()

ax_list = plot_fancy.plot_kernels_embed(F.T,[f1,f2],
                              N_kernels=2,
                              subplot=[gs001[0],gs001[1]],
                              ylabels=[r'$\Lambda_{SFA}$',''],
                              titles=[r'$\tau_{STDP} = 0.5 ms$',r'$\tau_{STDP}= 2 ms$'],
                              xlabels=[r'$t (ms)$',r'$t (ms)$'],
                              colors=['black','black'])
ax_list[1].get_yaxis().set_ticklabels([])




xlabs = map("{:2.0f}".format, np.log10(cos_coeff_list))

params = { 'cos_coeff_list': cos_coeff_list ,'tau_omega': 0 * second}
corr = result_saver.build_correlation_statistic(db_path,params,number=number)
plot_fancy.plot_correlation_matrix(corr.T,
                                   y_tick_labels=plot_fancy.get_win(),
                                   x_tick_labels=xlabs,
                                   subplot=[gs01[0]],
                                   xlabel=r'$\log_{10} \alpha$',
                                   title=r'$\tau_{STDP} = 0 ms $',
                                   y_tick_color=plot_fancy.get_color())


params = { 'cos_coeff_list': cos_coeff_list ,'tau_omega': 10 * ms}
corr = result_saver.build_correlation_statistic(db_path,params,number=number)

ax,im = plot_fancy.plot_correlation_matrix(corr.T,
                                   x_tick_labels=xlabs,
                                   subplot=[gs01[1]],xlabel=r'$\log_{10} \alpha$',
                                   title=r'$\tau_{STDP} = 10 ms $')



xlabs = map("{:0.1f}".format, tau_omega_list/ (1 *ms))

db_path= '../../bin/brian_SFA/results_tau/batch/'
params = {'tau_omega_list': tau_omega_list}
corr = result_saver.build_correlation_statistic(db_path,params,number=number)
plot_fancy.plot_correlation_matrix(corr.T,
                                   x_tick_labels=xlabs,
                                   subplot=[gs01[2]],
                                   xlabel=r'$\tau_{STDP} (ms)$ ',
                                   title=r'$\alpha = 1$')

cax = fig.add_axes([0.25, 0.65, 0.5, 0.04])
cbar = plt.colorbar(im,cax=cax,ticks=[0, 1],orientation='horizontal')
cbar.ax.set_xticklabels(['Faster solutions', 'Slow solution at each trial'])# vertically oriented colorbar


#tight_layout()
savefig('../../figures/fig4_correlations.pdf')
plt.show()


#plt.show()