__author__ = 'guillaume'
import scipy as sc
import scipy.linalg as la
import matplotlib.pyplot as plt
import bin.filter_optimisation.filter_builder as tr
from numpy import *
from brian2 import *

import bin.tools.spectralAnalysis as sa

Tau = 10 * ms
tmin = -10 * Tau
tmax = 10 * Tau
N = 1000
K_plot = 4

TauPeriod = tmax - tmin

dt = (tmax - tmin) / (N* 1.0)
Tdata = range(0,N) *dt - TauPeriod*0.5

#Tau = 5 *dt

builder = tr.FilterBuilder(Tdata,TauPeriod)

# Window design
wind = [[],[],[],[]]
wind[0] = builder.getAlphaFilter(Tau).getFilter()
wind[1] = builder.getDerivativeFilter(Tau).getFilter()
wind[2] = builder.getSigmaFilter(Tau).getFilter()
wind[3] = - builder.getSigmaFilter(Tau).getFilter()

T = builder.T

color = ["green","red","purple","brown"]
win = [r'$\Omega_{SFA} $',r'$\Omega_{Classic} = d_{(1,\tau)}$',r'$\Omega_{Control+} = \sigma_{\tau}$',r'$\Omega_{Control-} = - \sigma_{\tau}$']
#Plot

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

for i in range(0,K_plot):
    plt.figure()
    plt.plot(T,zeros(shape(T)),'y--')
    plt.plot(T,wind[i],color = color[i],linewidth=2.0)
    xlabel(r't (s)')

    ylim(np.min(wind[1]),np.max(wind[1]))
    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])
    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)
    tight_layout()
    savefig('../../figures/win_{0}.pdf'.format(i))

plt.show()


plt.figure()
frq,spc = sa.getSpectrum(wind2,1/dt)
plt.loglog(frq,spc)

frq,spc = sa.getSpectrum(wind3,1/dt)
plt.loglog(frq,spc)

show()

plt.figure()
#plt.subplot(2,2,1)
plt.plot(T,zeros(shape(T)),'r--')
plt.plot(T,wind1,'blue',linewidth=2.0)

plt.figure()
plt.subplot(2,2,2)
plt.plot(T,np.zeros(np.shape(T)),'r--')
plt.plot(T,wind2,'blue')



plt.subplot(2,2,3)
plt.plot(T,np.zeros(np.shape(T)),'r--')
plt.plot(T,vstack((wind3,wind4)).T,'blue')



plt.subplot(2,2,4)
plt.plot(T,np.zeros(np.shape(T)),'r--')
plt.plot(T,wind4,'blue')



plt.figure(4)
plt.plot(T,wind3)
plt.plot(T,wind4)
plt.show()
