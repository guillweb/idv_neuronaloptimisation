__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx

from scipy.interpolate import spline

import os

import bin.tools.general as genTools
"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""
#Input Type
inputType = 2       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none

optType1 = 1        # 2: Wiskott SFA
                    # 1: FISTA
                    # 0: Projected gradient

renormType = 0      # 0: weight,
                    # 1: output variance,
                    # else: none

# Sampling parameters
tmin = .45 * second
tmax = .65 * second
FS = 11025 * Hz

# SFA parameters
#TauOmegaArray = genTools.logSpace(1.,1000.,4)*ms         # STDP window time constant constant
TauOmegaArray = [10.,100.]*ms                            # STDP window time constant constant



# Optimisation Param
Niteration = 10**4
RecordEvery = Niteration/1000

# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]
# --------------------
# EXPANSION PARAMETERS
# --------------------

# Input expansion into high dimensional space
NfiltersOrDelay = 800

TauDelay = 1/ FS
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True
Degenerate = False

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering,Degenerate]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering,Degenerate]
# -----------------------
# Optimisation PARAMETERS
# -----------------------
alOpt = 0.9
optParam[1] = [Niteration,RecordEvery]
optParam[0] = [Niteration,alOpt,RecordEvery]


# -------
# Solver
# -------
fact = 20
print [tmin,tmax]
print FS
print TauOmegaArray
print Niteration
print NfiltersOrDelay
print fact
t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam)
t = ti.timeCount(t,'Input setup (s):')

freqs = np.zeros((len(TauOmegaArray,)))
slownesses = np.zeros((len(TauOmegaArray,)))
OUT = np.zeros((len(TauOmegaArray),inp.N,))
Weights = np.zeros((len(TauOmegaArray),inp.n,))

for iTau in range(0,len(TauOmegaArray)):

    TauOmega = TauOmegaArray[iTau]
    TauPeriod = fact * TauOmega

    expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]

    # Create STDP window filter (Omega)
    builder = fb.FilterBuilder(inp.T,TauPeriod)
    F = builder.getDerivativeSquaredFilter(TauOmega)
    t = ti.timeCount(t,'Filter setup (s):')


    # Create optimiser object and run optimisation
    opt1 = fo.FilterOpt(inp,F,optType1,optParam,renormType)
    opt1.solve()

    # Get out output and cost sequence
    output = opt1.getOutput().T
    t = ti.timeCount(t,'Output1 %s (s):' % [TauOmega,NfiltersOrDelay])

    Nlen = len(output)
    dout = output[1:Nlen] - output[0:Nlen-1]

    slownesses[iTau] = np.dot(dout.T,dout) * 1 / float(Nlen)
    OUT[iTau,:] = output.ravel()
    Weights[iTau,:] = opt1.W.ravel()

    del opt1

for iTau in range(0,len(TauOmegaArray)):
    output = OUT[iTau,:].ravel()

    frq,spc = to.getSpectrum(output,FS)
    freqs[iTau] = frq[np.argmax(spc)]


print "Slowness:"
print slownesses

print "Frequencies:"
print freqs


matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')



cmap_greens = cm = plt.get_cmap('Greens')
cNorm  = colors.Normalize(vmin=np.log(np.min(TauOmegaArray)/ ms)-3, vmax=np.log(np.max(TauOmegaArray) /ms))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cmap_greens)

for iTau in range(0,len(TauOmegaArray)):
    plt.figure()
    tau = TauOmegaArray[iTau]
    color = scalarMap.to_rgba(np.log(tau/ms))

    output = OUT[iTau,:].ravel()
    plt.plot(inp.T,inp.x,color='gray',linewidth=2.0)
    plt.plot(inp.T,output,color='green',linewidth=2.0)

    plt.locator_params(axis = 'x', nbins = 4)
    plt.locator_params(axis = 'y', nbins = 4)

    xlim(0.05,(tmax - tmin)/second)
    ylim([-20,20])

    xlabel('t (s)')
    tight_layout()
    savefig('../../figures/audio_wav_{0}.pdf'.format(tau))

show()

for iTau in range(0,len(TauOmegaArray)):
    output = OUT[iTau,:].ravel()

    plt.figure()
    tau = TauOmegaArray[iTau]
    color = scalarMap.to_rgba(np.log(tau/ms))



    frq,spc = to.getSpectrum(inp.x,FS)
    fstart = find(frq > 50)[0]
    fend = find(frq < 3000)[-1]

    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='gray',linewidth=2.0)

    frq,spc = to.getSpectrum(output,FS)
    plt.loglog(frq[fstart:fend],spc[fstart:fend],color='green',linewidth=2.0)

    xlim([50,3000])
    ylim([10**-7,10**1])

    xlabel('f (Hz)')
    tight_layout()
    savefig('../../figures/audio_spc_{0}.pdf'.format(tau))
show()


for iTau in range(0,len(TauOmegaArray)):
    weights = Weights[iTau,:].ravel()

    plt.figure()
    tau = TauOmegaArray[iTau]
    plt.plot(range(0,NfiltersOrDelay)*TauDelay,weights,color='green',linewidth=2.0)

    ylabel('Weights')
    xlabel('Delay (s)')
    tight_layout()
    savefig('../../figures/audio_weights_{0}.pdf'.format(tau))
show()






TauPeriodDraw =  max(TauOmegaArray) * 14
dt_draw =  min(TauOmegaArray) /5

Tdraw = np.array(range(0,int(TauPeriodDraw/dt_draw))) * dt_draw
Tdraw = Tdraw - Tdraw[-1]/2.

for iTau in range(0,len(TauOmegaArray)):
    tau = TauOmegaArray[iTau]
    color = scalarMap.to_rgba(np.log(TauOmegaArray[iTau]/ms))
    drawer = fb.FilterBuilder(Tdraw,TauPeriodDraw)
    plt.figure()

    plt.plot(Tdraw,drawer.getDerivativeSquaredFilter(TauOmegaArray[iTau]).getFilter(),color='green',linewidth=2.0)

    plt.ylim((-0.001,0.01))
    plt.xlim((-TauPeriodDraw/2,TauPeriodDraw/2))

    xlabel('t (s)')
    ylabel(r'$\tau$ = {0}'.format(tau))

    tight_layout()
    savefig('../../figures/audio_win_{0}.pdf'.format(tau))

show()

plt.figure()
plt.plot(inp.T,inp.x)
xlabel('t (s)')
savefig('../../figures/audio_source.pdf')


plt.figure()
frq,spc = to.getSpectrum(inp.x,FS)
plt.loglog(frq,spc)
ylabel('Spectrum')
xlabel('f (Hz)')
savefig('../../figures/audio_source_spc.pdf')
show()