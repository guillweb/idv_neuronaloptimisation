__author__ = 'guillaume'
import bin.filter_optimisation.filter_builder as fb
import bin.filter_optimisation.filter_optimisers as fo
import bin.eigs_optimisation.eigs_optimisers as eo
from brian2 import *
import bin.input_loader.input_loader as il
import bin.tools.spectralAnalysis as to
import bin.tools.timeCheck as ti
import time

import scipy.linalg as la
import matplotlib
"""
Script to perform one-dimensional SFA using a particular STDP window.
The algorithm is implemented in the toeplitz matrix space.
A FISTA algorithm can be used, it is a gradient descent accelerated algorithm, no parameter is involved in that case.
"""

N_window = 4
optType = [0,0,0,0]
windowType = [0,1,2,3]

#Input Type
optType[0] = 0      # 2: Wiskott SFA
                    # 1: FISTA
                    # 0: Projected gradient

windowType[0] = 0   # 3: -Sigma
                    # 2: Sigma
                    # 1: D_(1,) - STDP causal
                    # 0: SFA = d2


renormType = 0      # 0: weight,
                    # 1: output variance,
                    # else: none
#Input Type
inputType = 3       # 3: Wiskott classic
                    # 2: audio file,
                    # 1: 2 Sinuses,
                    # else: Noised cosin

expansionType = 1   # 2: Exponential filtering
                    # 1: Delays
                    # else: none



renormType = 0      # 0: weight,
                    # 1: output variance,
                    # else: none

# Sampling parameters
tmin = 0 * second
tmax = 0.25 * second
FS = 22050 * Hz

# SFA parameters
TauOmega = 10 / FS                  # STDP window time constant constant
TauPeriod = 10 / FS                # Filer length (Limited for memory error issue)

# Optimisation Param
Niteration = 10**5
RecordEvery = Niteration/1000


# ----------------
# INPUT PARAMETERS
# ----------------
N = int((tmax - tmin) * FS)

inputParam = [[],[],[],[]]
expParam = [[],[],[]]
optParam = [[],[],[]]
# From audio input
inputFileName= "../../data/yourmom.wav"
inputParam[2] = [inputFileName,0 * ms]
# Cosin like inputs
f0,a0,offset0 = [1000 * Hz,1,0 * ms] # frequency, amplitude, offset
f1,a1,offset1 = [1100 * Hz,1,0 * ms]
ard = 0.1
inputParam[1] = [f0,a0,offset0,f1,a1,offset1]
inputParam[0] = [f0,a0,offset0,ard]
# For Wiskott classic
inputParam[3] = [10 * Hz]
# --------------------
# EXPANSION PARAMETERS
# --------------------
# Input expansion into high dimensional space
NfiltersOrDelay = 200

TauDelay = 1/ FS * 2
TauInputFilters = 10* 1/FS           # Filter time constant constant
TauMin = TauInputFilters/2
TauMax = TauInputFilters*2

Sphering = True

expParam[1] = [NfiltersOrDelay,TauDelay,Sphering]
expParam[2] = [NfiltersOrDelay,TauMin,TauMax,Sphering]
# -----------------------
# Optimisation PARAMETERS
# -----------------------
alOpt = 0.9
optParam[1] = [Niteration,RecordEvery]
optParam[0] = [Niteration,alOpt,RecordEvery]



# -------
# Solver
# -------

t = time.clock()

# Define input
inp = il.Input.buildInput(tmin,tmax,N,inputType,inputParam,expansionType,expParam)
t = ti.timeCount(t,'Input setup (s):')

# Create STDP window filter (Omega)
builder = fb.FilterBuilder(inp.T,TauPeriod)


output = np.zeros((N_window,N))
for i in range(0,N_window):
    F = builder.newFilter()
    if windowType[i] == 2:
        F = builder.getDiracFilter()
    elif windowType[i] == 1:
        F = builder.get_D_filter(1)
    elif windowType[i] == 3:
        F.setFilter(- builder.getDiracFilter().getFilter())
    else:
        F = builder.get_D_filter(2)

    opt = fo.FilterOpt(inp,F,optType[i],optParam,renormType)
    opt.solve()
    t = ti.timeCount(t,'Output {0} (s):'.format(i))

    output[i,:] = opt.getOutput().ravel()

    del opt,F

# -------
# Plots
# ------

matplotlib.rc('font', size=40)
matplotlib.rc('xtick', labelsize='20')
matplotlib.rc('ytick', labelsize='20')

color = ["green","red","purple","brown"]
win = [r'$\lambda_{SFA} = d_{(2)}$',r'$\lambda_{Classic} = d_{(1)}$',r'$\lambda_{Control+} = \delta_{0}$',r'$\lambda_{Control-} = - \delta_0$']


for i in range(0,N_window):
    # OUTPUT 1
    plt.figure()
    plt.plot(inp.T,output[i,:].ravel(), color = color[i],linewidth=2.0)
    xlabel(r't (s)')
    ylabel(r'$s$')
    figtext(0.55, 0.88, win[i], ha="center", va="bottom", size="medium",color=color[i])
    plt.locator_params(axis = 'x', nbins = 5)
    plt.locator_params(axis = 'y', nbins = 2)
    tight_layout()
    savefig('../../figures/s_STDP_{0}.pdf'.format(i))


plt.show()

#
# # OUTPUT 1
# plt.figure()
# plt.pcolor(opt1.W.reshape(5,1),cmap='BrBG')
# print 'W SFA'
# print opt1.W.reshape(5,1)
# title(r'$W_{SFA}$')
# tight_layout()
# savefig('../../figures/w_flag_STDP_d2.pdf')
#
#
# # OUTPUT 2
# plt.figure()
# plt.pcolor(opt2.W.reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
# print 'W STDP'
# print opt2.W.reshape(5,1)
# title(r'$W_{STDP}$')
# tight_layout()
# savefig('../../figures/w_flag_STDP_d1.pdf')
#
#
# # OUTPUT 3
# plt.figure()
# plt.pcolor(opt3.W.reshape(5,1),cmap='BrBG',vmin=-1, vmax=1)
# print 'W control'
# print opt3.W.reshape(5,1)
# title(r'$W_{control}$')
# tight_layout()
# savefig('../../figures/w_flag_STDP_sig.pdf')
#
#
# plt.show()
#
#
# # OUTPUT 1
# plt.figure()
# w_seq = sum(opt1.Wsequence**2,axis=1)
# plt.plot(np.log(w_seq),'green',linewidth=2.0)
#
#
# # OUTPUT 2
# plt.figure()
# w_seq = sum(opt2.Wsequence**2,axis=1)
# plt.plot(np.log(w_seq),'red',linewidth=2.0)
#
#
# # OUTPUT 3
# plt.figure()
# w_seq = sum(opt3.Wsequence**2,axis=1)
# plt.plot(np.log(w_seq),'purple',linewidth=2.0)
#
#
# t = ti.timeCount(t,'Plot (s):')
# plt.show()