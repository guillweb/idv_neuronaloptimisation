__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_continuous_lambda import run_SFA_continuous_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_lambda
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy

from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

tmax_short = 4 * second
tmax = 100 * second

tau_omega = 10 * ms
tau_w = 2 * second
tau_derivation = tau_omega

w_0 =  np.random.randn(5,1)
w_0 /= norm(w_0)

T_batch,x,s_batch = run_SFA_batch_lambda(tau_omega=tau_omega,tmax=tmax_short,w_0=w_0,Noutputs=1)
T_wiskott,x,s_wiskott = run_SFA_wiskott(tau_omega=tau_omega,tmax=tmax_short,w_0=w_0,Noutputs=1)
T_online,x_th,x_alpha,s_online,s_al,w_online,w_al,post_idx = run_SFA_continuous_lambda(tau_omega=tau_omega,tmax=tmax,tau_w=tau_w,tau_derivation=tau_derivation,w_0=w_0,Noutputs=1)



plt.figure()
gs = gridspec.GridSpec(3,2)


plot_fancy.plot_s_embed(T_wiskott,s_wiskott,Noutputs=1,subplot=[gs[0,0]],titles=['SFA'],ylabels=[r'$s$'],colors=['red'])
plot_fancy.plot_s_embed(T_batch,s_batch,Noutputs=1,subplot=[gs[0,1]],titles=['Batch'],linestyles=['-'])
plot_fancy.plot_online(T_online,w_online,s_online,post_idx,subplot=[gs[1:3,:]])


tight_layout()
savefig('../../figures/fig3_convergence.pdf')



plt.show()