__author__ = 'guillaume'

from numpy import *
from brian2 import *
from bin.brian_SFA.runner_brian import result_saver
import time

from bin.input_loader.input_loader import Input
import bin.tools.timeCheck as ti
import cPickle as pickle
import scipy.io.wavfile as wave

from bin.eigs_optimisation.eigs_optimisers import EigOpt
from bin.filter_optimisation.filter_optimisers import *
from bin.filter_optimisation.filter_builder import *

import resource

rsrc = resource.RLIMIT_DATA
soft, hard = resource.getrlimit(rsrc)
resource.setrlimit(rsrc, (2048, hard)) #limit to one kilobyte

## plot param
tmax = 4 * second
FS = 22050 * Hz
sphering = True
file_number = 100

N_delays = 128
epsi = 10**-9
N_iteration = 10**3
al_opt = 0.9
tau_delay = 100 /FS
save = True

song_db_path = 'sub_bpm_database.pickle'
f = open(song_db_path,'rb')
song_list = pickle.load(f)

tCount = time.time()

input_path='input_file/'
input_params = \
    {'tmax': tmax,
     'tau_delay':tau_delay,
     'N_delays':N_delays,
     'FS': FS,
     'song_db_path': song_db_path,
     'sphering': sphering,
     'first_file_path':song_list[0]['path'],
     'last_file_path':song_list[-1]['path'],
     'file_number': file_number}

# Build input
count,file_list = result_saver.find_results_in_db(input_path,input_params)
if count == 0:
    print 'Building input.'
    n = int(FS * tmax)
    shuffle(song_list)
    tempo_list = zeros(file_number)

    inp = Input(0*second,tmax,n * file_number)
    for k_s,s in enumerate(song_list):
        if k_s >= file_number: break
        print 'Concatenating song ', k_s
        fs,data = wave.read(s['path'])
        assert FS/Hz == fs, 'Sampling frequency should be {0} and found to be {1}'.format(FS,fs)
        inp.x[k_s*n:(k_s+1)*n] = data[0:n]
        tempo_list[k_s] = s['bpm']

    inp.whitenInputs()
    inp.expandWithDelay(N_delays,tau_delay)
    if sphering:
        _,R = inp.sphereInputs(False,epsi)
    else:
        R = np.eye(N_delays)
    result_saver.save_result(input_path,input_params,[inp.x,inp.X,R,tempo_list],[])
else:
    n = int(FS * tmax)
    file_name = input_path + 'objects/' + file_list[0]
    _,[x,X,R,tempo_list],params_loaded = result_saver.get_data(file_name)
    inp = Input(0*second,tmax,n * file_number)
    inp.x = x
    inp.X = X
    inp.n = shape(X)[0]
    assert params_loaded == input_params, 'Parameter mismatch with saved computation'

plt.figure()
plt.hist(tempo_list)
plt.show()

tCount = ti.timeCount(tCount,'Input built (s):')

# Compute SFA
res_path='results_SFA_for_all/'
params = { 'input_file': file_list[0],
           'input_params': input_params,
           'epsi': epsi,
           'N_iteration': N_iteration,
           'al_opt': al_opt}
count,file_list = result_saver.find_results_in_db(res_path,params)

if count == 0:
    print 'Optimising'
    ## Optimisation with our algorithm
    w_0 = np.ones((N_delays,1))

    F = Filter(inp.T,10/FS).get_D_filter(2)
    fil_opt = FilterOpt(inp,F,optParam=[[N_iteration,al_opt,N_iteration/10**2]])
    fil_opt.solve()
    fil_output = fil_opt.getOutput().ravel()
    fil_weights = fil_opt.W.T
    fil_weights = np.dot(fil_weights,R).ravel()

    cost_seq,log_cost_seq = fil_opt.get_objective_sequence()
    T = inp.T
    x_in = inp.x.ravel()

    if save:
        result_saver.save_result(res_path,params,[fil_output,fil_weights,log_cost_seq],[T,x_in])
else:

    file_name = res_path + 'objects/' + file_list[0]
    [T,x_in],[fil_output,fil_weights,log_cost_seq],params_loaded = result_saver.get_data(file_name)

tCount = ti.timeCount(tCount,'Result loaded (s):')

def get_variance_per_chunk(output,chunk_number,chunk_len):
    vars = zeros(chunk_number)
    for k in arange(chunk_number):
        vars[k] = var(output[k*chunk_len:(k+1)*chunk_len])
    return vars

vars = get_variance_per_chunk(fil_output,file_number,n)

plt.figure()
plt.subplot(2,1,1)
plt.hist(tempo_list)
plt.subplot(2,1,2)
plt.scatter(vars,tempo_list)

plt.figure()
plt.plot(log_cost_seq)

plt.figure()
plt.plot(fil_weights,'black')
plt.show()

plt.figure()
plt.plot(fil_output)