__author__ = 'guillaume'


from numpy import *
from brian2 import *
import matplotlib.pyplot as plt
import cPickle as pickle
from generate_task import *
import matplotlib.gridspec as gridspec
from scipy.signal import fftconvolve

from bin.brian_SFA.runner_brian import plot_fancy
import bin.tools.spectralAnalysis as to

## plot param

BPM = 125 / (60* second)

NFFT = 256
pad_to = 4*NFFT

frq_min = 0.1
frq_max = 4.

colors = ['blue','red','black']
alpha = [0.5,1,0.8]

T_audio = 9 * second
tmax = T_audio * 6
FS = 22050 * Hz

N_delays = 256
tau_delay = 10 * ms
tau_omega = 3 * ms

f = open('SFA_for_all_results.pickle','rb')
res = pickle.load(f)
w = res['w'].ravel()
delays = res['delays']

Groups = get_song_groups(K_groups=5)
k_g = 1

WAV,X = load_group_audio_from_song_list(Groups[k_g]['list'],T_audio,FS,N_delays,tau_delay,tau_omega)
T = arange(len(WAV)) * 1./FS

## Printing
plt.figure(figsize=(22,9))
gs = gridspec.GridSpec(2,2)
gs.update(wspace=0.5,hspace=0.5)

ind = -1
for k_s,s in enumerate(Groups[k_g]['list']):
    if s['bpm'] > 125 or s['bpm'] < 115:
        ind = k_s
        print str(k_s) + ' spotted as ousider'

t_start_outsider = T_audio * ind
t_end_outsider = T_audio * (ind+1)
out = dot(X,w.ravel())

f = open('tempo_list.pickle','rb')
tempo_list = pickle.load(f)

plt.subplot(gs[0,0])
plot_fancy.set_font()
bins = arange(85,155,5)
hist,bins = histogram(tempo_list,bins)
plt.bar(bins[1:],hist,width=5)
plt.savefig('tempo_histogram')
plt.xlabel('BPM')
plt.ylabel('Number of songs')
plt.xticks([90,120,150])
plt.xlim([90,150])
plt.yticks([0,15,30])

ax = plt.subplot(gs[0,1])
plot_fancy.set_font()

g1 = Groups[0]['list']
g2 = Groups[1]['list']

print_lab = [True,True,True]
for s in concatenate((g1,g2)):
    x = load_audio(s['path'],T_audio,FS)
    x -= mean(x)
    x /= la.norm(x)
    out = dot(expand_with_delays(x,N_delays,tau_delay,FS),w.ravel())

    if s['bpm'] < 115:
        col = colors[0]
        al = alpha[0]
        if print_lab[0]:
            lab = r'BPM < 115, > 125'
            print_lab[0] = False
        else: lab = ''
    elif s['bpm'] < 120:
        col = colors[1]
        al = alpha[1]
        if print_lab[1]:
            lab = r'115 < BPM < 120'
            print_lab[1] = False
        else: lab = ''
    elif s['bpm'] < 125:
        col = colors[2]
        al = alpha[2]
        if print_lab[2]:
            lab = r'120 < BPM < 125'
            print_lab[2] = False
        else: lab = ''
    elif s['bpm'] > 125:
        col = colors[0]
        al = alpha[0]
        if print_lab[0]:
            lab = r'BPM < 115, > 125'
            print_lab[0] = False
        else: lab = ''

    frq,spc = to.getSpectrum(out,FS/Hz)

    plot(frq,10* np.log10(spc),color=col,alpha=al,linewidth=2.,label=lab)
xlim([frq_min,frq_max])
#ylim([-80,0])
#yticks([-80,-40,0])
ylabel(r'$| \mathcal{F} (s) |$ (dB)')
xlabel('f (Hz)')
title('Power spectrum')

ax.axvline(BPM/Hz,linestyle='--',linewidth=4.0, color='green')
legend(bbox_to_anchor=(1.05, 1), loc=2, mode="expand")
xscale('log')

# tau = 100 * ms
# def window_rms(a, window_size):
#   a2 = pow(a,2)
#   window = np.ones(window_size)/float(window_size)
#   return sqrt(fftconvolve(a2, window, 'same'))
# window_duration = 3 * second
# rms_WAV = window_rms(WAV,int(window_duration * FS))
# rms_out = window_rms(out,int(window_duration * FS))
#
# plt.plot(T,rms_out / rms_WAV,color=colors[2],alpha=alpha[0],linewidth=2.0)
# #plt.plot(T,rms_out,color=colors[2],alpha=alpha[2],linewidth=1.4,label=r'$s$ (Batch)')
#
# xstart = t_start_outsider / second
# xend = t_end_outsider / second
# xmax = tmax / second
# rect1 = matplotlib.patches.Rectangle((0.,-5.), xstart, 5., color='red')
# rect2 = matplotlib.patches.Rectangle((xend,-5.), xmax, -4.5, color='red')
# ax.add_patch(rect1)
# ax.add_patch(rect2)

# print t_start_outsider
# print t_end_outsider
#
# ylabel(r'$x_0 / s $')
# xlabel('t (s)')
# ylim([-5,5])
# xlim([0,tmax/second])
# xticks(arange(6) * T_audio / second)
# locator_params(axis = 'y', nbins = 4)
#
# legend(bbox_to_anchor=(1.05, 1), loc=2, mode="expand")




frq_wei,spc_wei = to.getSpectrum(w,1/tau_delay,pad_to=4096)

spc_wei /= norm(spc_wei)
spc_wei_dB = 10 * np.log10(spc_wei)


ax = plt.subplot(gs[1,0])
plot_fancy.set_font()
ax.plot(delays,w,color=colors[2],linewidth=2.0)
ax.set_title('Learnt filter')
ax.set_ylabel('Weights')
ax.set_xlabel('Delays (s)')
xlim([min(delays),max(delays)])
ylim([-0.15,0.15])
yticks([-.1,0,.1])
locator_params(axis = 'x', nbins = 4)


ax = plt.subplot(gs[1,1])
plot_fancy.set_font()
ax.plot(frq_wei,spc_wei_dB,color=colors[2],linewidth=2.0)
ax.set_title('Frequency response')
ax.set_ylabel('Gain (dB)')
ax.set_xlabel('f (Hz)')
ax.set_ylim([-60,0])
ax.set_yticks([-60,-30,0])
ax.set_xscale('log')
ax.set_xlim([frq_min,frq_max])


ax.axvline(BPM/Hz,linestyle='--',linewidth=4.0, color='green')


#tight_layout()
savefig('fig10_SFA_for_all.pdf')

plt.show()
