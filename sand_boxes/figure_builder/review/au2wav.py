__author__ = 'guillaume'

import sys
import os
import sunau
import librosa
import wave
import struct
from bin.tools.resample_audio import resample

def byteswap(s):
    """Swaps the endianness of the bytesting s, which must be an array
    of shorts (16-bit signed integers). This is probably less efficient
    than it should be.
    """
    assert len(s) % 2 == 0
    parts = []
    for i in range(0, len(s), 2):
        chunk = s[i:i + 2]
        newchunk = struct.pack('<h', *struct.unpack('>h', chunk))
        parts.append(newchunk)
    return b''.join(parts)


datasetpath = '/home/guillaume/_Programming/datasets/GTZAN/genres'

for g in os.listdir(datasetpath):
    path = datasetpath + '/' + g
    for f in os.listdir(path):
        try:
            wav_name = f[:-3] + '.wav'
            au_file = sunau.open(path + '/' + f)
            N = au_file.getnframes()
            FS = au_file.getframerate()

            aud = au_file.readframes(N)
            aud = byteswap(aud)

            wav = wave.open(path + '/' + wav_name, 'w')
            wav.setnchannels(1)
            wav.setsampwidth(2)
            wav.setframerate(FS)
            wav.writeframes(aud)
            wav.close()

            #resample(path + '/' + wav_name[:-4],'.wav',11025)
            print 'created ', wav_name
        except:
            print 'Warning: ', f, 'failed'
