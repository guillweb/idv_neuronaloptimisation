__author__ = 'guillaume'

import os
import sunau
import scipy.io.wavfile as wa
import cPickle as pickle
from sand_boxes.figure_builder.review.bpm_detection import *
import numpy.random as rd
from numpy import *
from brian2 import *
import bin.input_loader.input_loader as il
from guillaume_toolbox.optimization.eigs_dimension_reduction import PCA
import numpy.linalg as la
import matplotlib as mpl
from profilehooks import profile
from scipy.signal import fftconvolve

#mpl.rc('lines',linewidth=2)
#mpl.rc('font',size=18)

def load_audio(path,T_audio,FS,ramp_duration=300 * ms):
    fs,wav = wa.read(path)
    if len(shape(wav)) > 1: wav = wav[:,0]
    fs = fs * Hz
    assert FS == fs, "Wrong sampling frequency {0} instead of {1} for song {2}.".format(fs,FS,path)
    N_audio = int(fs * T_audio)
    wav = wav[:N_audio]
    wav -= mean(wav)
    wav /= std(wav)

    N_ramp = int(ramp_duration * FS)
    ramp = linspace(0.,1.,N_ramp)
    wav[:N_ramp] *= ramp
    wav[-N_ramp:] *= ramp[::-1]

    return array(wav[:N_audio],dtype=float)

def expand_with_delays(WAV,N_delays,tau_delay,FS):
    X = zeros((WAV.size,N_delays))

    n_shift = int(FS * tau_delay)
    for k_d in arange(N_delays):
        X[:,k_d] = roll(WAV,n_shift * k_d)
    return X

def load_group_audio_from_song_list(list,T_audio,FS,N_delays,tau_delay,tau_omega):
    # Make wav
    WAV = zeros((0,))
    for s in list:
        wav = load_audio(s['path'],T_audio,FS)
        WAV = concatenate((WAV,wav))

    WAV -= mean(WAV)
    WAV /= std(WAV)
    if tau_omega > 0 * second:
        t_vec = arange(20 * int(tau_omega * FS)) * 1. /FS
        filt = t_vec / tau_omega**2 * exp(- t_vec / tau_omega)
        WAV = fftconvolve(WAV,filt,'same')
        WAV -= mean(WAV)
        WAV /= std(WAV)

    X = expand_with_delays(WAV,N_delays,tau_delay,FS)
    return WAV,X

def get_tempo_values(datasetpath = '/home/bellec/datasets/GTZAN/genres',T_audio = 9 * second,do_save =True):
    results = []

    for g in os.listdir(datasetpath):
        path = datasetpath + '/' + g
        for f in os.listdir(path):
            try:
                if f[-4:] == '.wav':
                    wav_path = path + '/' + f
                    FS,data = wa.read(wav_path)

                    N_audio = int(T_audio * FS * Hz)
                    if len(data.shape) > 1: data = data[:,0]
                    data = data[0:N_audio]

                    bpm,_ = bpm_detector(data,FS)
                    obj = {'name': f, 'path': wav_path, 'bpm': bpm,'genre':g}
                    results.append(obj)
            except:
                print 'WARNING: ', f, ' tempo computation failed.'

    if do_save:
        file = open('bpm_database.pickle','wb')
        pickle.dump(results,file)
        file.close()

    return results

def get_song_selection(seed=0,T_audio=9*second,do_save=True,do_plot=True,bpm_min=85,bpm_max=155,bpm_peak=120,d_bpm=5,count_peak = 50,count_others=10,genre_restriction = array([])):

    try:
        file = open('bpm_database.pickle','rb')
        bpm_db = pickle.load(file)
    except:
        bpm_db = get_tempo_values(T_audio=T_audio)

    if seed >0:rd.seed(seed)

    N = len(bpm_db)
    rd.shuffle(bpm_db)

    sub_set_peak = []
    sub_set_others = []

    tempo_list = []

    for s in bpm_db:
        if not(genre_restriction) or s['genre'] in genre_restriction:
            if s['bpm'] > bpm_peak - d_bpm and s['bpm'] < bpm_peak + d_bpm:
                if count_peak >0:
                    sub_set_peak.append(s)
                    tempo_list.append(s['bpm'])
                    count_peak -= 1
            elif s['bpm'] > bpm_min and s['bpm'] < bpm_max:
                if count_others > 0:
                    sub_set_others.append(s)
                    tempo_list.append(s['bpm'])
                    count_others -= 1

    assert count_others ==0 and count_peak == 0, 'Not enough song found \t peak {0} \t others {1}'.format(len(sub_set_peak),len(sub_set_others))

    if do_save:
        file = open('peak_bpm_database.pickle','wb')
        pickle.dump(sub_set_peak,file)
        file.close()

        file = open('others_bpm_database.pickle','wb')
        pickle.dump(sub_set_others,file)
        file.close()

        file = open('tempo_list.pickle','wb')
        pickle.dump(tempo_list,file)
        file.close()

    if do_plot:
        plt.figure()
        bins = arange(bpm_min,bpm_max,d_bpm)
        hist,bins = histogram(tempo_list,bins)
        plt.bar(bins[1:],hist,width=d_bpm)
        plt.savefig('tempo_histogram')
        plt.xlabel('BPM')
        plt.ylabel('Number of songs')
        plt.xticks(bins[::4])
        plt.yticks([0,15,30])
        plt.show()

    return sub_set_peak,sub_set_others,tempo_list

@profile
def get_song_groups(K_groups=20,n_peak=5,n_others=1,FS=22050 * Hz,N_delays=128,tau_delay=10 * ms,T_audio = 9 * second,do_pca=False,tau_omega=-1 * second):

    try:
        # Open subset list
        file = open('peak_bpm_database.pickle','rb')
        sub_set_peak = pickle.load(file)
        file.close()
        file = open('others_bpm_database.pickle','rb')
        sub_set_others = pickle.load(file)
        file.close()
    except:
        sub_set_peak,sub_set_others,tempo_list = get_song_selection(seed=0)

    list_dir = os.listdir('./mini_batch_groups/')
    Groups = []

    # K times do: take a subset of 6 song, verify that maximum one element has a tempo out
    for k in arange(K_groups):
        filename = 'group_' + str(k) + '.pickle'

        if not(filename in list_dir):
            rd.seed(k)
            peak_list = []
            others_list = []

            for k_ch in rd.choice(len(sub_set_peak),n_peak,replace=False):
                peak_list.append(sub_set_peak[k_ch])
            for k_ch in rd.choice(len(sub_set_others),n_others,replace=False):
                others_list.append(sub_set_others[k_ch])

            list = concatenate((peak_list,others_list))
            rd.shuffle(list)

            WAV,X = load_group_audio_from_song_list(list,T_audio,FS,N_delays,tau_delay,tau_omega)

            if do_pca:
                pca = PCA(X,std_min=1e-6)
                Y = pca.transform(X)
                dY = diff(Y,axis=0)
            else:
                pca = False
                Y = X
                dY = diff(X,axis=0)

            cov = dot(Y[:,0:3].T,Y[:,0:3]) / Y.shape[0]
            print 'De-correlation check \t {0} \t fact {1} and {2}'.format(cov[0,0],cov[0,1],cov[0,2])

            K = - dot(dY.T,dY)
            w,U = la.eig(K)
            K /= max(abs(w))

            obj = {'list':list,'kernel':K,'pca':pca}

            file = open('mini_batch_groups/' + filename,'wb')
            pickle.dump(obj,file)
            file.close()
            print 'Group '+ str(k) + '  created with bpm list:'
            bpm_list = zeros(len(list))
            for k_s,s in enumerate(list):
                bpm_list[k_s] = s['bpm']
            print bpm_list

            Groups.append(obj)
        else:
            print 'Group '+ str(k) + ' exists.'
            file = open('mini_batch_groups/' + filename,'rb')
            obj = pickle.load(file)
            Groups.append(obj)
            file.close()

    return Groups

    # Concatenate, expand with delays. build PCA transformation compute kernel in group PCA basis

