__author__ = 'guillaume'

from numpy import *
from brian2 import *
from generate_task import *
import numpy.linalg as la

tau_delay = 97 / (11025 * Hz)
FS = 22050 * Hz
N_delays = 256
tau_omega = .3*ms

do_pca = False

# Iteration numbers
K_groups = 20
groups_per_iteration = 20
n_peak = 5
n_others = 1
N_dataset_loop = 10**6

# Learning rates
eta = .001

# init group
Groups = get_song_groups(tau_omega=tau_omega,do_pca=do_pca,n_peak=n_peak,K_groups=K_groups,N_delays=N_delays,FS=FS,tau_delay=tau_delay)
N_iterations = groups_per_iteration*N_dataset_loop
record_every = N_dataset_loop / 1000

# init weight
w = rd.rand(1,N_delays)
w /= la.norm(w)
dW = zeros(w.shape)

E_record = zeros(N_dataset_loop / record_every -1)
print 'N iterations ', N_iterations
step = 0
for k_loop in arange(N_dataset_loop):

    rd.shuffle(Groups)
    dw = zeros(shape(w))

    for k_g,g in enumerate(Groups):
        if k_g > groups_per_iteration: break
        K = g['kernel']
        pca = g['pca']

        if do_pca:
            w_ = pca.transform(w)
            g_ = dot(w_,K)
            g = pca.inv_transform(g_)
        else:
            g = dot(w,K)
        dw += g / groups_per_iteration
    w += eta * dw
    w /= la.norm(w)

    if mod(step,record_every) == 0:
        E = - inner(dw,w)
        print 'Step {0} \t E {1}'.format(step,E)
        if step/record_every < len(E_record):
            E_record[step/record_every] = E
    step += 1

delays = arange(N_delays) * tau_delay

obj = { 'w':w, 'delays':delays, 'E':E_record}
f = open('SFA_for_all_results.pickle','wb')
pickle.dump(obj,f)
f.close()

plt.figure()
plt.subplot(2,1,1)
plt.plot(E_record)
plt.subplot(2,1,2)
plt.plot(delays,w.ravel())

plt.show()

plt.figure()