__author__ = 'guillaume'

from brian2 import *
from bin.brian_SFA.runner_brian.run_SFA_continuous_lambda import run_SFA_continuous_lambda
from bin.brian_SFA.runner_brian.run_SFA_batch_lambda import run_SFA_batch_lambda
from bin.brian_SFA.runner_brian.run_SFA_wiskott import run_SFA_wiskott
from bin.brian_SFA.runner_brian import plot_fancy

from bin.filter_optimisation.filter_builder import *
from bin.brian_SFA.runner_brian import check_validity
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cPickle as pickle
import time

tmax_short = 4 * second
tmax = 100 * second

tau_omega = 0 * ms
tau_omega_draw = 10 * ms

tau_w = 1 * second
tau_derivation = 5 * ms


w_0 =  np.random.randn(5,1)
w_0 /= norm(w_0)

T_batch,x,s_batch = run_SFA_batch_lambda(tau_omega=tau_omega,tmax=tmax_short,w_0=w_0,Noutputs=4)
T_online,x_th,x_alpha,s_online,s_al,w_online,w_al,post_idx = run_SFA_continuous_lambda(tau_omega=tau_omega,tmax=tmax,tau_w=tau_w,tau_derivation=tau_derivation,w_0=w_0,Noutputs=4)


dt = 0.1 * ms
T = arange(10**4) * dt
tau_period = 20 * tau_omega_draw

builder = FilterBuilder(T,tau_period)
F = zeros((4,len(builder.T)))
F[0,:] = builder.getSFAFilter(tau_omega_draw).getFilter()
F[1,:] = builder.getDerivativeFilter(tau_omega_draw).getFilter()
F[2,:] = builder.getSigmaFilter(tau_omega_draw).getFilter()
F[3,:] = - builder.getSigmaFilter(tau_omega_draw).getFilter()


plt.figure(figsize=(16,16))
gs = gridspec.GridSpec(4,3)

colors = plot_fancy.get_color()
wins = plot_fancy.get_win()
plot_fancy.set_font()

ax_list = []
win_list = []
batch_list= []
online_list= []

print 'sum'
print sum(np.abs(F),axis=1)

gs_online = []
for No in arange(4):
    ax0 = plt.subplot(gs[No,0])
    ax0.plot(builder.T,np.zeros(np.shape(builder.T)),'r--',color='gold')
    ax0.plot(builder.T,F[No,:],color=colors[No],linewidth=2.0)
    win_list.append(ax0)
    #ax0.set_ylim([-0.005,0.005])
    ax0.set_xlim([- 10 * tau_omega_draw,10 * tau_omega_draw])
    ax0.set_ylabel(wins[No])
    ax0.yaxis.label.set_color(colors[No])
    ax0.yaxis.label.set_size(30)


    ax1 = plt.subplot(gs[No,1])
    ax1.plot(T_batch,s_batch[No,:],color=colors[No],linewidth=2.0)
    batch_list.append(ax1)

    gs_online.append(gridspec.GridSpecFromSubplotSpec(1,4,gs[No,2]))

    ax2 = plt.subplot(gs_online[No][0])
    ax2.plot(T_online,s_online[No,:],color=colors[No],linewidth=2.0)
    ax2.set_xlim([0,1])

    ax3 = plt.subplot(gs_online[No][1:-1])
    ax3.plot(T_online,s_online[No,:],color=colors[No],linewidth=2.0)
    ax3.set_xlim([97,100])


    # Breaking between before and after
    ax2.spines['right'].set_visible(False)
    ax3.spines['left'].set_visible(False)

    ax2.yaxis.tick_left()
    ax3.yaxis.tick_right()

    ax3.tick_params(labelleft='off') # don't put tick labels at the top

    d = .05 # how big to make the diagonal lines in axes coordinates
    # arguments to pass plot, just so we don't keep repeating them
    kwargs = dict(transform=ax2.transAxes, color='k', clip_on=False)
    ax2.plot((1-d*2.2,1+d*2.2),(1-d,1+d), **kwargs)      # bottom-right diagonal
    ax2.plot((1-d*2.2,1+d*2.2),(-d,+d), **kwargs)    # tpop-right diagonal

    kwargs.update(transform=ax3.transAxes)  # switch to the bottom axes
    ax3.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
    ax3.plot((-d,+d),(-d,+d), **kwargs) # top-left diagonal


    plt.setp(ax3.get_yticklabels(), visible=False)

    if No < 3:
        plt.setp(ax0.get_xticklabels(), visible=False)
        plt.setp(ax1.get_xticklabels(), visible=False)
        plt.setp(ax2.get_xticklabels(), visible=False)
        plt.setp(ax3.get_xticklabels(), visible=False)








    online_list.append([ax2,ax3])

    ax_list = ax_list + [ax0,ax1,ax2,ax3]

for ax in win_list:
    ax.locator_params(axis = 'x', nbins = 3)
    ax.locator_params(axis = 'y', nbins = 3)

for ax in batch_list:
    ax.locator_params(axis = 'x', nbins = 4)
    ax.locator_params(axis = 'y', nbins = 4)
    ax.set_ylim([-2.5,2.5])

for l in online_list:
    l[0].locator_params(axis = 'x', nbins = 1)
    l[0].locator_params(axis = 'y', nbins = 4)
    l[0].set_ylim([-2.5,2.5])
    l[1].locator_params(axis = 'x', nbins = 3)
    l[1].yaxis.label.set_visible([False])
    l[1].set_ylim([-2.5,2.5])



win_list[0].set_title('STDP window')
batch_list[0].set_title('Batch')
online_list[0][1].set_title('Online')

win_list[3].set_xlabel('t (s)')
batch_list[3].set_xlabel('t (s)')
online_list[3][0].set_xlabel('t (s)')
online_list[3][1].set_xlabel('t (s)')

tight_layout()
savefig('../../figures/fig5_window_comparison.pdf')



plt.show()